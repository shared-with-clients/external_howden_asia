import { LightningElement, api, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
// import Label_Button_Register_Text from '@salesforce/label/c.Label_Button_Register_Text';
import createActionQueueable from '@salesforce/apex/ActionQueueablePageController.createActionQueueable';
import validateAction from '@salesforce/apex/ActionQueueablePageController.validateAction';

export function showNotification({variant, msg, title,context})
    {
        const evt = new ShowToastEvent({
            title: title,
            message: msg,
            variant: variant
        });
        context.dispatchEvent(evt);
    }
export default class ActionButtonAsyncCmp  extends NavigationMixin(LightningElement) {
    @api variant = "brand";
    @api label;
    @api actionName; 
    @api dispatcherName; 
    @api className; 
    @api recordId; 
    @api navigateType = "standard__recordPage";
    @api navigateAction = "view";
    @api notificationTitle = '';
    @api errorMsg = "Error Action"; // add custum label
    @api successMsg = "Success Action"; // add custum label
    @api result;
    @api apiCallFlag = false;
    @track listEventData = [];

    
    

    handleButtonClick(){
        this.apiCallFlag = true;
        const dataInputJson = {
            listEventData : this.listEventData,
            actionName: this.actionName,
            dispatcherName: this.dispatcherName,
            className: this.className,
            errorMsg: this.errorMsg,
            successMsg: this.successMsg,
            recordId: this.recordId //not in use from the button but from theS cmp (for now)
        };
          // eslint-disable-next-line no-console
        console.log('click ActionsCall param ',JSON.stringify(dataInputJson));
        validateAction({dataInputJson: JSON.stringify(dataInputJson), dispatcherName : this.dispatcherName}).then(result => {
            console.log(result);
            if(result){
                createActionQueueable({dataInputJson: JSON.stringify(dataInputJson), dispatcherName : this.dispatcherName}).then(result => {
                    console.log(result);
                    if(result){
                        let urlWithParams = '/apex/ActionQueueablePage?jobId=' + result + '&returnToId=' + this.recordId;
                        this[NavigationMixin.GenerateUrl]({
                            type: 'standard__webPage',
                            attributes: {
                                url: urlWithParams
                            }
                        }).then(generatedUrl => {
                            window.location.href = generatedUrl;
                        });
                    }
                }).catch(error => {
                    // eslint-disable-next-line no-console
                    console.log('click error', error);
                    showNotification({title: error.body.message, variant: 'error', msg :error, context:this});
                    // this.apiCallFlag = false;
                })
            }
        }).catch(error => {
            
            // eslint-disable-next-line no-console
            
            
            showNotification({title: error.body.message, variant: 'error', msg :error, context:this});
            // this.apiCallFlag = false;
            console.log('click error', error);
        })
    }
}