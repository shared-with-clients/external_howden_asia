public with sharing class ReplacementTransactionController {

    private Novidea_HPC__Income__c[] incomes;
    public Boolean foundError{get; private set;}
    public String sureReplace {get; private set;}
    public string retUrl {get; private set;}
    private Id policyId;
    
    public ReplacementTransactionController(ApexPages.StandardSetController controller) {
        this.incomes = controller.getSelected();
        this.policyId = Apexpages.currentPage().getParameters().get('id');
        this.retUrl = null;
        this.foundError = false;
    }
    public void init(){
        this.sureReplace = String.format(Label.Sure_To_Replace, new String[]{Novidea_HPC__Income__c.sObjectType.getDescribe().getLabel()});
    }
    public void contra(){
        System.Savepoint savePoint = Database.setSavePoint();
        try{
            Novidea_HPC.BinderDispatcher dispatcher = new Novidea_HPC.BinderDispatcher();
            dispatcher.init(JSON.serialize(new Map<String, Object>{'className' => 'ContraTransaction'}));
            for (Integer i = 0; i < incomes.size(); i++){
                //Information for ContraTransaction method initArgsStandardStructure in BinderDispatcher
                Map<String, Object> dataCmpMap = new Map<String, Object>{'value' => 'Replacement'};
                Map<String, Object> metaDataMap = new Map<String, Object>{NOVU.VirtualDispatcher.RECORD_ID => incomes[i].Id};
                Map<String, Object> dataEventMap = new Map<String, Object>{'metaData' => metaDataMap, 'data' => dataCmpMap};
                Object listEventData = new List<Object>{dataEventMap};
                Map<String, Object> mydata = new Map<String, Object>{'listEventData' => listEventData};
                //Contra Transaction operation
                Object result = dispatcher.call('createContraTransactions' , new Map<String, Object>{'data' => (new Object[]{mydata})});
                Map<String, Object> webResult = (map<String, Object>)JSON.deserializeUntyped((String)result);
                //If exception, code not 200
                if (webResult.get('code')!=200){
                    foundError = true;
                    throw new ReplacementTransactionException((String)webResult.get('errorMessage'));
                }
            }
        } catch (Exception e) {
            Database.rollback(savePoint);
            foundError = true;
            NOVU.PageUtility.showFatal(e.getMessage());
        }
    }
    public void replace(){
        System.Savepoint savePoint = Database.setSavePoint();
        try{
            Novidea_HPC.BinderDispatcher dispatcher = new Novidea_HPC.BinderDispatcher();
            dispatcher.init(JSON.serialize(new Map<String, Object>{'className' => 'ContraTransaction'}));
            for (Integer i = 0; i < incomes.size(); i++){
                //Information for ReplacementTransaction method initArgsRecordId in BinderDispatcher
                Map<String, Object> metaDataMap = new Map<String, Object>{NOVU.VirtualDispatcher.RECORD_ID => incomes[i].Id};
                Object listEventData = new List<Object>{metaDataMap};
                //Replacement Transaction operation
                Object result = dispatcher.call('createReplacementTransactions' , new Map<String, Object>{'data' => listEventData});
                map<String, Object> webResult = (map<String, Object>)JSON.deserializeUntyped((String)result);
                //If exception, code not 200
                if (webResult.get('code')!=200){
                    foundError = true;
                    throw new ReplacementTransactionException((String)webResult.get('errorMessage'));
                }
            }
            retUrl = policyId;
        } catch (Exception e) {
            Database.rollback(savePoint);
            foundError = true;
            NOVU.PageUtility.showFatal(e.getMessage());
        }
    }
    
    public class ReplacementTransactionException extends Exception{}

    public PageReference back(){ return null; }
}
