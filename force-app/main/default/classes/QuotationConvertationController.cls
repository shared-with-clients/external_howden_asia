public class QuotationConvertationController {

    @AuraEnabled
    public static Response validateConversion(String recordId) {
        try {
            Response resp;
            Novidea_HPC__Application__c application = [SELECT Id, Account_not_Exists_or_Unapproved__c, (SELECT Id FROM Novidea_HPC__Proposals__r) FROM Novidea_HPC__Application__c WHERE Id =: recordId]; 
            system.debug('application ' + application);
            if(application.Account_not_Exists_or_Unapproved__c) {
                resp = new Response('Error', 'Assigned client account to the Quotation is not Approved yet.');
            } else{
                if(application.Novidea_HPC__Proposals__r.size() > 0) {
                    String partOfUrl = '';
                    for(Novidea_HPC__Proposal__c proposal_i: application.Novidea_HPC__Proposals__r) {
                        partOfUrl += proposal_i.Id + '%2C';
                    }
                    partOfUrl.removeEnd('%2C');
                    String redirectUrl = 'https://howden-tmea--novidea-hpc.visualforce.com/apex/ApprovedProposals?proposalCsv=' + partOfUrl + '&retURL=%2Fapex%2FNovidea_HPC__windowclose&id=' + application.Id;
                    system.debug('redirectUrl ' + redirectUrl);
                    resp = new Response('Success', redirectUrl);
                } else{
                   resp = new Response('Error', 'There are no related proposals.'); 
                }
            }
            return resp;
        } catch(Exception e) {
            system.debug('Exception ' + e.getMessage() + ' ' + e.getLineNumber());
            return new Response('Error', 'Convertion exception. Please, contact your administrator!');
        }
    }
}