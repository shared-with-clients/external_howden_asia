public with sharing class ReceiptTriggerHelper {
    public static boolean stopTrigger = false;
    public static boolean inTrigger = false;
    private static final Set<String> CHANGED_FIELDS = new Set<String>{'HPC_Collection__Client_Receipt__c', 'HPC_Collection__Collection_Type__c', 'HPC_Collection__Ready_For_Collection__c', 'HPC_Collection__Effective_Date__c'};

    public static void onBeforeInsert(List<HPC_Collection__Receipt__c> newReceipts){
		if(stopTrigger || inTrigger)return;
        inTrigger = true;
        setDataFromCancelledReceipts(newReceipts);
        updateUwrInstallments(null, null, newReceipts);
        inTrigger = false; 
    }  
    
    public static void onAfterInsert(List<HPC_Collection__Receipt__c> newReceipts){
		if(stopTrigger || inTrigger)return;
        inTrigger = true;

        inTrigger = false; 
    }  

    public static void onBeforeUpdate(
        List<HPC_Collection__Receipt__c> oldReceipts,
        Map<Id,HPC_Collection__Receipt__c> oldReceiptsMap,
        List<HPC_Collection__Receipt__c> newReceipts,
        Map<Id,HPC_Collection__Receipt__c> newReceiptsMap){
		if(stopTrigger || inTrigger)return;
        inTrigger = true;
        //setDataFromCancelledReceipts(newReceipts);
        IncomeTriggerHelper.MapComparisonResult compareResult = IncomeTriggerHelper.compareRecordsFieldsInclude(newReceiptsMap.values(), oldReceiptsMap, CHANGED_FIELDS);
		if(!compareResult.isEqual){
            updateUwrInstallments(oldReceiptsMap, newReceiptsMap, null);
        }
        inTrigger = false; 
    }  
    
    public static void onAfterUpdate(
        List<HPC_Collection__Receipt__c> oldReceipts,
        Map<Id,HPC_Collection__Receipt__c> oldReceiptsMap,
        List<HPC_Collection__Receipt__c> newReceipts,
        Map<Id,HPC_Collection__Receipt__c> newReceiptsMap){
		if(stopTrigger || inTrigger)return;
        inTrigger = true;
        IncomeTriggerHelper.MapComparisonResult compareResult = IncomeTriggerHelper.compareRecordsFieldsInclude(newReceiptsMap.values(), oldReceiptsMap, CHANGED_FIELDS);
		if(!compareResult.isEqual){
            updateUwrInstallments(oldReceiptsMap, newReceiptsMap);
        }
        inTrigger = false; 
    }      

    private static void updateUwrInstallments(Map<Id,HPC_Collection__Receipt__c> oldReceiptsMap, Map<Id,HPC_Collection__Receipt__c> newReceiptsMap, List<HPC_Collection__Receipt__c> newReceipts){
        Set<Id> clientInstallmentsIdSet = new Set<Id>();
        Boolean isInsert = newReceipts != null;
        for(HPC_Collection__Receipt__c receipt : isInsert ? newReceipts : newReceiptsMap.values()){
            if(!isInsert && receipt.HPC_Collection__Client_Receipt__c != null && oldReceiptsMap.get(receipt.Id).HPC_Collection__Client_Receipt__c == null){
                clientInstallmentsIdSet.add(receipt.HPC_Collection__Client_Receipt__c);
            }
            else if(isInsert && receipt.HPC_Collection__Client_Receipt__c != null){
                clientInstallmentsIdSet.add(receipt.HPC_Collection__Client_Receipt__c);
            }
        }
        if(!clientInstallmentsIdSet.isEmpty()){
            Map<Id, HPC_Collection__Receipt__c> clientInstallmentsMap = new Map<Id, HPC_Collection__Receipt__c>([
                SELECT HPC_Collection__Collection_Type__c
                FROM HPC_Collection__Receipt__c
                WHERE Id IN :clientInstallmentsIdSet
            ]);
            for(HPC_Collection__Receipt__c receipt : isInsert ? newReceipts : newReceiptsMap.values()){
                if(receipt.HPC_Collection__Client_Receipt__c != null && clientInstallmentsMap.containsKey(receipt.HPC_Collection__Client_Receipt__c)){
                    receipt.HPC_Collection__Collection_Type__c = clientInstallmentsMap.get(receipt.HPC_Collection__Client_Receipt__c).HPC_Collection__Collection_Type__c;
                }
            }
        }
    }
    private static void updateUwrInstallments(Map<Id,HPC_Collection__Receipt__c> oldReceiptsMap, Map<Id,HPC_Collection__Receipt__c> newReceiptsMap){
        Set<Id> relevantInstallmentIdSet = new Set<Id>();
        Map<Id, HPC_Collection__Receipt__c> uwrInstallmentsToUpdate = new  Map<Id, HPC_Collection__Receipt__c>();

        for(HPC_Collection__Receipt__c receipt : newReceiptsMap.values()){
            if(receipt.HPC_Collection__Collection_Type__c != oldReceiptsMap.get(receipt.Id).HPC_Collection__Collection_Type__c && receipt.HPC_Collection__Client_Receipt__c == null){
                relevantInstallmentIdSet.add(receipt.Id);
            }
        }

        if(!relevantInstallmentIdSet.isEmpty()){
            Map<Id, HPC_Collection__Receipt__c> uwrReceiptsMap = new Map<Id, HPC_Collection__Receipt__c>([
                SELECT HPC_Collection__Client_Receipt__c
                FROM HPC_Collection__Receipt__c
                WHERE HPC_Collection__Client_Receipt__c IN :relevantInstallmentIdSet
            ]);

            for(HPC_Collection__Receipt__c receipt : uwrReceiptsMap.values()){
                receipt.HPC_Collection__Collection_Type__c = newReceiptsMap.get(receipt.HPC_Collection__Client_Receipt__c).HPC_Collection__Collection_Type__c;
                uwrInstallmentsToUpdate.put(receipt.Id, receipt);
            }

            if(!uwrInstallmentsToUpdate.isEmpty()){
                try {
                    Database.update(uwrInstallmentsToUpdate.values());
                } catch (Exception e) {
                    System.debug(e.getMessage() + ' - ' + e.getLineNumber() + ' - ' + e.getStackTraceString());
                }
            }
        }
    }

    public static void setDataFromCancelledReceipts(List<HPC_Collection__Receipt__c> newReceipts){
        Map<Id, List<HPC_Collection__Receipt__c>> cancellingIncomeIdsToNewReceipts = new Map<Id, List<HPC_Collection__Receipt__c>>();
        Map<Id, Novidea_HPC__Income__c> incomeIdsToCancelledIncomes = new Map<Id, Novidea_HPC__Income__c>();
        Map<Id, List<HPC_Collection__Receipt__c>> cancelledIncomeIdsToNewReceipts = new Map<Id, List<HPC_Collection__Receipt__c>>();
        Map<Id, List<HPC_Collection__Receipt__c>> cancelledIncomeIdsToCancelledReceipts = new Map<Id, List<HPC_Collection__Receipt__c>>();
        for(HPC_Collection__Receipt__c newReceipt : newReceipts){
            if( newReceipt.HPC_Collection__Ready_For_Collection__c != true ){
                if(cancellingIncomeIdsToNewReceipts.containsKey(newReceipt.HPC_Collection__Income__c)){
                    cancellingIncomeIdsToNewReceipts.get(newReceipt.HPC_Collection__Income__c).add(newReceipt);
                }else{
                    cancellingIncomeIdsToNewReceipts.put(newReceipt.HPC_Collection__Income__c, new List<HPC_Collection__Receipt__c>{newReceipt});
                }
            }
        }

        List<Novidea_HPC__Income__c> incomes = [SELECT Id, Novidea_HPC__Cancel_Income__c
                                                FROM Novidea_HPC__Income__c
                                                WHERE Novidea_HPC__Cancel_Income__c != null AND Id IN :cancellingIncomeIdsToNewReceipts.keySet()];
        if(incomes == null || incomes.isEmpty()) return;

        for(Novidea_HPC__Income__c income : incomes){
            cancelledIncomeIdsToNewReceipts.put(income.Novidea_HPC__Cancel_Income__c, cancellingIncomeIdsToNewReceipts.get(income.Id));
        }

        List<Novidea_HPC__Income__c> cancelledIncomes = [SELECT Id, 
                                                    (SELECT HPC_Collection__Commission_Amount__c, Discount__c, Commission_GST__c, GST_Amount_on_Premium__c, Client_Payable__c, Commission_Amount_Discount__c,
                                                    Net_Brokerage__c, HPC_Collection__Premium_To_Pay__c, Other_Charges__c, Other_Taxes__c, Payable_to_Insurer__c, GST_on_Brokerage_Base_Currency__c, 
                                                    Policy_Charges__c, Service_Fee__c, Service_Fee_GST__c, Stamp_Duty__c, HPC_Collection__Effective_Date__c, HPC_Collection__Net_Premium__c, HPC_Collection__Collection_Type__c,
                                                    Legal_Entity__c, Legal_Entity_Unit_4_Code__c, Settlement_Commission_Discount_Amount__c, Division__c, Department__c, Waiting_for_Calculation__c
                                                    FROM HPC_Collection__Receipts__r)
                                                FROM Novidea_HPC__Income__c
                                                WHERE Id IN :cancelledIncomeIdsToNewReceipts.keySet()];
        
        for(Novidea_HPC__Income__c income : cancelledIncomes){
            for(HPC_Collection__Receipt__c receipt : cancelledIncomeIdsToNewReceipts.get(income.Id)){
                for(HPC_Collection__Receipt__c cancelledReceipt : income.HPC_Collection__Receipts__r){
                    if(receipt.HPC_Collection__Effective_Date__c == cancelledReceipt.HPC_Collection__Effective_Date__c){
                        copyFields(receipt, cancelledReceipt);
                        break;
                    }
                }
            }
        }
    }

    @TestVisible
    private static void copyFields(HPC_Collection__Receipt__c receipt, HPC_Collection__Receipt__c cancelledReceipt){
        receipt.HPC_Collection__Commission_Amount__c = -cancelledReceipt.HPC_Collection__Commission_Amount__c;
        receipt.Discount__c = -cancelledReceipt.Discount__c;
        receipt.Commission_GST__c = -cancelledReceipt.Commission_GST__c;
        receipt.GST_Amount_on_Premium__c = -cancelledReceipt.GST_Amount_on_Premium__c;
        receipt.Net_Brokerage__c = -cancelledReceipt.Net_Brokerage__c;
        receipt.Other_Charges__c = -cancelledReceipt.Other_Charges__c;
        receipt.HPC_Collection__Premium_To_Pay__c = -cancelledReceipt.HPC_Collection__Premium_To_Pay__c;
        receipt.HPC_Collection__Net_Premium__c = -cancelledReceipt.HPC_Collection__Net_Premium__c;
        receipt.Other_Taxes__c = -cancelledReceipt.Other_Taxes__c;
        receipt.Payable_to_Insurer__c = -cancelledReceipt.Payable_to_Insurer__c;
        receipt.Policy_Charges__c = -cancelledReceipt.Policy_Charges__c;
        receipt.Service_Fee__c = -cancelledReceipt.Service_Fee__c;
        receipt.Service_Fee_GST__c = -cancelledReceipt.Service_Fee_GST__c;
        receipt.Client_Payable__c = -cancelledReceipt.Client_Payable__c;
        receipt.Commission_Amount_Discount__c = -cancelledReceipt.Commission_Amount_Discount__c;
        //receipt.GST_on_Brokerage_Base_Currency__c = -cancelledReceipt.GST_on_Brokerage_Base_Currency__c;
        receipt.Legal_Entity__c = cancelledReceipt.Legal_Entity__c;
        receipt.Legal_Entity_Unit_4_Code__c = cancelledReceipt.Legal_Entity_Unit_4_Code__c;
        receipt.Stamp_Duty__c = -cancelledReceipt.Stamp_Duty__c;
        receipt.Settlement_Commission_Discount_Amount__c = -cancelledReceipt.Settlement_Commission_Discount_Amount__c;
        receipt.Division__c = cancelledReceipt.Division__c; 
        receipt.Department__c = cancelledReceipt.Department__c;
        receipt.Waiting_for_Calculation__c = cancelledReceipt.Waiting_for_Calculation__c;
        receipt.HPC_Collection__Collection_Type__c = cancelledReceipt.HPC_Collection__Collection_Type__c;
    }
}
