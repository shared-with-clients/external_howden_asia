@isTest
public with sharing class PostingTriggerHelperTest {

    static void validationSetToTrueTest(){ 
        Novidea_HPC__Policy__c frontingPolicy = new Novidea_HPC__Policy__c(HPC_Collection__Payment_Schedule__c = 'Annual', Novidea_HPC__Collection_Manager__c = [SELECT Id FROM User WHERE Id != :UserInfo.getUserId() LIMIT 1].Id);
        Database.insert(frontingPolicy);
        Novidea_HPC__Policy__c policy = new Novidea_HPC__Policy__c(HPC_Collection__Payment_Schedule__c = 'Annual', Fronting_Policy__c = frontingPolicy.Id);
        Database.insert(policy);

        HPC_Collection__Receipt__c frontingReceipt = new HPC_Collection__Receipt__c(HPC_Collection__Policy__c = frontingPolicy.Id);
        HPC_Collection__Receipt__c receipt = new HPC_Collection__Receipt__c(HPC_Collection__Policy__c = policy.Id);
        Database.insert(new List<HPC_Collection__Receipt__c>{frontingReceipt, receipt});

        NIBA__Allocation__c allocation = new NIBA__Allocation__c();
        Database.insert(allocation);

        NIBA__Posting__c frontingPosting = new NIBA__Posting__c(NIHC__Receipt__c = frontingReceipt.Id, NIBA__Allocation_Ref__c = allocation.Id);
        NIBA__Posting__c posting = new NIBA__Posting__c(NIHC__Receipt__c = receipt.Id, NIBA__Posting_Type__c = 'Cash Payment');
        NIBA__Posting__c posting2 = new NIBA__Posting__c(NIHC__Receipt__c = receipt.Id, NIBA__Posting_Type__c = 'Cash Receipt');
        NIBA__Posting__c posting3 = new NIBA__Posting__c(NIHC__Receipt__c = receipt.Id, NIBA__Posting_Type__c = 'Journal');

        Database.insert(frontingPosting);
        Database.insert(new List<NIBA__Posting__c>{posting, posting2, posting3});

        posting = [
            SELECT Id, Validation__c
                    FROM NIBA__Posting__c
                    WHERE Id = :posting.Id
        ];

        List<AggregateResult> maxNumbers = [
			SELECT MAX(Receipting_Number__c) maxReceiptNumber, MAX(Payment_Number__c) maxPaymentNumber,
			MAX(Voucher_Number__c) maxVoucherNumber
			FROM NIBA__Posting__c
		];

        System.assert(posting.Validation__c == null);
        System.assertEquals(Integer.valueOf(maxNumbers[0].get('maxReceiptNumber')), 1, '1st posting of Cash Receipt type');
        System.assertEquals(Integer.valueOf(maxNumbers[0].get('maxPaymentNumber')), 1, '1st posting of Cash Payment type');
        System.assertEquals(Integer.valueOf(maxNumbers[0].get('maxVoucherNumber')), 1, '1st posting of Journal type');
    }

    static void validationSetToFalseTest(){ 
        Novidea_HPC__Policy__c frontingPolicy = new Novidea_HPC__Policy__c(HPC_Collection__Payment_Schedule__c = 'Annual', Novidea_HPC__Collection_Manager__c = [SELECT Id FROM User WHERE Id != :UserInfo.getUserId() LIMIT 1].Id);
        Database.insert(frontingPolicy);
        Novidea_HPC__Policy__c policy = new Novidea_HPC__Policy__c(HPC_Collection__Payment_Schedule__c = 'Annual', Fronting_Policy__c = frontingPolicy.Id);
        Database.insert(policy);

        HPC_Collection__Receipt__c frontingReceipt = new HPC_Collection__Receipt__c(HPC_Collection__Policy__c = frontingPolicy.Id);
        HPC_Collection__Receipt__c receipt = new HPC_Collection__Receipt__c(HPC_Collection__Policy__c = policy.Id);
        Database.insert(new List<HPC_Collection__Receipt__c>{ receipt});

        NIBA__Allocation__c allocation = new NIBA__Allocation__c();
        Database.insert(allocation);

        NIBA__Posting__c frontingPosting = new NIBA__Posting__c(NIHC__Receipt__c = frontingReceipt.Id, NIBA__Allocation_Ref__c = allocation.Id);
        NIBA__Posting__c posting = new NIBA__Posting__c(NIHC__Receipt__c = receipt.Id, NIBA__Posting_Type__c = 'Cash Payment');
        try{
            Database.insert(posting);
        }catch(Exception e){
            System.assert(e.getMessage().contains('Payment to Insurer is not allowed for a Reinsurance policy when not all Client payments were done on the Fronting policy'));
        }
    }

    @isTest
    static void testData(){
        Test.startTest();
        validationSetToTrueTest();
        validationSetToFalseTest();
        Test.stopTest();
    }

    @isTest 
    static void generateInstallmentNoteNumbersTest(){
        Test.startTest();
        RecordType accountRecordTypeCarrier = [SELECT Id FROM RecordType WHERE DeveloperName = 'Carrier' AND SobjectType = 'Account'];
        Account testAccountCarrier = new Account(
            Name = 'Test Account Carrier',
            Account_Full_Name__c = 'Carrier Account',
            RecordTypeId = accountRecordTypeCarrier.Id,
            NIBA__Re_Insurer_type__c = 'Non-Bureau Company',
            NIBA__Has_Financial_Implications__c = true,
            Novidea_HPC__Status__c = 'Approved',
            ShippingCountry = 'Singapore',
            Offshore__c = 'Offshore');

        RecordType accountRecordTypeClient = [SELECT Id FROM RecordType WHERE DeveloperName = 'Business' AND SobjectType = 'Account'];
        Account testAccountClient = new Account(
            Name = 'Test Account Client',
            Account_Full_Name__c = 'Client Account',
            NIBA__Has_Financial_Implications__c = true,
            RecordTypeId = accountRecordTypeClient.Id,
            Novidea_HPC__Status__c = 'Approved',
            ShippingCountry = 'United Kingdom');

        Database.insert(new List<Account>{testAccountCarrier, testAccountClient});


        Novidea_HPC__Policy__c policy = new Novidea_HPC__Policy__c(Novidea_HPC__Carrier__c = testAccountCarrier.Id, Novidea_HPC__Client__c = testAccountClient.Id);
        Database.insert(policy);
        Novidea_HPC__Product_Def__c prodDef = new Novidea_HPC__Product_Def__c(GST_Applicable_On_Brokerage__c = 'Standard Rated', Code__c = '123', 
        GST_Applicable_On_Premium__c = 'Yes', Life_Non_Life_Indicator__c = 'Life', PPW_Applicable__c = 'No');
        Database.insert(prodDef);
        Novidea_HPC__Product__c product = new Novidea_HPC__Product__c(Novidea_HPC__Product_Definition__c = prodDef.Id, Novidea_HPC__Policy__c = policy.Id, Novidea_HPC__Effective_Date__c = Date.today());
        Database.insert(product);
        policy.Novidea_HPC__Product_Definition__c = prodDef.Id;
        Database.update(policy);

        Map<String,Id> incomeRtIds = NOVU.RecordTypeUtils.getIdsByDevNameAndFullSObjType(new List<String>{'Regular', 'MTA', 'uwr_mta', 'uwr_regular', 'Fee'}, 'Novidea_HPC__Income__c');

        Novidea_HPC__Income__c income = new Novidea_HPC__Income__c(
            RecordTypeId = incomeRtIds.get('Regular'), 
            Novidea_HPC__Client__c = testAccountClient.Id, 
            Novidea_HPC__Policy__c = policy.Id, 
            Novidea_HPC__Carrier__c = testAccountCarrier.Id,
            Novidea_HPC__Number_of_Payments__c = 1,
            HPC_Collection__Period_Between_Payments__c = 12,
            HPC_Collection__First_Payment_Date__c = Date.today(),
            Premium_before_GST__c = 1000);

        Novidea_HPC__Income__c uwrIncome = new Novidea_HPC__Income__c(
            RecordTypeId = incomeRtIds.get('uwr_regular'), 
            Novidea_HPC__Client__c = testAccountClient.Id, 
            Novidea_HPC__Policy__c = policy.Id, 
            Novidea_HPC__Carrier__c = testAccountCarrier.Id,
            Novidea_HPC__Number_of_Payments__c = 1,
            HPC_Collection__Period_Between_Payments__c = 12,
            HPC_Collection__First_Payment_Date__c = Date.today(),
            Premium_before_GST__c = 1000);

        Novidea_HPC__Income__c feeIncome = new Novidea_HPC__Income__c(
            RecordTypeId = incomeRtIds.get('Fee'), 
            Novidea_HPC__Client__c = testAccountClient.Id, 
            Novidea_HPC__Policy__c = policy.Id, 
            Novidea_HPC__Carrier__c = testAccountCarrier.Id,
            Novidea_HPC__Number_of_Payments__c = 1,
            HPC_Collection__Period_Between_Payments__c = 12,
            HPC_Collection__First_Payment_Date__c = Date.today(),
            Premium_before_GST__c = 1000,
            Service_Fee__c = 100);
            
        Database.insert(new List<Novidea_HPC__Income__c>{income, uwrIncome, feeIncome});

        uwrIncome.Novidea_HPC__Client_Income__c = income.Id;
        Database.update(uwrIncome);

        Test.stopTest();
        List<HPC_Collection__Receipt__c> receipts = [SELECT Debit_Credit_note_No__c, HPC_Collection__Income__r.RecordType.DeveloperName FROM HPC_Collection__Receipt__c];
        List<NIBA__Posting__c> postings = new List<NIBA__Posting__c>();
        for(HPC_Collection__Receipt__c receipt : receipts){
            postings.add(New NIBA__Posting__c(NIHC__Receipt__c = receipt.Id));
        }
        Database.insert(postings);

        List<NIBA__Posting__c> postingsAfter = [SELECT Debit_Credit_note_No__c, NIHC__Receipt__c FROM NIBA__Posting__c];
        for(NIBA__Posting__c posting : postingsAfter){
            System.assert(posting.Debit_Credit_note_No__c != null, 'Posting didnt get a note number');
        }
    }
}
