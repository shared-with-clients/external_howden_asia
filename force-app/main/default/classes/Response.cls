// Title : Response
// Description : used for return value for most of methods
// 			     
// Parameters :	
// 		- status      - Type: String - 'success', 'error' or 'warning' - used for displaing result of action
// 		- responseObj - Type: depend on method - return object that is result of action
public class Response {
    
    @AuraEnabled 
    public String status      { get; set; }
    @AuraEnabled 
    public Object responseObj { get; set; }
    
    public Response() { }
    
    public Response(String pStatus, Object pResponseObj) {
        this.status = pStatus;
        this.responseObj = pResponseObj;
    }

}