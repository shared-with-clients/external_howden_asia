@isTest
public with sharing class InstallmentAutoNumberGeneratorTest {

    @TestSetup
    static void makeData(){
        Test.startTest();
        RecordType accountRecordTypeCarrier = [SELECT Id FROM RecordType WHERE DeveloperName = 'Carrier' AND SobjectType = 'Account'];
        Account testAccountCarrier = new Account(
            Name = 'Test Account Carrier',
            Account_Full_Name__c = 'Carrier Account',
            RecordTypeId = accountRecordTypeCarrier.Id,
            NIBA__Re_Insurer_type__c = 'Non-Bureau Company',
            NIBA__Has_Financial_Implications__c = true,
            Novidea_HPC__Status__c = 'Approved',
            ShippingCountry = 'Singapore',
            Offshore__c = 'Offshore');

        RecordType accountRecordTypeClient = [SELECT Id FROM RecordType WHERE DeveloperName = 'Business' AND SobjectType = 'Account'];
        Account testAccountClient = new Account(
            Name = 'Test Account Client',
            Account_Full_Name__c = 'Client Account',
            NIBA__Has_Financial_Implications__c = true,
            RecordTypeId = accountRecordTypeClient.Id,
            Novidea_HPC__Status__c = 'Approved',
            ShippingCountry = 'United Kingdom');

        Database.insert(new List<Account>{testAccountCarrier, testAccountClient});


        Novidea_HPC__Policy__c policy = new Novidea_HPC__Policy__c(Novidea_HPC__Carrier__c = testAccountCarrier.Id, Novidea_HPC__Client__c = testAccountClient.Id);
        Database.insert(policy);
        Novidea_HPC__Product_Def__c prodDef = new Novidea_HPC__Product_Def__c(GST_Applicable_On_Brokerage__c = 'Standard Rated', Code__c = '123', 
        GST_Applicable_On_Premium__c = 'Yes', Life_Non_Life_Indicator__c = 'Life', PPW_Applicable__c = 'No');
        Database.insert(prodDef);
        Novidea_HPC__Product__c product = new Novidea_HPC__Product__c(Novidea_HPC__Product_Definition__c = prodDef.Id, Novidea_HPC__Policy__c = policy.Id, Novidea_HPC__Effective_Date__c = Date.today());
        Database.insert(product);
        policy.Novidea_HPC__Product_Definition__c = prodDef.Id;
        Database.update(policy);

        Map<String,Id> incomeRtIds = NOVU.RecordTypeUtils.getIdsByDevNameAndFullSObjType(new List<String>{'Regular', 'MTA', 'uwr_mta', 'uwr_regular', 'Fee'}, 'Novidea_HPC__Income__c');

        Novidea_HPC__Income__c income = new Novidea_HPC__Income__c(
            RecordTypeId = incomeRtIds.get('Regular'), 
            Novidea_HPC__Client__c = testAccountClient.Id, 
            Novidea_HPC__Policy__c = policy.Id, 
            Novidea_HPC__Carrier__c = testAccountCarrier.Id,
            Novidea_HPC__Number_of_Payments__c = 1,
            HPC_Collection__Period_Between_Payments__c = 12,
            HPC_Collection__First_Payment_Date__c = Date.today(),
            Premium_before_GST__c = 1000);

        Novidea_HPC__Income__c uwrIncome = new Novidea_HPC__Income__c(
            RecordTypeId = incomeRtIds.get('uwr_regular'), 
            Novidea_HPC__Client__c = testAccountClient.Id, 
            Novidea_HPC__Policy__c = policy.Id, 
            Novidea_HPC__Carrier__c = testAccountCarrier.Id,
            Novidea_HPC__Number_of_Payments__c = 1,
            HPC_Collection__Period_Between_Payments__c = 12,
            HPC_Collection__First_Payment_Date__c = Date.today(),
            Premium_before_GST__c = 1000);

        Novidea_HPC__Income__c feeIncome = new Novidea_HPC__Income__c(
            RecordTypeId = incomeRtIds.get('Fee'), 
            Novidea_HPC__Client__c = testAccountClient.Id, 
            Novidea_HPC__Policy__c = policy.Id, 
            Novidea_HPC__Carrier__c = testAccountCarrier.Id,
            Novidea_HPC__Number_of_Payments__c = 1,
            HPC_Collection__Period_Between_Payments__c = 12,
            HPC_Collection__First_Payment_Date__c = Date.today(),
            Premium_before_GST__c = 1000,
            Service_Fee__c = 100);
            
        Database.insert(new List<Novidea_HPC__Income__c>{income, uwrIncome, feeIncome});

        uwrIncome.Novidea_HPC__Client_Income__c = income.Id;
        Database.update(uwrIncome);

        Test.stopTest();

    }
    @isTest
    static void testData(){
        Test.startTest();
        Novidea_HPC__Income__c income = [SELECT Id FROM Novidea_HPC__Income__c WHERE RecordType.DeveloperName = 'Regular' LIMIT 1];
        InstallmentAutoNumberGenerator.TransactionRow row = new InstallmentAutoNumberGenerator.TransactionRow();
        row.incomeId = income.Id;
        row.isUWR = false;
        Map<Id, String> noteNumbersMapScenario1 = InstallmentAutoNumberGenerator.generateNumbers(new List<InstallmentAutoNumberGenerator.TransactionRow>{row});

        Novidea_HPC__Income__c uwrIncome = [SELECT Id FROM Novidea_HPC__Income__c WHERE RecordType.DeveloperName = 'uwr_regular' LIMIT 1];
        InstallmentAutoNumberGenerator.TransactionRow row2 = new InstallmentAutoNumberGenerator.TransactionRow();
        row2.incomeId = uwrIncome.Id;
        row2.isUWR = true;
        Map<Id, String> noteNumbersMapScenario2 = InstallmentAutoNumberGenerator.generateNumbers(new List<InstallmentAutoNumberGenerator.TransactionRow>{row2});

        Novidea_HPC__Income__c FeeIncome = [SELECT Id FROM Novidea_HPC__Income__c WHERE RecordType.DeveloperName = 'Fee' LIMIT 1];
        InstallmentAutoNumberGenerator.TransactionRow row3 = new InstallmentAutoNumberGenerator.TransactionRow();
        row3.incomeId = FeeIncome.Id;
        row3.isUWR = false;
        Map<Id, String> noteNumbersMapScenario3 = InstallmentAutoNumberGenerator.generateNumbers(new List<InstallmentAutoNumberGenerator.TransactionRow>{row3});

        Novidea_HPC__Policy__c policy = [SELECT Id FROM Novidea_HPC__Policy__c LIMIT 1];
        policy.Reinsurance_Bro__c = 'Yes';
        policy.Reinsurance_Broker__c = Userinfo.getUserId();
        Database.update(policy);

		Map<Id, String> noteNumbersMap = InstallmentAutoNumberGenerator.generateNumbers(new List<InstallmentAutoNumberGenerator.TransactionRow>{row, row2, row3});
        Test.stopTest();
    }
}