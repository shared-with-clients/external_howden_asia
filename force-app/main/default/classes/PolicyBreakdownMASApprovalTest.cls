@isTest(seeAllData=false)
public with sharing class PolicyBreakdownMASApprovalTest {
    @TestSetup
	public static void makeData(){
        PolicyTriggerHelperTest.makeData();
    }

    @isTest
	public static void policyBreakdownCreateTest(){
        List<Novidea_HPC__Policy__c> policies = [SELECT Id FROM Novidea_HPC__Policy__c LIMIT 100];
        List<PolicyBreakdownMASApproval.PolicyBreakdownRow> rows = new List<PolicyBreakdownMASApproval.PolicyBreakdownRow>();
        for(Novidea_HPC__Policy__c policy : policies){
            PolicyBreakdownMASApproval.PolicyBreakdownRow row = new PolicyBreakdownMASApproval.PolicyBreakdownRow();
            row.policyId = policy.Id;
            rows.add(row);
        }
        PolicyBreakdownMASApproval.updateMASApproval(rows);
        system.assert(!rows.isEmpty(), 'no policies found');
    }

}
