@isTest
public without sharing class EnquiryConvertationControllerTest {
    
@isTest
    public static void validateConversionTest() {
        Profile p =[SELECT Id FROM Profile WHERE Name='Accounts Department'];
        User u = new User(Alias = 'standt1',Country='United Kingdom',Email='demo1@randomdemodomain.com',EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',LocaleSidKey='en_US',ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', UserName='apexTest@gmail.com'); 
        insert u;
        Account acc = new Account(Name='apexTestAcc', Novidea_HPC__Status__c = 'Approved');
        Account acc2 = new Account(Name='newAccount');
        system.RunAs(u)
        {
            insert acc;
            insert acc2;
        }

        Contact c = new Contact(AccountId = acc.Id, LastName = 'Razer');
        insert c;

        Contact c2 = new Contact(AccountId = acc2.Id, LastName = 'Brighton');
        insert c2;

        Novidea_HPC__Lead__c nov  = new Novidea_HPC__Lead__c(Novidea_HPC__Client__c = acc.Id);
        insert nov;
        
        EnquiryConvertationController.validateConversion(nov.Id);
        
        Novidea_HPC__Lead__c nov2  = new Novidea_HPC__Lead__c();
        //insert nov2;
        
        EnquiryConvertationController.validateConversion(nov2.Id);
        
        Novidea_HPC__Lead__c nov3  = new Novidea_HPC__Lead__c(Novidea_HPC__Client__c = acc2.Id);
        insert nov3;
        
        EnquiryConvertationController.validateConversion(nov3.Id);
    }
}