public with sharing class ChangeCollectionTypeController {

    public Novidea_HPC__Income__c currentIncome {get; set;}
    private Id recordId = null;

    public ChangeCollectionTypeController(ApexPages.StandardController controller) {
        recordId = controller.getRecord().Id;
        currentIncome = [SELECT Provisional_Bill__c FROM Novidea_HPC__Income__c WHERE Id =:recordId ];
    }

	public PageReference init() {
        if(currentIncome.Provisional_Bill__c != true){
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.Reverse_Transaction_Error));
            return null;
        }
        else {
            return new PageReference('/apex/NIHC__ContraOfTransaction?Id=' + recordId);
        }
    }
    
    public PageReference back(){ 
        return new PageReference('/'+ recordId);
    }	
}
