@isTest(seeAllData = false)
public class TestGenerateDocumentController {
    
    @isTest
    static void testGenerateDocumentNotAllow() {
        Novidea_HPC__Proposal__c proposal = new Novidea_HPC__Proposal__c();
        Database.insert(proposal);

        ApexPages.StandardController stdController = new ApexPages.StandardController(proposal);
        
        GenerateDocumentController controller = new GenerateDocumentController(stdController);
        controller.init();
        
        System.assert((ApexPages.getMessages())[0].getSummary().contains(Label.Perform_Error));
    }
    
    @isTest
    static void testGenerateDocumentAllow() {
        Novidea_HPC__Proposal__c proposal = new Novidea_HPC__Proposal__c(Reviewed__c = true);
        Database.insert(proposal);

        ApexPages.StandardController stdController = new ApexPages.StandardController(proposal);
        
        GenerateDocumentController controller = new GenerateDocumentController(stdController);
        PageReference pg = controller.init();
        System.assertEquals('/apex/NPERFORM__generatePdf?Id=' + proposal.id, pg.getURL());
 
        pg = controller.back();
        System.assertEquals('/' + proposal.id, pg.getURL());
    }
}