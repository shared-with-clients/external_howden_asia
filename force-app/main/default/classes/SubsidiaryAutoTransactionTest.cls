@isTest
public with sharing class SubsidiaryAutoTransactionTest {

    @TestSetup
    static void makeData(){
        RecordType accountRecordTypeCarrier = [SELECT Id FROM RecordType WHERE DeveloperName = 'Carrier' AND SobjectType = 'Account'];
        Account testAccountCarrier = new Account(
            Name = 'Test Account Carrier',
            RecordTypeId = accountRecordTypeCarrier.Id,
            NIBA__Re_Insurer_type__c = 'Non-Bureau Company',
            ShippingCountry = 'Singapore');

        RecordType accountRecordTypeClient = [SELECT Id FROM RecordType WHERE DeveloperName = 'Business' AND SobjectType = 'Account'];
        Account testAccountClient = new Account(
            Name = 'Test Account Client',
            RecordTypeId = accountRecordTypeClient.Id,
            ShippingCountry = 'United Kingdom');

        Database.insert(new List<Account>{testAccountCarrier, testAccountClient});


        Novidea_HPC__Policy__c policy = new Novidea_HPC__Policy__c(Novidea_HPC__Carrier__c = testAccountCarrier.Id, Novidea_HPC__Client__c = testAccountClient.Id);
        Database.insert(policy);
        Novidea_HPC__Product__c product = new Novidea_HPC__Product__c(Novidea_HPC__Policy__c = policy.Id, Novidea_HPC__Effective_Date__c = Date.today());
        Database.insert(product);
        Novidea_HPC__Account_Risk__c subsidiary = new Novidea_HPC__Account_Risk__c(Novidea_HPC__Product__c = product.Id, Novidea_HPC__Account__c = testAccountClient.Id, Novidea_HPC__Client__c = true);
        Database.insert(subsidiary);

        RecordType incomeRecordType = [SELECT Id FROM RecordType 
            WHERE DeveloperName = 'Regular' 
                AND IsActive = true 
                AND SobjectType = 'Novidea_HPC__Income__c'];

        Novidea_HPC__Income__c income = new Novidea_HPC__Income__c(
            RecordTypeId = incomeRecordType.Id, 
            Novidea_HPC__Client__c = testAccountClient.Id, 
            Novidea_HPC__Policy__c = policy.Id, 
            Novidea_HPC__Carrier__c = testAccountCarrier.Id,
            Novidea_HPC__Number_of_Payments__c = 1,
            HPC_Collection__Period_Between_Payments__c = 12,
            HPC_Collection__First_Payment_Date__c = Date.today(),
            Premium_before_GST__c = 1000);
            
        Database.insert(income);

    }

    @isTest
    static void testData(){
        Test.startTest();
        Map<Id, Novidea_HPC__Account_Risk__c> ids = new Map<Id, Novidea_HPC__Account_Risk__c>(
            [SELECT Id 
            FROM Novidea_HPC__Account_Risk__c 
            WHERE Novidea_HPC__Product__r.Novidea_HPC__Effective_Date__c != null
            LIMIT 1]);

        SubsidiaryAutoTransaction.createSubsidiaryAutoTransaction(new List<Id>(ids.keyset()));

        Test.stopTest();
    }

    public SubsidiaryAutoTransactionTest() {

    }
}