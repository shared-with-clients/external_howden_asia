public with sharing class ActionQueueablePageController {
    /** Constant value for creation reason */
	private static final String CREATION_REASON = 'Creation';//TODO: label this.
	/** The ID of the transaction to negate */
	public Id returnToId {set; get;}
	/** Boolean value to disable Replace Transaction button after the action */
	public boolean isSaveButtonDisabled {set; get;}
	/** job Id */
    public Id jobId {get; set;}
    /**job object */
	public AsyncApexJob job {get; set;}
	/** width of progess bar*/
    public Decimal progressBarWidth {get;set;}
    /** status message of batch */
    public String strStatus {get;set;}
    /** progess bar enabled */
	public Boolean bProcess {get;set;}

    public ActionQueueablePageController(){
		// policyId = controller.getId();
		init();
    }
    
    private void init(){
		isSaveButtonDisabled = true;
		progressBarWidth = 0;
        strStatus = '';
		bProcess = true;
		jobId = System.currentPageReference().getParameters().get('jobId');
		returnToId = System.currentPageReference().getParameters().get('returnToId');
		checkStatusProgressBar();
	}

    public PageReference checkStatusProgressBar(){
		// Caluclating Percentage value
		system.debug(jobId);
        if(bProcess == false || jobId == null){
            return null;
		}
		
        this.job = [SELECT Status, ExtendedStatus, NumberOfErrors FROM AsyncApexJob WHERE Id=:jobId];
		switch on this.job.Status{
			when 'Queued'{
				progressBarWidth = 5;
			}
			when 'Preparing'{
				progressBarWidth = 20;
			}
			when 'Processing'{
				progressBarWidth = 75;
			}
			when 'Completed'{
				progressBarWidth = 100;
			}
			when 'Failed'{
				progressBarWidth = 100;
			}
		}
		strStatus = this.job.Status;
        if(progressBarWidth == 100 || Test.isRunningTest()) {
            bProcess = false;
            if((this.job != null && this.job.Status == 'Failed')){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, this.job.ExtendedStatus));
            }else{
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, ' Action Completed'));
            }
        }
        return null;
    }

	@AuraEnabled
    public static String createActionQueueable(String dataInputJson, String dispatcherName){
        ActionQueueable ActionQueueable = new ActionQueueable(dataInputJson, dispatcherName);		
		return System.enqueueJob(ActionQueueable);
	}
	
	@AuraEnabled
    public static Boolean validateAction(String dataInputJson, String dispatcherName){
		List<NORE.RuleEngineIdInvoker.InvocationRow> ruleRows = new List<NORE.RuleEngineIdInvoker.InvocationRow>();
		NORE.RuleEngineIdInvoker.InvocationRow row = new NORE.RuleEngineIdInvoker.InvocationRow();
		Map<String, Object> data = (Map<String, Object>)JSON.deserializeUntyped(dataInputJson);
		row.recordId = (String)data.get('recordId');
		row.useConnectingObjects = false; //Consider adding a checkbox on the receipt for this.
		if(String.isNotBlank(Label.Carrier_Split_Validator)){
			row.categoryExternalIdsAndIds = Label.Carrier_Split_Validator.split(',');
		}
		
		row.recordsToInsert = new List<SObject>();
		row.recordsToUpdate = new List<SObject>();
		row.shouldPersist = false;//TODO: should we set this to true instead?
		ruleRows.add(row);
		NORE.RuleEngineIdInvoker.runEngine(ruleRows);
		return true;
    }
    
	public Pagereference back(){//TODO: Should we change to lightning url format?
		Pagereference pg = new Pagereference ('/' + this.returnToId);
		pg.setRedirect (true);
		return pg;
	}
}
