@isTest
public class QuotationConvertationControllerTest {

    @isTest
    public static void validateConversionTest() {
        Profile p =[SELECT Id FROM Profile WHERE Name='Accounts Department'];
        User u = new User(Alias = 'standt1',Country='United Kingdom',Email='demo1@randomdemodomain.com',EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',LocaleSidKey='en_US',ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', UserName='apexTest@gmail.com'); 
        insert u;
        Account acc = new Account(Name='apexTestAcc', Novidea_HPC__Status__c = 'Approved');
        system.RunAs(u)
        {
            insert acc;
        }
        Novidea_HPC__Application__c app = new Novidea_HPC__Application__c(Name = 'asdb', Novidea_HPC__Account__c = acc.Id);
        insert app;
        Novidea_HPC__Proposal__c prop = new Novidea_HPC__Proposal__c(Novidea_HPC__Application__c = app.Id);
        insert prop;
        QuotationConvertationController.validateConversion(app.Id);
        QuotationConvertationController.validateConversion('invald Id');
    }
}