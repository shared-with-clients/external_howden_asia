@isTest(seeAllData = false)
public with sharing class PolicyTriggerHelperTest {
	private static final String RECEIVED = 'Received';
	private static final String ACCEPTED = 'Accepted';

	@TestSetup
	public static void makeData(){
		Integer size = 3; 
        Map<String,Id> accountRtIds = NOVU.RecordTypeUtils.getIdsByDevNameAndFullSObjType(new List<String>{'Carrier', 'Business', 'Legal_Entity'}, 'Account');
		Id rfpRecordTypeId = NOVU.RecordTypeUtils.getIdByDevNameAndFullSObjType('RFP', 'Novidea_HPC__Request_for_Proposal__c');
		List<Account> approvedAccounts = new List<Account>();
		List<Account> businessAccounts = new List<Account>();
		List<Account> carrierAccounts = new List<Account>();
		List<Novidea_HPC__Request_for_Proposal__c> rfqs = new List<Novidea_HPC__Request_for_Proposal__c>();
		List<Novidea_HPC__Product__c> rfqProducts = new List<Novidea_HPC__Product__c>();
		List<Novidea_HPC__Proposal__c> proposals = new List<Novidea_HPC__Proposal__c>();
		List<Novidea_HPC__Product__c> propProducts = new List<Novidea_HPC__Product__c>();
		for(Integer i = 0; i < size; i++){
			approvedAccounts.add(new Account(Account_Full_Name__c ='Account ' + i, Name ='Account ' + i, NIBA__Re_Insurer_type__c = 'Non-Bureau Company', Novidea_HPC__Status__c = 'Approved', BillingStreet = 'Shalom', 
				RecordTypeId = accountRtIds.get('Business')));
			businessAccounts.add(new Account(Account_Full_Name__c = 'client'+ i,  Name = 'client'+ i, NIBA__Re_Insurer_type__c = 'Non-Bureau Company', Novidea_HPC__Tax_ID__c = '345', BillingStreet = 'Shalom',
				Location_Type__c='Domestic',  RecordTypeId = accountRtIds.get('Business')));
			carrierAccounts.add(new Account(Account_Full_Name__c = 'carrier'+ i,  Name = 'carrier'+ i, NIBA__Re_Insurer_type__c = 'Non-Bureau Company', RecordTypeId = accountRtIds.get('Carrier'), BillingStreet = 'Shalom',
				Novidea_HPC__Agreed_Date__c = Date.today().addDays(-7), Offshore__c = 'Offshore', Novidea_HPC__Status__c = 'Unapproved',
				Novidea_HPC__Business_Effective_Date__c = Date.today().addMonths(-1), Novidea_HPC__Deactivation_Date__c = Date.today().addMonths(1)));
        }
		
        Account legalEntity = new Account(
			Novidea_HPC__Status__c = 'Approved',
			Account_Full_Name__c = 'Howden Singapore', 
			Name = 'Howden Singapore', 
			Fee_Brokerage__c = true, 
			Account_Currency__c = 'USD', 
			RecordTypeId = accountRtIds.get('Legal_Entity'), 
			NIBA__Account_Type__c = 'Other P&L'
		);
		
        
		List<Account> accountToInsert = new List<Account>();
		accountToInsert.addAll(approvedAccounts);
		accountToInsert.addAll(businessAccounts);
        accountToInsert.addAll(carrierAccounts);
        accountToInsert.add(legalEntity);
		Database.insert(accountToInsert);

		List<Novidea_HPC__Application__c> applications = new List<Novidea_HPC__Application__c>();
		for(Integer i = 0; i < size; i++){
			applications.add(new Novidea_HPC__Application__c(Name='Application' + i, Novidea_HPC__Account__c = approvedAccounts[i].Id, Legal_Entity__c = legalEntity.Id));
		}
		Database.insert(applications);

		List<Novidea_HPC__Product_Category__c> productsCategory = new list<Novidea_HPC__Product_Category__c>(); 
		for(Integer i = 0; i < size; i++){	
			productsCategory.add(new Novidea_HPC__Product_Category__c(name='Test Product Category' + i, Code__c = 'Cat'));
		}
		Database.insert(productsCategory);

		List<Novidea_HPC__Product_Def__c> productdefs = new list<Novidea_HPC__Product_Def__c>(); 
		for(Integer i = 0; i < size; i++){			
			productdefs.add(new Novidea_HPC__Product_Def__c(
				Name='products Def' + i, 
				Novidea_HPC__Category__c= productsCategory[i].Id,
				Code__c = 'Def',
				PPW_Applicable__c = 'Yes',
				Life_Non_Life_Indicator__c = 'Life',
				GST_Applicable_On_Premium__c = 'Yes',
				GST_Applicable_On_Brokerage__c = 'Standard Rated'
			));
		}
		Database.insert(productdefs);

		Novidea_HPC__Process__c process = new Novidea_HPC__Process__c();
		Database.insert(process);

		Novidea_HPC__Process_Connection__c processCon = new Novidea_HPC__Process_Connection__c(Novidea_HPC__Process__c = process.Id, Novidea_HPC__Product_Def__c = productdefs[0].id);
		Database.insert(processCon);

		List<Novidea_HPC__Product__c> products = new list<Novidea_HPC__Product__c>(); 
		for(Integer i = 0; i < size; i++){			
				products.add(new Novidea_HPC__Product__c(Name='Product' + i,
					Novidea_HPC__Product_Definition__c = productdefs[i].Id,
					Novidea_HPC__Effective_Date__c = Date.today(), Novidea_HPC__Expiration_Date__c = Date.today().addDays(364)));			
		} 
		database.insert(products);
		
		rfqs.add(new Novidea_HPC__Request_for_Proposal__c(RecordTypeId = rfpRecordTypeId, CurrencyIsoCode = 'SGD',Product_Main_Class__c = 'FIN', Name__c = 'prdct/FIN/', Name='rfp1', Novidea_HPC__Application__c = applications[0].id, Novidea_HPC__Carrier__c = carrierAccounts[0].Id, Novidea_HPC__Status__c = 'Proposal Received'));
		Database.insert(rfqs);
		
		rfqProducts.add(new Novidea_HPC__Product__c(name='rfpProd1', Novidea_HPC__Product_Definition__c = productdefs[0].id, Novidea_HPC__Request_for_Proposal__c = rfqs[0].id, Novidea_HPC__Product__c = products[0].id));
		Database.insert(rfqProducts);

		// Approval.ProcessSubmitRequest rfqApproval = new Approval.ProcessSubmitRequest();
		// rfqApproval.setProcessDefinitionNameOrId('RFP_MASApproval_Checked');
		// rfqApproval.setObjectId(rfqs[0].Id);
		// rfqApproval.setSkipEntryCriteria(true);
		// Approval.ProcessResult rfqApprovalResult = Approval.process(rfqApproval);
		// System.debug(rfqApprovalResult);

		List<ProcessInstanceWorkitem> workItems = [
            SELECT Id, ProcessInstanceId 
            FROM ProcessInstanceWorkitem 
            WHERE ProcessInstance.TargetObjectId IN :rfqs
        ];
        System.debug(workItems);
        List<Approval.ProcessWorkitemRequest> requests = new List<Approval.ProcessWorkitemRequest>();
        for(ProcessInstanceWorkitem workItem : workItems){
        Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
        req.setWorkitemId(workItem.Id);
        req.setAction('Approve');
        req.setComments('Your Comment.');
        requests.add(req);
        }
        System.debug(requests);
        Approval.ProcessResult[] processResults = Approval.process(requests);
		System.debug(processResults);
		
		workItems = [
            SELECT Id, ProcessInstanceId 
            FROM ProcessInstanceWorkitem 
            WHERE Id IN :processResults[0].getNewWorkitemIds()
		];
		
		System.debug(workItems);
        requests = new List<Approval.ProcessWorkitemRequest>();
        for(ProcessInstanceWorkitem workItem : workItems){
        Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
        req.setWorkitemId(workItem.Id);
        req.setAction('Approve');
        req.setComments('Your Comment.');
        requests.add(req);
        }
        System.debug(requests);
        processResults = Approval.process(requests);
		System.debug(processResults);

		proposals.add(new Novidea_HPC__Proposal__c(MAS_Approval_Required__c = true, Name = 'prop0', Novidea_HPC__Status__c = RECEIVED, Novidea_HPC__Application__c = applications[0].id, Novidea_HPC__Carrier__c = carrierAccounts[0].Id, Novidea_HPC__Request_for_Proposal__c = rfqs[0].Id));
		Database.insert(proposals);

		propProducts.add(new Novidea_HPC__Product__c(Name = 'pr1', Novidea_HPC__Product_Definition__c = productdefs[0].id, Novidea_HPC__Status__c = ACCEPTED, Novidea_HPC__Proposal__c = proposals[0].id, Novidea_HPC__Product__c = rfqProducts[0].id));
		Database.insert(propProducts);

		PolicyTriggerHelper.stopTrigger = true;
		List<Novidea_HPC__Policy__c>  masterPolicys = new List<Novidea_HPC__Policy__c>();
		Map<String, Id> policyRecordTypesMap = NOVU.RecordTypeUtils.getIdsByDevNameAndFullSObjType(new String[]{'Policy','Multi_Section','Section', 'Shell_Policy'},'Novidea_HPC__Policy__c');
		for(Integer i=0; i<3; i++){	 
			masterPolicys.add(new Novidea_HPC__Policy__c(RecordTypeID = policyRecordTypesMap.get('Policy'), //change to master Shell_Policy
				Novidea_HPC__Carrier__c = carrierAccounts[i].ID, Novidea_HPC__Policy_Number__c = '1234', Name='master policy' + i, 
				Novidea_HPC__Commission__c = 10, Novidea_HPC__Expiration_Date__c = Date.today(),
				Novidea_HPC__Effective_Date__c = Date.today().addYears(-1)));
	
		}
		Database.insert(masterPolicys);

		List<Novidea_HPC__Policy__c> policys = new List<Novidea_HPC__Policy__c>(); 
		for(Integer i=0; i<propProducts.size(); i++){			
			Policys.add(new Novidea_HPC__Policy__c(Name='Policy' + i,
				Novidea_HPC__Master_Policy__c = masterPolicys[i].Id,
				Novidea_HPC__Effective_Date__c = Date.today(),
				Novidea_HPC__Application__c = applications[i].Id,
				Novidea_HPC__Product__c = propProducts[i].Id,
				Novidea_HPC__Product_Definition__c = productdefs[0].id,
				Novidea_HPC__Client__c = businessAccounts[i].Id,
				Client_Cedant_Code__c = null,
				Risk_Location__c = 'Domestic',
				MAS_Approved_Required__c = false,
				Novidea_HPC__Expiration_Date__c = Date.today().addDays(364)));
		} 
		Database.insert(policys);
		Database.update(policys);

		PolicyTriggerHelper.stopTrigger = false;
		List<Novidea_HPC__Policy_Breakdown__c> policysBreakdown = new List<Novidea_HPC__Policy_Breakdown__c>(); 
		for(Integer i=0; i<policys.size(); i++){		
			policysBreakdown.add(new Novidea_HPC__Policy_Breakdown__c(
				Name='Breakdown of policy '+ i,
				Novidea_HPC__Carrier__c =  carrierAccounts[i].Id,
				Novidea_HPC__Policy__c = policys[i].Id
			));
		}
		policysBreakdown[0].Lead_Insurer__c = true;
		Database.insert(policysBreakdown);

		List<Novidea_HPC__Account_Risk__c> subsidiaries = new List<Novidea_HPC__Account_Risk__c>();
		for(Integer i=0; i<2; i++){		
			subsidiaries.add(new Novidea_HPC__Account_Risk__c(
				Novidea_HPC__Order__c = 50,
				Novidea_HPC__Client__c = true,
				Novidea_HPC__Product__c = propProducts[0].Id,
				Novidea_HPC__Account__c = businessAccounts[0].Id
			));
		}

		propProducts[0].Novidea_HPC__Policy__c = policys[0].Id;
		Database.insert(subsidiaries);
		Database.update(propProducts);

		Novidea_HPC__Trigger__c triggerCS = Novidea_HPC__Trigger__c.getInstance();
		triggerCS.Novidea_HPC__Stop_Validation_Rule__c  = true;
		upsert triggerCS;
	}

	@isTest
	public static void policyTrigerBeforeInsertTest(){
		List<Novidea_HPC__Policy__c> newPolicies = [SELECT Novidea_HPC__Product__c, Novidea_HPC__Expiration_Date__c,
			 Novidea_HPC__Product_Definition__c, Risk_Location__c, Client_Cedant_Code__c, 
			 Novidea_HPC__Client__c, Novidea_HPC__Application__c, Novidea_HPC__Effective_Date__c, Legal_Entity__c
			FROM Novidea_HPC__Policy__c];
		Map<Id, Novidea_HPC__Policy__c> newPoliciesMap = new Map<Id, Novidea_HPC__Policy__c>(newPolicies);
		
		PolicyTriggerHelper.onBeforeInsert(newPolicies, null);
		PolicyTriggerHelper.assignClientCedantCode(newPolicies, null);
	}

	@isTest
	public static void policyTrigerBeforeUpdateTest(){
		List<Novidea_HPC__Policy__c> newPolicies = [SELECT Id, Risk_Location__c, Novidea_HPC__Status__c, Novidea_HPC__Effective_Date__c, Legal_Entity__c, Novidea_HPC__Product__c, Novidea_HPC__Product_Definition__c, Novidea_HPC__Process__c, Client_Cedant_Code__c, Novidea_HPC__Client__c, MAS_Approved_Required__c FROM Novidea_HPC__Policy__c];
		for(Novidea_HPC__Policy__c policy : newPolicies){
			if(policy.Novidea_HPC__Status__c != 'Placement confirmed'){
				policy.Novidea_HPC__Status__c = 'Placement confirmed';
			}
			if(policy.Legal_Entity__c != null) {
				createTaxParam(policy.Legal_Entity__c);
			}
			policy.Client_Cedant_Code__c = null;
		}

		// newPolicies[0].Novidea_HPC__Product_Definition__c = null;
		Map<Id, Novidea_HPC__Policy__c> newPoliciesMap = new Map<Id, Novidea_HPC__Policy__c>(newPolicies);
		List<Novidea_HPC__Policy__c> oldPolicies = [SELECT Id, Risk_Location__c, Novidea_HPC__Status__c, Novidea_HPC__Product__c, Novidea_HPC__Product_Definition__c, Novidea_HPC__Process__c, Client_Cedant_Code__c, Novidea_HPC__Client__c, MAS_Approved_Required__c FROM Novidea_HPC__Policy__c];
		Map<Id, Novidea_HPC__Policy__c> oldPoliciesMap = new Map<Id, Novidea_HPC__Policy__c>(oldPolicies);
		
		PolicyTriggerHelper.onBeforeUpdate(newPolicies, oldPolicies, newPoliciesMap, oldPoliciesMap);

		Account client = [SELECT Id, Client_Code__c FROM Account WHERE Client_Code__c != null LIMIT 1];
		Novidea_HPC__Policy__c clientCodePolicy = [SELECT Id FROM Novidea_HPC__Policy__c WHERE Name = 'master policy2' LIMIT 1];
		clientCodePolicy.Novidea_HPC__Client__c = client.Id;
		clientCodePolicy.Client_Cedant_Code__c = null;
		Database.update(new List<Novidea_HPC__Policy__c>{clientCodePolicy});
		clientCodePolicy = [SELECT Id, Client_Cedant_Code__c FROM Novidea_HPC__Policy__c WHERE Id =: clientCodePolicy.Id LIMIT 1];

		//assignClientCedantCode method
		System.assertEquals(clientCodePolicy.Client_Cedant_Code__c, client.Client_Code__c, 'Expected client code to be copied over');

		Novidea_HPC__Policy__c policy = [SELECT Id, Total_percentage_paid_by_Subsidiaries__c, MAS_Approved_Required__c, Novidea_HPC__Status__c, Novidea_HPC__Product__c, Novidea_HPC__Product_Definition__c, Novidea_HPC__Process__c, Client_Cedant_Code__c, Novidea_HPC__Client__c FROM Novidea_HPC__Policy__c WHERE Novidea_HPC__Client__c != null LIMIT 1];
		//validateMASApproval method
		System.assertEquals(policy.MAS_Approved_Required__c, true, 'Expected approved required to be true');

		//placementConfirmedLogic method
		// Novidea_HPC__Policy__c placementConfirmedPolicy = [SELECT Id, Total_amount_paid_by_Subsidiaries__c, MAS_Approved_Required__c, Novidea_HPC__Status__c, Novidea_HPC__Product__c, Novidea_HPC__Product_Definition__c, Novidea_HPC__Process__c, Client_Cedant_Code__c, Novidea_HPC__Client__c FROM Novidea_HPC__Policy__c WHERE Name = 'Policy0' LIMIT 1];
		// Account approved = [SELECT Id FROM Account WHERE Novidea_HPC__Status__c = 'Approved' LIMIT 1];
		// placementConfirmedPolicy.Novidea_HPC__Client__c = approved.Id;
		// placementConfirmedPolicy.Novidea_HPC__Status__c = 'New';
		// Database.update(placementConfirmedPolicy);
		// placementConfirmedPolicy.Novidea_HPC__Status__c = 'Send to Review';
		// Database.update(placementConfirmedPolicy);
		// placementConfirmedPolicy.Novidea_HPC__Status__c = 'Sent to Insurer';
		// Database.update(placementConfirmedPolicy);
		// placementConfirmedPolicy.Novidea_HPC__Status__c = 'Placement confirmed';
		// Database.update(placementConfirmedPolicy);
		// placementConfirmedPolicy = [SELECT Id, Total_amount_paid_by_Subsidiaries__c FROM Novidea_HPC__Policy__c WHERE Name = 'Policy0' LIMIT 1];

		// System.assert(placementConfirmedPolicy.Total_amount_paid_by_Subsidiaries__c != null, 'Expected Total_amount_paid_by_Subsidiaries__c to be populated');

		Novidea_HPC__Trigger__c triggerCS = Novidea_HPC__Trigger__c.getInstance();
		triggerCS.Novidea_HPC__Stop_Validation_Rule__c  = false;
		upsert triggerCS;

		//validateQuotation method
		Novidea_HPC__Application__c unapprovedQuotation = new Novidea_HPC__Application__c(Novidea_HPC__Status__c = 'New');
		Database.insert(unapprovedQuotation);
		Novidea_HPC__Policy__c clientUnapprovedPolicy = new Novidea_HPC__Policy__c(Name='clientUnapprovedPolicy',
		Novidea_HPC__Effective_Date__c = Date.today(),
		Client_Cedant_Code__c = null,
		Novidea_HPC__Application__c = unapprovedQuotation.Id,
		Risk_Location__c = 'Domestic',
		MAS_Approved_Required__c = false,
		Novidea_HPC__Expiration_Date__c = Date.today().addDays(364));
		try{
			Database.insert(clientUnapprovedPolicy);
		}catch(Exception ex){
			System.debug(ex.getMessage());
			System.Assert(ex.getMessage().contains('the account status must be approved'), 'Expected validation is Client Unapproved');
		}
	}

	@isTest
	public static void testSetIaLevyTaxAndCapMissingConfiguration() {
		Novidea_HPC__Policy__c newPolicy = [SELECT Id 
													FROM Novidea_HPC__Policy__c
													LIMIT 1];

		newPolicy.Novidea_HPC__Status__c = 'Placement confirmed';
		try{
			Database.update(newPolicy);
		}catch(Exception ex){
			System.debug(ex.getMessage());
			System.Assert(ex.getMessage().contains(System.Label.Missing_Tax_Configuration), 'Expected validation is: ' + System.Label.Missing_Tax_Configuration);
		}
	}

	@isTest
	public static void testSetIaLevyTaxAndCapMultiConfiguration() {
		testSetIaLevyTaxAndCap(true);
	}

	@isTest
	public static void testSetIaLevyTaxAndCapSuccess() {
		testSetIaLevyTaxAndCap(false);
	}


	public static void testSetIaLevyTaxAndCap(Boolean multiConfiguration) {
		Novidea_HPC__Policy__c newPolicy = [SELECT Id 
													FROM Novidea_HPC__Policy__c
													LIMIT 1];

		Account acc = [SELECT Id 
					   FROM Account 
					   WHERE Name = 'Howden Singapore'];

		createTaxParam(acc.Id);
		if(multiConfiguration) {
			createTaxParam(acc.Id);
		}

		Novidea_HPC__Product_Def__c  proddef = [
			SELECT Id, Life_Non_Life_Indicator__c 
			FROM Novidea_HPC__Product_Def__c 
			LIMIT 1];

		newPolicy.Novidea_HPC__Status__c = 'Placement confirmed';
		newPolicy.Legal_Entity__c = acc.Id;
		newPolicy.Novidea_HPC__Product_Definition__c = proddef.Id;

		try{
			Database.update(newPolicy);
		}catch(Exception ex){
			System.Assert(ex.getMessage().contains(System.Label.Found_More_Than_One_Record_In_Tax_Configuration), 'Expected validation is: ' + System.Label.Found_More_Than_One_Record_In_Tax_Configuration);
		}

		if(!multiConfiguration) {
			Novidea_HPC__Policy__c policy = [SELECT IA_Levy_Percentage__c, IA_Levy_Cap__c 
											FROM Novidea_HPC__Policy__c 
											WHERE Id = :newPolicy.Id];

			System.assertEquals(7, policy.IA_Levy_Percentage__c);
			System.assertEquals(5, policy.IA_Levy_Cap__c);
		}
	}

	private static void createTaxParam(Id legalEntity) {
		Tax_Parameters__c taxParam = new Tax_Parameters__c(Name = 'Test Tax',
														   Legal_Entity__c = legalEntity, 
														   Tax_Type__c = 'IA Levy',
														   Life_Cap__c = 5,
														   Non_Life_Cap__c = 10,
														   Percentage__c = 7,
														   From_Effective_Date__c = Date.today().addYears(-2),
														   To_Effective_Date__c = Date.today().addYears(1));

		Database.insert(taxParam);
	}

	// @isTest
	// public static void testCoverage(){
	// 	PolicyTriggerHelper.coverageCode();
	// }

	// @isTest
	// private static void invalidQuatationTest(){
	// 	Account client = [SELECT Id FROM Account WHERE Name = 'client0'];
	// 	Novidea_HPC__Application__c application = new Novidea_HPC__Application__c(
	// 		Name='Unapproved Application', Novidea_HPC__Account__c = client.Id);
	// 	Database.insert(application);

	// 	Test.startTest();
	// 	Novidea_HPC__Policy__c policy = new Novidea_HPC__Policy__c(
	// 		Name = 'Policy',
	// 		Novidea_HPC__Effective_Date__c = Date.today(),
	// 		Novidea_HPC__Client__c = client.Id,
	// 		Novidea_HPC__Application__c = application.Id,
	// 		Client_Cedant_Code__c = null,
	// 		Risk_Location__c = 'Domestic',
	// 		MAS_Approved_Required__c = false,
	// 		Novidea_HPC__Expiration_Date__c = Date.today().addDays(364));
	// 	try{
	// 		Database.insert(policy);
	// 		// policy = [SELECT Validation__c FROM Novidea_HPC__Policy__c WHERE Id = :policy.Id];
	// 		// System.assertEquals('Client Unapproved', policy.Validation__c, 'Expected client unapproved error message');
	// 		System.assert(false, 'The validation "Client Unapproved" should cause a DML exception');
	// 	}catch(Exception validationExc){
	// 		System.assert(validationExc.getMessage().contains('the account status must be approved'),
	// 			'Validation message of "Converted_Proposal"');
	// 	}
	// 	Test.stopTest();
	// }

	// @isTest
	// private static void cedantUpdatesTest(){
	// 	Account client = [SELECT Client_Code__c FROM Account WHERE Name = 'account0'];
	// 	Novidea_HPC__Application__c application = [SELECT Id FROM Novidea_HPC__Application__c WHERE Name = 'Application0'];
	// 	Novidea_HPC__Product__c propProd = [SELECT Id FROM Novidea_HPC__Product__c WHERE Novidea_HPC__Proposal__r.Novidea_HPC__Application__r.Name = 'Application0'];
	// 	Test.startTest();
	// 	Novidea_HPC__Policy__c policy = new Novidea_HPC__Policy__c(
	// 		Name = 'Policy',
	// 		Novidea_HPC__Effective_Date__c = Date.today(),
	// 		Novidea_HPC__Application__c = application.Id,
	// 		Novidea_HPC__Client__c = client.Id,
	// 		Novidea_HPC__Product__c = propProd.Id,
	// 		Client_Cedant_Code__c = null,
	// 		Risk_Location__c = 'Domestic',
	// 		MAS_Approved_Required__c = false,
	// 		Novidea_HPC__Status__c = 'New',
	// 		Novidea_HPC__Expiration_Date__c = Date.today().addDays(364));
	// 	Database.insert(policy);
	// 	policy.Novidea_HPC__Status__c = 'Placement Confirmed';
	// 	policy.Novidea_HPC__Process__c = null;
	// 	policy.Client_Cedant_Code__c = null;
	// 	System.debug('now updating policy 0');
	// 	Database.update(policy);
	// 	//Missing field : product.proposal.MAS_Approval_required
	// 	// System.assertEquals(client.Client_Code__c, policy.Client_Cedant_Code__c, 'The client code is expected to be copied to the policy');
	// 	Test.stopTest();
	// }
}