public with sharing class PolicyBreakdownMASApproval {

    public class PolicyBreakdownRow{
        @invocableVariable(required=true)
        public Id policyId;
    }

    @invocableMethod
    public static void updateMASApproval(List<PolicyBreakdownRow> rows){
        Set<Id> policyIds = new Set<Id>();
        for(PolicyBreakdownRow row : rows){
            policyIds.add(row.policyId);
        }
        Map<Id, Novidea_HPC__Policy__c> policiesMap = new Map<Id, Novidea_HPC__Policy__c>(
            [SELECT Risk_Location__c, Novidea_HPC__Carrier__c, Client_Cedant_Code__c, Novidea_HPC__Client__c, MAS_Approved_Required__c
                FROM Novidea_HPC__Policy__c
                WHERE Id IN :policyIds]
        );
        Map<Id, List<Novidea_HPC__Policy_Breakdown__c>> policyIdToPolicyBreakdownsMap = new Map<Id, List<Novidea_HPC__Policy_Breakdown__c>>();
        Map<Id, Account> policyIdToClientMap = PolicyTriggerHelper.assignClientCedantCode(policiesMap.values(), policiesMap);
        Map<Id, Novidea_HPC__Policy__c> policiesToUpdateMap = new Map<Id, Novidea_HPC__Policy__c>();
        if(!PolicyTriggerHelper.validateMASApproval(policiesMap.values(), policiesMap, policyIdToClientMap, policyIdToPolicyBreakdownsMap, false, policiesToUpdateMap)){
            Boolean tmpStopTrigger = PolicyTriggerHelper.stopTrigger;
            PolicyTriggerHelper.stopTrigger = true;
            Database.update(policiesToUpdateMap.values());
            PolicyTriggerHelper.stopTrigger = tmpStopTrigger;
        }
    } 
}
