@isTest
public with sharing class ActionQueueableTest {
    @TestSetup
    static void makeData(){
        Novidea_HPC__General_Switches__c gs = new Novidea_HPC__General_Switches__c(Name = 'Default');
		Map<String,Id> accRtIds = NOVU.RecordTypeUtils.getIdsByDevNameAndFullSObjType(new List<String>{'Legal_Entity', 'Business', 'ReInsurer', 'Business', 'Accounting', 'Carrier'}, 'Account');
		Map<String,Id> incomeRtIds = NOVU.RecordTypeUtils.getIdsByDevNameAndFullSObjType(new List<String>{'Regular', 'MTA', 'uwr_mta', 'uwr_regular'}, 'Novidea_HPC__Income__c');


		//Create custom settings
		gs.Novidea_HPC__Income_Is_Approved_Field__c = 'HPC_Collection__Is_Approved__c';
		Database.insert(gs);

		//create accounts
		Account legalEntity = new Account(
			Novidea_HPC__Status__c = 'Approved',
			Name = 'legalEntity', 
			Fee_Brokerage__c = true, 
			Account_Currency__c = 'USD', 
			RecordTypeId = accRtIds.get('Legal_Entity'), 
			NIBA__Account_Type__c = 'Other P&L',
			NBU__Legal_Entity_Base_Currency__c = 'SGD'
		);
		Database.insert(legalEntity);
		
        Account testAccountCarrier = new Account(
            Name = 'Test Account Carrier',
			RecordTypeId = accRtIds.get('Carrier'),
			Novidea_HPC__Status__c = 'Approved',
            NIBA__Re_Insurer_type__c = 'Non-Bureau Company',
			ShippingCountry = 'Singapore', 
			Offshore__c = 'Offshore');

        Account testAccountClient = new Account(
			Name = 'Test Account Client',
			Novidea_HPC__Status__c = 'Approved',
            RecordTypeId = accRtIds.get('Business'),
			ShippingCountry = 'United Kingdom');
			
		Database.insert(new List<Account>{testAccountClient, testAccountCarrier});
		
		//Create product
		Novidea_HPC__Product__c prod = new Novidea_HPC__Product__c();
		Database.insert(prod);

		//Create product definition
		Novidea_HPC__Product_Def__c pDef = new Novidea_HPC__Product_Def__c(
			Debit_Note_Indicator__c = 'RI', 
			Code__c = '123', 
			GST_Applicable_On_Brokerage__c = 'Exempted', 
			GST_Applicable_On_Premium__c = 'Yes', 
			Life_Non_Life_Indicator__c = 'Life', 
			PPW_Applicable__c = 'No');
		Database.insert(pDef);
        //Create policy
		Novidea_HPC__Policy__c policy = new Novidea_HPC__Policy__c(
			Novidea_HPC__Apportionment__c = 100, 
			Legal_Entity__c = legalEntity.id, 
			Novidea_HPC__Division_Code__c = 'Corporate Development', 
			Novidea_HPC__Product__c = prod.Id, 
			Reinsurance_Broker__c = Userinfo.getUserId(), 
			Reinsurance_Bro__c = 'Yes',
			Novidea_HPC__Product_Definition__c = pDef.Id, 
			Novidea_HPC__Carrier__c = testAccountCarrier.Id, 
			Novidea_HPC__Client__c = testAccountClient.Id);
        Database.insert(policy);
        
        Novidea_HPC__Income__c regIncome = new Novidea_HPC__Income__c(
			RecordTypeId = incomeRtIds.get('Regular'), 
            Novidea_HPC__Client__c = testAccountClient.Id, 
            Novidea_HPC__Policy__c = policy.Id, 
            Novidea_HPC__Carrier__c = testAccountCarrier.Id,
			Novidea_HPC__Number_of_Payments__c = 1,
			HPC_Collection__Payment_Schedule__c = 'Annual',
            HPC_Collection__Period_Between_Payments__c = 12,
            HPC_Collection__First_Payment_Date__c = Date.today(),
			Premium_before_GST__c = 1000,
			Novidea_HPC__Endorsement_Number__c = 2,
			Approved_On__c = Datetime.now(),
		 	Approved_By__c = Userinfo.getUserId(),
			Transaction_Status__c = 'Approved',
			Novidea_HPC__Settlement_Currency__c = 'SGD',
			Transaction_Type__c = 'New Business',
			Policy_Charges__c = 1,
			Novidea_HPC__Commission_Amount__c = 1,
			Commission_Discount__c = 0,
			Commission_GST__c = 7,
			GST_Amount_on_Premium__c = 0.5,
			Insurer_Share__c = 0.2,
			Net_Brokerage__c = 1,
			Novidea_HPC__Premium__c = 1000,
			Other_Charges__c = 1,
			Others_Taxes__c = 1,
			Payable_to_Insurer__c = 500,
			Service_Fee__c = 1,
			Service_Fee_GST__c = 0.5,
			Total_Commission__c = 200,
			Premium_to_Pay__c = 300,
			Premium_To_Pay_With_Credit_Fee__c = 301
        );
        
        Database.insert(regIncome);

    }

    @isTest
	private static void TestActionQueueable(){
        try {
            Test.startTest();

            Novidea_HPC__Policy__c policy = [SELECT Id FROM Novidea_HPC__Policy__c LIMIT 1];
            String dataInputJson = '{"listEventData":[],"actionName":"underwriterTxs","dispatcherName":"Novidea_HPC.BinderDispatcher","className":"Novidea_HPC.MultiSectionPolicyService","errorMsg":"Error Action","successMsg":"Success Action","recordId":"' + policy.Id + '"}';
            ActionQueueablePageController.validateAction(dataInputJson, 'Novidea_HPC.BinderDispatcher');
            Id jobId = ActionQueueablePageController.createActionQueueable(dataInputJson, 'Novidea_HPC.BinderDispatcher');

            PageReference tpageRef = Page.ActionQueueablePage;
            Test.setCurrentPage(tpageRef);
            ApexPages.currentPage().getParameters().put('jobId', jobId);
            ApexPages.currentPage().getParameters().put('returnToId', policy.Id);

			ActionQueueablePageController vfController = new ActionQueueablePageController();
            vfController.checkStatusProgressBar();
            vfController.back();
            Test.stoptest();
            vfController.checkStatusProgressBar();
        } catch (Exception e) {
            System.debug(e);
        }
    }
}
