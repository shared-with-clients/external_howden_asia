public with sharing class InstallmentAutoNumberGenerator {

    public with sharing class TransactionRow{
		//@invocableVariable
        public Id incomeId;
        
        //@invocableVariable
        public Boolean isUWR;

        public TransactionRow(){

        }

        public TransactionRow(Boolean isUnderwriter, Id relevantIncomeId){
            isUWR = isUnderwriter;
            incomeId = relevantIncomeId;
        }
	}

	//@invocableMethod(label='Generate Receipt Numbers') //logic moved from PB implementation to posting trigger as per HWA-3164 requirements
	public static Map<Id, String> generateNumbers(List<TransactionRow> rows){
        Map<Id, String> receiptToNoteNumberMap = new Map<Id, String>();
        Set<Id> clientIncomeIds = new Set<Id>();
        Set<Id> uwrIncomeIds = new Set<Id>();

        for(TransactionRow row : rows){
            if(!row.isUWR){
                clientIncomeIds.add(row.incomeId);
            }else{
                uwrIncomeIds.add(row.incomeId);
            }
        }

        if(!clientIncomeIds.isEmpty()){
            receiptToNoteNumberMap.putAll(generateNumbersFromClient(clientIncomeIds));
        }

        if(!uwrIncomeIds.isEmpty()){
            receiptToNoteNumberMap.putAll(generateNumbersFromUnderwriter(uwrIncomeIds));
        }

        return receiptToNoteNumberMap;
    }

    private static Map<Id, String> generateNumbersFromClient(Set<Id> incomeIds){
        Map<Id, String> receiptIdToNoteNumberMap = new Map<Id, String>();
        Map<Id, List<HPC_Collection__Receipt__c>> clientIncomeIdToReceiptsMap = new Map<Id, List<HPC_Collection__Receipt__c>>();
        Map<Id, List<HPC_Collection__Receipt__c>> underwriterIncomeIdToReceiptsMap = new Map<Id, List<HPC_Collection__Receipt__c>>();
        Map<Date, HPC_Collection__Receipt__c> clientReceiptDateToReceiptMap = new Map<Date, HPC_Collection__Receipt__c>();
        Map<Id, HPC_Collection__Receipt__c> receiptsToUpdate = new Map<Id, HPC_Collection__Receipt__c>();
        Map<Id, Note_Number__c> clientReceiptIdToNoteNumberMap = new Map<Id, Note_Number__c>();
        Map<Id, Decimal> noteNumberCounterByLegalEntityId = new Map<Id, Decimal>();

        Map<Id, Novidea_HPC__Income__c> clientIncomesMap = new Map<Id, Novidea_HPC__Income__c>([
            SELECT Id, Legal_Entity__r.Counter_Suffix__c, Legal_Entity__r.Debit_Credit_Note_Counter__c, Legal_Entity__c,
                (SELECT Id, HPC_Collection__Effective_Date__c, Note_Number__c, HPC_Collection__Premium_To_Pay__c,
                HPC_Collection__Income__r.RecordType.DeveloperName, HPC_Collection__Income__r.Premium_To_Pay_With_Credit_Fee__c,
                HPC_Collection__Income__r.Novidea_HPC__Policy__r.Reinsurance_Bro__c,
                HPC_Collection__Income__r.Service_Fee__c, HPC_Collection__Income__c,
                HPC_Collection__Income__r.Legal_Entity__r.Counter_Suffix__c, HPC_Collection__Income__r.Legal_Entity__r.Debit_Credit_Note_Counter__c
                FROM HPC_Collection__Receipts__r)
            FROM Novidea_HPC__Income__c
            WHERE Id IN: incomeIds
                AND (RecordType.DeveloperName = 'regular' OR RecordType.DeveloperName = 'MTA' OR RecordType.DeveloperName = 'Fee' OR RecordType.DeveloperName = 'Cancellation')
        ]);

        Map<Id, Novidea_HPC__Income__c> underwriterIncomesMap = new Map<Id, Novidea_HPC__Income__c>([
            SELECT Id, Legal_Entity__r.Counter_Suffix__c, Legal_Entity__r.Debit_Credit_Note_Counter__c,
                (SELECT Id, HPC_Collection__Effective_Date__c, Note_Number__c, HPC_Collection__Premium_To_Pay__c
                FROM HPC_Collection__Receipts__r) 
            FROM Novidea_HPC__Income__c 
            WHERE Novidea_HPC__Client_Income__c IN: incomeIds
                AND (RecordType.DeveloperName = 'uwr_regular' OR RecordType.DeveloperName = 'uwr_mta')
        ]);

        Set<HPC_Collection__Receipt__c> clientReceipts = new Set<HPC_Collection__Receipt__c>();
        Set<HPC_Collection__Receipt__c> underwriterReceipts = new Set<HPC_Collection__Receipt__c>();

        for(Novidea_HPC__Income__c income : clientIncomesMap.values()){
            if(!clientIncomeIdToReceiptsMap.containsKey(income.Id)){
                noteNumberCounterByLegalEntityId.put(income.Legal_Entity__c, income.Legal_Entity__r.Debit_Credit_Note_Counter__c);
                clientIncomeIdToReceiptsMap.put(income.Id, income.HPC_Collection__Receipts__r);
                clientReceipts.addAll(income.HPC_Collection__Receipts__r);
            }
        }

        for(Novidea_HPC__Income__c income : underwriterIncomesMap.values()){
            if(!underwriterIncomeIdToReceiptsMap.containsKey(income.Id)){
                underwriterIncomeIdToReceiptsMap.put(income.Id, income.HPC_Collection__Receipts__r);
                underwriterReceipts.addAll(income.HPC_Collection__Receipts__r);
            }
        }

        for(HPC_Collection__Receipt__c receipt : clientReceipts){
            Decimal counter = noteNumberCounterByLegalEntityId.get(receipt.HPC_Collection__Income__r.Legal_Entity__c);
            String noteNumber = generateSerialNumber(String.valueOf(counter), receipt, receipt.HPC_Collection__Income__r.Legal_Entity__r.Counter_Suffix__c);
            receipt.Debit_Credit_note_No__c = noteNumber;
            receiptIdToNoteNumberMap.put(receipt.Id, noteNumber);
            receiptsToUpdate.put(receipt.Id, receipt);
            counter += 1;
            counter = counter == 10000 ? 1 : counter;
            noteNumberCounterByLegalEntityId.put(receipt.HPC_Collection__Income__r.Legal_Entity__c, counter);
        }

        for(HPC_Collection__Receipt__c receipt : underwriterReceipts){
            HPC_Collection__Receipt__c relevantClientInstallment = clientReceiptDateToReceiptMap.get(receipt.HPC_Collection__Effective_Date__c);
            if(relevantClientInstallment != null){
                String noteNumber = relevantClientInstallment.Debit_Credit_note_No__c;
                receipt.Debit_Credit_note_No__c = noteNumber;
                receiptIdToNoteNumberMap.put(receipt.Id, noteNumber);
                receiptsToUpdate.put(receipt.Id, receipt);
            }
        }

        if(!receiptsToUpdate.values().isEmpty()){
			Database.update(receiptsToUpdate.values()); 
		}

        List<Account> legalEntitiesToUpdate = new List<Account>();
        for(Id legalEntityId : noteNumberCounterByLegalEntityId.keyset()){
            legalEntitiesToUpdate.add(new Account(Id = legalEntityId, Debit_Credit_Note_Counter__c = noteNumberCounterByLegalEntityId.get(legalEntityId)));
        }
        if(!legalEntitiesToUpdate.isEmpty()){
            Database.update(legalEntitiesToUpdate);
        }
        return receiptIdToNoteNumberMap;
    }

    private static Map<Id, String> generateNumbersFromUnderwriter(Set<Id> incomeIds){
        Map<Id, String> receiptIdToNoteNumberMap = new Map<Id, String>();
        Map<Id, Map<Date, HPC_Collection__Receipt__c>> incomeIdToClientReceiptDateToReceiptMap = new Map<Id, Map<Date, HPC_Collection__Receipt__c>>();
        Set<HPC_Collection__Receipt__c> clientReceipts = new Set<HPC_Collection__Receipt__c>();
        Set<HPC_Collection__Receipt__c> underwriterReceipts = new Set<HPC_Collection__Receipt__c>();
        Map<Id, Note_Number__c> uwrReceiptIdToNoteNumberMap = new Map<Id, Note_Number__c>();
        Map<Id, HPC_Collection__Receipt__c> receiptsToUpdate = new Map<Id, HPC_Collection__Receipt__c>();
        Map<Id, Decimal> noteNumberCounterByLegalEntityId = new Map<Id, Decimal>();

        Map<Id, Novidea_HPC__Income__c> uwrIncomesMap = new Map<Id, Novidea_HPC__Income__c>([
            SELECT Id, Novidea_HPC__Client_Income__c, Novidea_HPC__Premium__c, Novidea_HPC__Cancel_Income__c, Novidea_HPC__Cancel_Income__r.Novidea_HPC__Client_Income__c, 
                Novidea_HPC__Replace_Income__c, Novidea_HPC__Replace_Income__r.Novidea_HPC__Client_Income__c, 
                (SELECT Id, HPC_Collection__Effective_Date__c, Note_Number__c, HPC_Collection__Premium_To_Pay__c,
                HPC_Collection__Income__r.RecordType.DeveloperName, HPC_Collection__Income__r.Premium_To_Pay_With_Credit_Fee__c, Payable_to_Insurer__c,
                HPC_Collection__Income__r.Novidea_HPC__Policy__r.Reinsurance_Bro__c, HPC_Collection__Income__r.Novidea_HPC__Replace_Income__r.Novidea_HPC__Client_Income__r.Provisional_Bill__c, 
                HPC_Collection__Income__r.Novidea_HPC__Replace_Income__c, HPC_Collection__Income__r.Novidea_HPC__Cancel_Income__c, Debit_Credit_note_No__c, 
                HPC_Collection__Income__r.Novidea_HPC__Replace_Income__r.Novidea_HPC__Client_Income__c,  HPC_Collection__Income__r.Novidea_HPC__Cancel_Income__r.Novidea_HPC__Client_Income__c,
                HPC_Collection__Income__r.Novidea_HPC__Client_Income__c, HPC_Collection__Income__r.Service_Fee__c,
                HPC_Collection__Income__r.Legal_Entity__c,
                HPC_Collection__Income__r.Legal_Entity__r.Debit_Credit_Note_Counter__c, HPC_Collection__Income__r.Legal_Entity__r.Counter_Suffix__c
                FROM HPC_Collection__Receipts__r)
            FROM Novidea_HPC__Income__c
            WHERE Id IN: incomeIds
                AND (RecordType.DeveloperName = 'uwr_regular' OR RecordType.DeveloperName = 'uwr_mta')
        ]);
        Set<Id> clientIncomeIds = new Set<Id>();
        for(Novidea_HPC__Income__c income : uwrIncomesMap.values()){
            if(income.Novidea_HPC__Replace_Income__c != null){
                clientIncomeIds.add(income.Novidea_HPC__Replace_Income__r.Novidea_HPC__Client_Income__c);
            }else if(income.Novidea_HPC__Cancel_Income__c != null){
                clientIncomeIds.add(income.Novidea_HPC__Client_Income__c);
            }
            underwriterReceipts.addAll(income.HPC_Collection__Receipts__r);
        }

        Map<Id, Novidea_HPC__Income__c> clientIncomesMap = new Map<Id, Novidea_HPC__Income__c>([
            SELECT Id, Provisional_Bill__c,
                (SELECT Id, HPC_Collection__Effective_Date__c, Note_Number__c, HPC_Collection__Premium_To_Pay__c, Note_Number__r.Note_Number__c, 
                HPC_Collection__Income__r.RecordType.DeveloperName, HPC_Collection__Income__r.Premium_To_Pay_With_Credit_Fee__c,
                HPC_Collection__Income__r.Novidea_HPC__Policy__r.Reinsurance_Bro__c, HPC_Collection__Income__c, Debit_Credit_note_No__c,
                HPC_Collection__Income__r.Service_Fee__c
                FROM HPC_Collection__Receipts__r)
            FROM Novidea_HPC__Income__c
            WHERE Id IN: clientIncomeIds
                AND (RecordType.DeveloperName = 'regular' OR RecordType.DeveloperName = 'MTA' OR RecordType.DeveloperName = 'Fee'OR RecordType.DeveloperName = 'Cancellation')
        ]);

        for(Novidea_HPC__Income__c income : clientIncomesMap.values()){
            clientReceipts.addAll(income.HPC_Collection__Receipts__r);
        }
        for(HPC_Collection__Receipt__c receipt : clientReceipts){
            if(!incomeIdToClientReceiptDateToReceiptMap.containsKey(receipt.HPC_Collection__Income__c)){
                incomeIdToClientReceiptDateToReceiptMap.put(receipt.HPC_Collection__Income__c, new Map<Date, HPC_Collection__Receipt__c>());
            }
            if(!incomeIdToClientReceiptDateToReceiptMap.get(receipt.HPC_Collection__Income__c).containsKey(receipt.HPC_Collection__Effective_Date__c)){
                incomeIdToClientReceiptDateToReceiptMap.get(receipt.HPC_Collection__Income__c).put(receipt.HPC_Collection__Effective_Date__c, receipt);
            }
        }

        Set<HPC_Collection__Receipt__c> uwrReceiptsNewNum = new Set<HPC_Collection__Receipt__c>();
        Set<HPC_Collection__Receipt__c> uwrReceiptsOldNum = new Set<HPC_Collection__Receipt__c>();

        for(HPC_Collection__Receipt__c receipt : underwriterReceipts){
            noteNumberCounterByLegalEntityId.put(receipt.HPC_Collection__Income__r.Legal_Entity__c, receipt.HPC_Collection__Income__r.Legal_Entity__r.Debit_Credit_Note_Counter__c);
            if(receipt.Payable_to_Insurer__c < 0 || (receipt.HPC_Collection__Income__r.Novidea_HPC__Replace_Income__c != null && !receipt.HPC_Collection__Income__r.Novidea_HPC__Replace_Income__r.Novidea_HPC__Client_Income__r.Provisional_Bill__c)){
                uwrReceiptsOldNum.add(receipt);
            }else{
                if(receipt.Debit_Credit_note_No__c == null){
                    uwrReceiptsNewNum.add(receipt);
                }
            }
        }

        for(HPC_Collection__Receipt__c receipt : uwrReceiptsOldNum){
            HPC_Collection__Receipt__c relevantClientInstallment;
            if(receipt.HPC_Collection__Income__r.Novidea_HPC__Replace_Income__c != null){
                relevantClientInstallment = incomeIdToClientReceiptDateToReceiptMap.get(receipt.HPC_Collection__Income__r.Novidea_HPC__Replace_Income__r.Novidea_HPC__Client_Income__c).get(receipt.HPC_Collection__Effective_Date__c);
            }else if(receipt.HPC_Collection__Income__r.Novidea_HPC__Cancel_Income__c != null){
                relevantClientInstallment = incomeIdToClientReceiptDateToReceiptMap.get(receipt.HPC_Collection__Income__r.Novidea_HPC__Client_Income__c).get(receipt.HPC_Collection__Effective_Date__c);
            }
            if(relevantClientInstallment != null){
                receipt.Debit_Credit_note_No__c = relevantClientInstallment.Debit_Credit_note_No__c;
                receiptsToUpdate.put(receipt.Id, receipt);
            }
        }

        for(HPC_Collection__Receipt__c receipt : uwrReceiptsNewNum){
            Decimal counter = noteNumberCounterByLegalEntityId.get(receipt.HPC_Collection__Income__r.Legal_Entity__c);
            String noteNumber = generateSerialNumber(String.valueOf(counter), receipt, receipt.HPC_Collection__Income__r.Legal_Entity__r.Counter_Suffix__c);
            receipt.Debit_Credit_note_No__c = noteNumber;
            receiptIdToNoteNumberMap.put(receipt.Id, noteNumber);
            receiptsToUpdate.put(receipt.Id, receipt);
            counter += 1;
            counter = counter == 10000 ? 1 : counter;
            noteNumberCounterByLegalEntityId.put(receipt.HPC_Collection__Income__r.Legal_Entity__c, counter);
        }
        
        if(!receiptsToUpdate.values().isEmpty()){
			Database.update(receiptsToUpdate.values());
		}

        List<Account> legalEntitiesToUpdate = new List<Account>();
        for(Id legalEntityId : noteNumberCounterByLegalEntityId.keyset()){
            legalEntitiesToUpdate.add(new Account(Id = legalEntityId, Debit_Credit_Note_Counter__c = noteNumberCounterByLegalEntityId.get(legalEntityId)));
        }
        if(!legalEntitiesToUpdate.isEmpty()){
            Database.update(legalEntitiesToUpdate);
        }
        return receiptIdToNoteNumberMap;
    }

    //Based on HWA-1476
    private static String generateSerialNumber(String num, HPC_Collection__Receipt__c receipt, String suffix){
        suffix = suffix != null ? suffix : '';
        num = num.leftPad(4, '0');
        Datetime now = Datetime.now();
        if(receipt.HPC_Collection__Income__r.Novidea_HPC__Policy__r.Reinsurance_Bro__c == 'Yes'){
            if(receipt.HPC_Collection__Income__r.RecordType.DeveloperName != 'Fee'){
                if(receipt.HPC_Collection__Income__r.Premium_To_Pay_With_Credit_Fee__c >= 0){
                    return 'RDN' + now.format('YY') + now.format('MM') + num + suffix;
                }else if(receipt.HPC_Collection__Income__r.Premium_To_Pay_With_Credit_Fee__c < 0 || 
                        receipt.HPC_Collection__Income__r.Premium_To_Pay_With_Credit_Fee__c == null){
                    return 'RCN' + now.format('YY') + now.format('MM') + num + suffix;
                }
            }else{
                if(receipt.HPC_Collection__Income__r.Service_Fee__c >= 0){
                    return 'RDN' + now.format('YY') + now.format('MM') + num + suffix;
                }else if(receipt.HPC_Collection__Income__r.Service_Fee__c < 0 || 
                        receipt.HPC_Collection__Income__r.Service_Fee__c == null){
                    return 'RCN' + now.format('YY') + now.format('MM') + num + suffix;
                }
            }
        }else{
            if(receipt.HPC_Collection__Income__r.RecordType.DeveloperName != 'Fee'){
                if(receipt.HPC_Collection__Income__r.Premium_To_Pay_With_Credit_Fee__c >= 0){
                    return 'DN' + now.format('YY') + now.format('MM') + num + suffix;
                }else if(receipt.HPC_Collection__Income__r.Premium_To_Pay_With_Credit_Fee__c < 0 || 
                        receipt.HPC_Collection__Income__r.Premium_To_Pay_With_Credit_Fee__c == null){
                    return 'CN' + now.format('YY') + now.format('MM') + num + suffix;
                }
            }else{
                if(receipt.HPC_Collection__Income__r.Service_Fee__c >= 0){
                    return 'DN' + now.format('YY') + now.format('MM') + num + suffix;
                }else if(receipt.HPC_Collection__Income__r.Service_Fee__c < 0 || 
                        receipt.HPC_Collection__Income__r.Service_Fee__c == null){
                    return 'CN' + now.format('YY') + now.format('MM') + num + suffix;
                }
            }
        }
        return null;
    }
}