public with sharing class GetExchangeRate {
    public class GetExchangeRateException extends Exception {}

    public class ExchangeRateParams{

        public ExchangeRateParams(){}

        public ExchangeRateParams(string sourceCurrency, string targetCurrency, Decimal rate, Date actualDate){
            this.sourceCurrency = sourceCurrency;
            this.targetCurrency = targetCurrency;
            this.rate = rate;
            this.actualDate = actualDate;
        }

        @InvocableVariable
        public string sourceCurrency;

        @InvocableVariable
        public string targetCurrency;

        @InvocableVariable
        public Decimal rate;
        
        @InvocableVariable
        public Date actualDate;
    }

    @InvocableMethod
	public static List<ExchangeRateParams> getExchangeRate(List<ExchangeRateParams> exchangeRateParams){
        if(NOVU.ArrayUtils.isListNullOrEmpty(exchangeRateParams)){
			throw new GetExchangeRateException(Label.Input_Is_Empty);
        }
        
        List<NBU.ExchangeRate> exchangeDetailsList= new List<NBU.ExchangeRate>();
        for(ExchangeRateParams exchangeRateParam : exchangeRateParams){
            NBU.ExchangeRate exchangeDetails = new NBU.ExchangeRate(exchangeRateParam.sourceCurrency,
                                                                    exchangeRateParam.targetCurrency,
                                                                    DateTime.Now());
            exchangeDetailsList.add(exchangeDetails); 
        }

        NBU.ExchangeRateUtils exchangeRateUtilsObject = new NBU.ExchangeRateUtils();

        exchangeRateUtilsObject.calculateRate(exchangeDetailsList);

        List<ExchangeRateParams> exchangeRateParamsResults = new List<ExchangeRateParams>();

        for(NBU.ExchangeRate exchangeRate : exchangeDetailsList){
            ExchangeRateParams exParams = new ExchangeRateParams(exchangeRate.sourceCurrency,
                                                                 exchangeRate.targetCurrency, 
                                                                 exchangeRate.rate, 
                                                                 exchangeRate.actualDate);
            exchangeRateParamsResults.add(exParams);
        }

        return exchangeRateParamsResults;
    }
}
