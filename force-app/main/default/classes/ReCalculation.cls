public with sharing class ReCalculation {
	
	public with sharing class TransactionRow{
		@invocableVariable
		public Id incomeId;

		@invocableVariable
		public Boolean isSuccess;

		@invocableVariable
		public String Message;
	}

	@invocableMethod
	public static List<TransactionRow> doRecalculation(List<TransactionRow> rows){
		Map<Id, TransactionRow> incomeIdToTransactionRowMap = new Map<Id, TransactionRow>();
		for(TransactionRow row : rows){
			incomeIdToTransactionRowMap.put(row.incomeId, row);
		}

		//Main Queries
		//clientTransactionsMap = map of client transaction id to client transactions
		Map<Id, Novidea_HPC__Income__c> clientTransactionsMap = new Map<Id, Novidea_HPC__Income__c>([
            SELECT Name, Commission_GST__c, Premium_before_GST__c, GST_Amount_on_Premium__c, Novidea_HPC__Premium__c, Payable_to_Insurer__c, 
            Premium_To_Pay_With_Credit_Fee__c, Novidea_HPC__Commission_Amount__c, Net_Brokerage__c, Service_Fee__c, Service_Fee_GST__c, 
            Stamp_Duty__c, Policy_Charges__c, Others_Taxes__c, Other_Charges__c, Novidea_HPC__Tax_on_Net_Premium__c, Premium_to_Pay__c, 
            Commission_Discount__c, Commission_discount_rate__c, Novidea_HPC__Commission_Percentage__c, RecordType.DeveloperName,
            Novidea_HPC__Client__r.NIBA__Has_Financial_Implications__c, Novidea_HPC__Policy__r.RecordType.DeveloperName,
			Novidea_HPC__Client__r.NIBA__Accounting_Status__c, Novidea_HPC__Client__r.Name, Total_Commission__c, 
			Novidea_HPC__Policy__r.Policy_Aggregated_IA_Levy_Amount__c, Novidea_HPC__Policy__r.IA_Levy_Cap__c, 
			IA_Levy_Amount__c, MIB_Amount__c, ECI_Levy_Amount__c, MIB__c, ECI_Levy__c,
				(SELECT Name, GST_Amount_on_Premium__c, HPC_Collection__Net_Premium__c, HPC_Collection__Commission_Amount__c, HPC_Collection__Income__c,
				Payable_to_Insurer__c, HPC_Collection__Expected_Premium_to_Pay__c, Commission_Amount_Discount__c, Net_Brokerage__c, Client_Payable__c, 
				Commission_GST__c, Service_Fee__c, Service_Fee_GST__c, Stamp_Duty__c, Policy_Charges__c, Other_Taxes__c, Other_Charges__c,
				IA_Levy_Amount__c, MIB_Amount__c, ECI_Levy_Amount__c, MIB__c, ECI_Levy__c,
				HPC_Collection__Income__r.Commission_GST__c, HPC_Collection__Effective_Date__c
				FROM HPC_Collection__Receipts__r
				WHERE HPC_Collection__Ready_For_Collection__c = false)
			FROM Novidea_HPC__Income__c
			WHERE Id IN :incomeIdToTransactionRowMap.keySet()
				AND (RecordType.DeveloperName = 'regular' OR RecordType.DeveloperName = 'MTA' OR RecordType.DeveloperName = 'Cancellation')
				AND HPC_Collection__Is_Approved__c = false
		]);

		//feeTransactionsMap = map fee transactions for seperate validations - HWA-2304
		Map<Id, Novidea_HPC__Income__c> feeTransactionsMap = new Map<Id, Novidea_HPC__Income__c>([
            SELECT Fee_Account__c
			FROM Novidea_HPC__Income__c
			WHERE Id IN :incomeIdToTransactionRowMap.keySet()
				AND (RecordType.DeveloperName = 'Fee') 
				AND HPC_Collection__Is_Approved__c = false
		]);

		//underwriterTransactionsMap = map of underwriter transaction id to underwriter transactions
		Map<Id, Novidea_HPC__Income__c> underwriterTransactionsMap = new Map<Id, Novidea_HPC__Income__c>([
			SELECT Name, Commission_GST__c, Novidea_HPC__Tax_on_Net_Premium__c, Total_Other_Taxes_and_Charges__c, Commission_Discount__c, 
			Premium_to_Pay__c, Novidea_HPC__Client_Income__c, Premium_before_GST__c, GST_Amount_on_Premium__c, Novidea_HPC__Premium__c, Payable_to_Insurer__c, Premium_To_Pay_With_Credit_Fee__c, Novidea_HPC__Commission_Amount__c, Net_Brokerage__c, Service_Fee__c, Service_Fee_GST__c, Stamp_Duty__c, Policy_Charges__c, Others_Taxes__c, Other_Charges__c, Novidea_HPC__Carrier__r.NIBA__Has_Financial_Implications__c, Novidea_HPC__Carrier__r.Name, 
			Novidea_HPC__Carrier__r.NIBA__Accounting_Status__c, Total_Commission__c, Novidea_HPC__Income__c.Novidea_HPC__Policy_Breakdown__c, 
            Novidea_HPC__Commission_Percentage__c, Commission_discount_rate__c, Novidea_HPC__Policy_Breakdown__r.Novidea_HPC__Signed_Line__c,
            Novidea_HPC__Policy_Breakdown__r.Name, Novidea_HPC__Policy__r.Novidea_HPC__Apportionment__c, 
			Brokerage_Account__c, Brokerage_Account_Definition_Status__c, Tax_Account__c, Tax_Account_Definition_Status__c ,
			Novidea_HPC__Policy__r.Policy_Aggregated_IA_Levy_Amount__c, Novidea_HPC__Policy__r.IA_Levy_Cap__c, 
			IA_Levy_Amount__c, MIB_Amount__c, ECI_Levy_Amount__c, MIB__c, ECI_Levy__c, Legal_Entity__c, Legal_Entity__r.Unit_4_Code__c, 
				(SELECT Name, HPC_Collection__Client_Receipt__c, HPC_Collection__Expected_Premium_to_Pay__c, Service_Fee__c, HPC_Collection__Income__c, HPC_Collection__Commission_Amount__c,GST_Amount_on_Premium__c, Payable_to_Insurer__c, HPC_Collection__Total_Commission__c, 
				HPC_Collection__Income__r.Novidea_HPC__Number_of_Payments__c,
				Service_Fee_GST__c, Stamp_Duty__c, Policy_Charges__c, Other_Taxes__c, Other_Charges__c, 
				HPC_Collection__Income__r.Novidea_HPC__Client_Income__c, Client_Payable__c, 
				IA_Levy_Amount__c, MIB_Amount__c, ECI_Levy_Amount__c, MIB__c, ECI_Levy__c,
				Commission_Amount_Discount__c, Net_Brokerage__c, HPC_Collection__Net_Premium__c, Commission_GST__c, 
				HPC_Collection__Income__r.Commission_GST__c, HPC_Collection__Income__r.Novidea_HPC__Tax_on_Net_Premium__c, HPC_Collection__Income__r.Total_Other_Taxes_and_Charges__c, HPC_Collection__Income__r.Premium_To_Pay_With_Credit_Fee__c,
				HPC_Collection__Income__r.Commission_Discount__c, HPC_Collection__Income__r.Novidea_HPC__Commission_Percentage__c,
				HPC_Collection__Income__r.Commission_discount_rate__c, HPC_Collection__Effective_Date__c,
				HPC_Collection__Income__r.Novidea_HPC__Policy_Breakdown__r.Novidea_HPC__Signed_Line__c
				FROM HPC_Collection__Receipts__r
				WHERE HPC_Collection__Ready_For_Collection__c = false)
			FROM Novidea_HPC__Income__c
			WHERE Novidea_HPC__Client_Income__c IN :incomeIdToTransactionRowMap.keySet()
				AND (RecordType.DeveloperName = 'uwr_regular' OR RecordType.DeveloperName = 'uwr_mta')
				AND HPC_Collection__Is_Approved__c = false
		]);

		Map<Id, List<Novidea_HPC__Income__c>> clientIncomeIdToUnderwriterIncomesMap = new Map<Id, List<Novidea_HPC__Income__c>>();
		for(Novidea_HPC__Income__c income : underwriterTransactionsMap.values()){
			if(!clientIncomeIdToUnderwriterIncomesMap.containsKey(income.Novidea_HPC__Client_Income__c)){
				clientIncomeIdToUnderwriterIncomesMap.put(income.Novidea_HPC__Client_Income__c, new List<Novidea_HPC__Income__c>());
			}
			income.Last_Recalculated__c = DateTime.now();
			clientIncomeIdToUnderwriterIncomesMap.get(income.Novidea_HPC__Client_Income__c).add(income);
		}

		for(TransactionRow row : incomeIdToTransactionRowMap.values()){
			if(!feeTransactionsMap.containsKey(row.incomeId) && (clientIncomeIdToUnderwriterIncomesMap.get(row.incomeId) == null || clientIncomeIdToUnderwriterIncomesMap.get(row.incomeId).isEmpty())){
				row.isSuccess = false;
				row.Message = System.Label.No_UWR_Installments;
				clientTransactionsMap.remove(row.incomeId);
			}
		}
		
		Map<Id, HPC_Collection__Receipt__c> clientInstallmentsMap = new Map<Id, HPC_Collection__Receipt__c>();
		Map<Id, List<HPC_Collection__Receipt__c>> clientIncomeIdToReceiptsMap = new Map<Id, List<HPC_Collection__Receipt__c>>();

		for(Novidea_HPC__Income__c income : clientTransactionsMap.values()){
			clientInstallmentsMap.putAll(income.HPC_Collection__Receipts__r);

			if(!clientIncomeIdToReceiptsMap.containsKey(income.Id)){
				clientIncomeIdToReceiptsMap.put(income.Id, income.HPC_Collection__Receipts__r);
			}
		}

		Map<Id, Map<Date, HPC_Collection__Receipt__c>> clientIncomeIdToReceiptByDatesMap = new Map<Id, Map<Date, HPC_Collection__Receipt__c>>();

		for(HPC_Collection__Receipt__c receipt : clientInstallmentsMap.values()){
			if(!clientIncomeIdToReceiptByDatesMap.containsKey(receipt.HPC_Collection__Income__c)){
				clientIncomeIdToReceiptByDatesMap.put(receipt.HPC_Collection__Income__c, new Map<Date, HPC_Collection__Receipt__c>());
			}
			clientIncomeIdToReceiptByDatesMap.get(receipt.HPC_Collection__Income__c).put(receipt.HPC_Collection__Effective_Date__c, receipt);
		}

		Map<Id, HPC_Collection__Receipt__c> underwriterInstallmentsMap = new Map<Id, HPC_Collection__Receipt__c>();
		Map<Id, List<HPC_Collection__Receipt__c>> underwriterIncomeIdToReceiptsMap = new Map<Id, List<HPC_Collection__Receipt__c>>();

		for(Novidea_HPC__Income__c income : underwriterTransactionsMap.values()){
			underwriterInstallmentsMap.putAll(income.HPC_Collection__Receipts__r);

			if(!underwriterIncomeIdToReceiptsMap.containsKey(income.Id)){
				underwriterIncomeIdToReceiptsMap.put(income.Id, income.HPC_Collection__Receipts__r);
			}
		}

		Map<Id, List<HPC_Collection__Receipt__c>> clientIdToUnderwriterReceiptsMap = new Map<Id, List<HPC_Collection__Receipt__c>>();
		for(HPC_Collection__Receipt__c installment : underwriterInstallmentsMap.values()){
			if(installment.HPC_Collection__Client_Receipt__c != null ){
				if(!clientIdToUnderwriterReceiptsMap.containsKey(installment.HPC_Collection__Client_Receipt__c)){
					clientIdToUnderwriterReceiptsMap.put(installment.HPC_Collection__Client_Receipt__c, new List<HPC_Collection__Receipt__c>());
				}
				clientIdToUnderwriterReceiptsMap.get(installment.HPC_Collection__Client_Receipt__c).add(installment);
			}
			else if(clientIncomeIdToReceiptByDatesMap.containsKey(installment.HPC_Collection__Income__r.Novidea_HPC__Client_Income__c)){
				Novidea_HPC__Income__c clientIncome = clientTransactionsMap.get(installment.HPC_Collection__Income__r.Novidea_HPC__Client_Income__c);
				HPC_Collection__Receipt__c clientReceipt = clientIncomeIdToReceiptByDatesMap.get(clientIncome.Id).get(installment.HPC_Collection__Effective_Date__c);
				if(clientReceipt != null && !clientIdToUnderwriterReceiptsMap.containsKey(clientReceipt.Id)){
					clientIdToUnderwriterReceiptsMap.put(clientReceipt.Id, new List<HPC_Collection__Receipt__c>());
				}
				if(clientReceipt != null && clientIdToUnderwriterReceiptsMap.get(clientReceipt.Id) != null){
					clientIdToUnderwriterReceiptsMap.get(clientReceipt.Id).add(installment);
				}
			}
		}

		//Send to TransactionCalculation class
		Set<Novidea_HPC__Income__c> incomesToUpdate = new Set<Novidea_HPC__Income__c>();
		Set<HPC_Collection__Receipt__c> receiptsToUpdate = new Set<HPC_Collection__Receipt__c>();

		Set<Id> modifiedReceiptIdSet = new Set<Id>();
		Set<Id> modifiedUnderwriterReceiptIdSet = new Set<Id>();
		Set<Id> modifiedUnderwriterIncomeIdSet = new Set<Id>();
		Set<Id> modifiedIncomeIdSet = new Set<Id>();
		Map<Id, String> transactionIdToErrorMessageMap = new Map<Id, String>();

		//Calculations
		modifiedReceiptIdSet.addAll(TransactionCalculations.recalculateUnderwriterInstallments(underwriterInstallmentsMap));
		modifiedUnderwriterReceiptIdSet.addAll(modifiedReceiptIdSet);
		modifiedIncomeIdSet.addAll(TransactionCalculations.recalculateUnderwriterTransactions(underwriterTransactionsMap, underwriterIncomeIdToReceiptsMap));
		modifiedUnderwriterIncomeIdSet.addAll(modifiedIncomeIdSet);
		modifiedReceiptIdSet.addAll(TransactionCalculations.recalculateClientInstallments(clientInstallmentsMap, clientIdToUnderwriterReceiptsMap));
		modifiedIncomeIdSet.addAll(TransactionCalculations.recalculateClientTransactions(clientTransactionsMap, clientIncomeIdToReceiptsMap));
		

		// Map<Id, Novidea_HPC__Income__c> multiSectionIncomes = new Map<Id, Novidea_HPC__Income__c>();

		// for(TransactionRow row : incomeIdToTransactionRowMap.values()){
		// 	if(row.isSuccess && clientTransactionsMap.get(row.incomeId).Novidea_HPC__Policy__r.RecordType.DeveloperName == 'Multi_Section'){
		// 		multiSectionIncomes.put(row.incomeId, clientTransactionsMap.get(row.incomeId));
		// 		clientTransactionsMap.remove(row.incomeId);
		// 	}
		// }
		//Validations
		transactionIdToErrorMessageMap = TransactionCalculations.validateTransactions(clientTransactionsMap, underwriterTransactionsMap);
		System.debug(transactionIdToErrorMessageMap);
		// clientTransactionsMap.putAll(multiSectionIncomes.values());
		Map<Id, Integer> clientTransactionIdToErrorsMap = new Map<Id, Integer>();
		Map<Id, Integer> underwriterTransactionIdToErrorsMap = new Map<Id, Integer>();

		//Split errors to client map / underwriter map
		for(Id incomeId : modifiedIncomeIdSet){
			if(clientTransactionsMap.containsKey(incomeId) && !clientTransactionIdToErrorsMap.containsKey(incomeId)){
				clientTransactionIdToErrorsMap.put(incomeId, 0);
			}
			if(underwriterTransactionsMap.containsKey(incomeId) && !underwriterTransactionIdToErrorsMap.containsKey(incomeId)){
				underwriterTransactionIdToErrorsMap.put(incomeId, 0);
			}

			if(transactionIdToErrorMessageMap.containsKey(incomeId)){
				if(clientTransactionIdToErrorsMap.containsKey(incomeId)){
					clientTransactionIdToErrorsMap.put(incomeId, clientTransactionIdToErrorsMap.get(incomeId) + 1);
				}
				if(underwriterTransactionIdToErrorsMap.containsKey(incomeId)){
					underwriterTransactionIdToErrorsMap.put(incomeId, underwriterTransactionIdToErrorsMap.get(incomeId) + 1);
				}
			}
		}

		for(Id receiptId : modifiedReceiptIdSet){
			HPC_Collection__Receipt__c receipt;
			Boolean isClient = false;
			if(clientInstallmentsMap.containsKey(receiptId)){
				receipt = clientInstallmentsMap.get(receiptId);
				isClient = true;
			}else if(underwriterInstallmentsMap.containsKey(receiptId)){
				receipt = underwriterInstallmentsMap.get(receiptId);
			}
			if(receipt != null){
				if(isClient){
					Id incomeId = receipt.HPC_Collection__Income__c;
					if(clientTransactionsMap.containsKey(incomeId) && !clientTransactionIdToErrorsMap.containsKey(incomeId) && !clientTransactionIdToErrorsMap.containsKey(incomeId)){
						clientTransactionIdToErrorsMap.put(incomeId, 0);
					}
					if(underwriterTransactionsMap.containsKey(incomeId) && !underwriterTransactionIdToErrorsMap.containsKey(incomeId) && !underwriterTransactionIdToErrorsMap.containsKey(incomeId)){
						underwriterTransactionIdToErrorsMap.put(incomeId, 0);
					}
		
					if(transactionIdToErrorMessageMap.containsKey(incomeId)){
						if(clientTransactionIdToErrorsMap.containsKey(incomeId)){
							clientTransactionIdToErrorsMap.put(incomeId, clientTransactionIdToErrorsMap.get(incomeId) + 1);
						}
						if(underwriterTransactionIdToErrorsMap.containsKey(incomeId)){
							underwriterTransactionIdToErrorsMap.put(incomeId, underwriterTransactionIdToErrorsMap.get(incomeId) + 1);
						}
					}
				}else{
					Id incomeId = receipt.HPC_Collection__Income__r.Novidea_HPC__Client_Income__c;
					if(clientTransactionsMap.containsKey(incomeId) && !clientTransactionIdToErrorsMap.containsKey(incomeId)){
						clientTransactionIdToErrorsMap.put(incomeId, 0);
					}
					if(underwriterTransactionsMap.containsKey(incomeId) && !underwriterTransactionIdToErrorsMap.containsKey(incomeId)){
						underwriterTransactionIdToErrorsMap.put(incomeId, 0);
					}
		
					if(transactionIdToErrorMessageMap.containsKey(incomeId)){
						if(clientTransactionIdToErrorsMap.containsKey(incomeId)){
							clientTransactionIdToErrorsMap.put(incomeId, clientTransactionIdToErrorsMap.get(incomeId) + 1);
						}
						if(underwriterTransactionIdToErrorsMap.containsKey(incomeId)){
							underwriterTransactionIdToErrorsMap.put(incomeId, underwriterTransactionIdToErrorsMap.get(incomeId) + 1);
						}
					}
				}
			}
		}
		//Add client transaction errors to rows
		for(Id incomeId : clientTransactionIdToErrorsMap.keySet()){
			if(clientTransactionIdToErrorsMap.get(incomeId) > 0){
				TransactionRow row = incomeIdToTransactionRowMap.get(incomeId);
				row.isSuccess = false;
				row.Message += ' ' + transactionIdToErrorMessageMap.get(incomeId);
			}
		}
		//Add underwriter transaction errors to rows
		for(Id incomeId : underwriterTransactionIdToErrorsMap.keySet()){
			if(underwriterTransactionIdToErrorsMap.get(incomeId) > 0){
				Novidea_HPC__Income__c income = underwriterTransactionsMap.get(incomeId);
				TransactionRow row = incomeIdToTransactionRowMap.get(income.Novidea_HPC__Client_Income__c);
				row.isSuccess = false;
				if(transactionIdToErrorMessageMap.get(income.Id) != null){
					row.Message += ' ' + transactionIdToErrorMessageMap.get(income.Id);
				}
			}
		}

		for(Id incomeId : transactionIdToErrorMessageMap.keySet()){
			if(clientTransactionsMap.containsKey(incomeId)){
				TransactionRow row = incomeIdToTransactionRowMap.get(incomeId);
				if(transactionIdToErrorMessageMap.get(incomeId) != null && row.isSuccess){
					row.isSuccess = false;
					row.Message += ' ' + transactionIdToErrorMessageMap.get(incomeId);
				}

			}else if(underwriterTransactionsMap.containsKey(incomeId)){
				Novidea_HPC__Income__c income = underwriterTransactionsMap.get(incomeId);
				TransactionRow row = incomeIdToTransactionRowMap.get(income.Novidea_HPC__Client_Income__c);
				if(transactionIdToErrorMessageMap.get(incomeId) != null && row.isSuccess){
					row.isSuccess = false;
					row.Message += ' ' + transactionIdToErrorMessageMap.get(incomeId);
				}
			}
		}

		//Build maps for DMLs based on client transaction id
		//errorClientTransactionIdSet aggregates underwriter transaction errors to parent client transaction
		Set<Id> errorClientTransactionIdSet = new Set<Id>();
		Map<Id, List<Novidea_HPC__Income__c>> clientTransactionIdToUnderwriterTransactionsMap = new Map<Id, List<Novidea_HPC__Income__c>>();
		Map<Id, List<HPC_Collection__Receipt__c>> clientTransactionIdToUnderwriterInstallmentsMap = new Map<Id, List<HPC_Collection__Receipt__c>>();
		Map<Id, List<HPC_Collection__Receipt__c>> clientTransactionIdToClientInstallmentsMap = new Map<Id, List<HPC_Collection__Receipt__c>>();

		for(HPC_Collection__Receipt__c underwriterReceipt : underwriterInstallmentsMap.values()){
			if(!clientTransactionIdToUnderwriterInstallmentsMap.containsKey(underwriterReceipt.HPC_Collection__Income__r.Novidea_HPC__Client_Income__c)){
				clientTransactionIdToUnderwriterInstallmentsMap.put(underwriterReceipt.HPC_Collection__Income__r.Novidea_HPC__Client_Income__c, new List<HPC_Collection__Receipt__c>());
			}
			clientTransactionIdToUnderwriterInstallmentsMap.get(underwriterReceipt.HPC_Collection__Income__r.Novidea_HPC__Client_Income__c).add(underwriterReceipt);
		}

		for(HPC_Collection__Receipt__c clientReceipt : clientInstallmentsMap.values()){
			if(!clientTransactionIdToClientInstallmentsMap.containsKey(clientReceipt.HPC_Collection__Income__c)){
				clientTransactionIdToClientInstallmentsMap.put(clientReceipt.HPC_Collection__Income__c, new List<HPC_Collection__Receipt__c>());
			}
			clientTransactionIdToClientInstallmentsMap.get(clientReceipt.HPC_Collection__Income__c).add(clientReceipt);
		}

		for(Id incomeId : underwriterTransactionIdToErrorsMap.keyset()){
			Novidea_HPC__Income__c income = underwriterTransactionsMap.get(incomeId);
			if(underwriterTransactionIdToErrorsMap.get(incomeId) > 0){
				errorClientTransactionIdSet.add(income.Novidea_HPC__Client_Income__c);
			}
			if(!clientTransactionIdToUnderwriterTransactionsMap.containsKey(income.Novidea_HPC__Client_Income__c)){
				clientTransactionIdToUnderwriterTransactionsMap.put(income.Novidea_HPC__Client_Income__c, new List<Novidea_HPC__Income__c>());
			}
			clientTransactionIdToUnderwriterTransactionsMap.get(income.Novidea_HPC__Client_Income__c).add(income);
		}

		//HWA-2304
		if(feeTransactionsMap != null && !feeTransactionsMap.isEmpty()){
			for(Novidea_HPC__Income__c income : feeTransactionsMap.values()){
				String errorMessage = '';
				errorMessage = TransactionCalculations.validateFeeAccountPopulated(income, errorMessage);

				if(errorMessage != ''){
					TransactionRow row = incomeIdToTransactionRowMap.get(income.Id);
					row.isSuccess = false;
					row.Message = errorMessage;
				}
			}
		}

		//Database updates
		System.Savepoint savePoint = Database.setSavePoint();
		try {
			for(Id incomeId : clientTransactionIdToErrorsMap.keyset()){
				if(!errorClientTransactionIdSet.contains(incomeId) && clientTransactionIdToErrorsMap.get(incomeId) == 0){
					//Add underwriter installments
					if(clientTransactionIdToUnderwriterInstallmentsMap.get(incomeId) != null){
						receiptsToUpdate.addAll(clientTransactionIdToUnderwriterInstallmentsMap.get(incomeId));
					}
					//Add underwriter transactions
					if(clientTransactionIdToUnderwriterTransactionsMap.get(incomeId) != null){
						incomesToUpdate.addAll(clientTransactionIdToUnderwriterTransactionsMap.get(incomeId));
					}
					//Add client installments
					receiptsToUpdate.addAll(clientTransactionIdToClientInstallmentsMap.get(incomeId));
					//Add client transaction
					incomesToUpdate.add(clientTransactionsMap.get(incomeId));
				}
			}
			
			if(!incomesToUpdate.isEmpty()){
				Database.update(new List<Novidea_HPC__Income__c>(incomesToUpdate));
			}
			if(!receiptsToUpdate.isEmpty()){
				Database.update(new List<HPC_Collection__Receipt__c>(receiptsToUpdate));
			}
			
			
		} catch (Exception e) {
			Database.rollback(savePoint);
			System.debug(e.getMessage() + ' - ' + e.getLineNumber() + ' - ' + e.getStackTraceString());
			for(TransactionRow row : incomeIdToTransactionRowMap.values()){
				row.isSuccess = false;
				row.Message = e.getMessage() + ' - ' + e.getLineNumber() + ' - ' + e.getStackTraceString();
			}
		}
		
		System.debug(incomeIdToTransactionRowMap.values());
		return incomeIdToTransactionRowMap.values();
	}
}