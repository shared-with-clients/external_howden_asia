/** Transaction recalculation helper class */
public with sharing class TransactionCalculations{
	private static final Integer REGULAR_SCALE = 2;
	private static final Integer RATE_SCALE = 2;
	private static final Decimal TOLERANCE = Settings__c.getInstance().Recalc_Tolerance__c;
	/**
	 * Recalculate underwriter installments 
	 * @param idToReceiptMap - Maps between ID and a underwriter installments that might need recalc.
	 * @return A set of the IDs of all the installments that got modified.
	 */
	public static Set<Id> recalculateUnderwriterInstallments(Map<Id, HPC_Collection__Receipt__c> idToReceiptMap){
		Set<Id> modifiedReceiptIdSet = new Set<Id>();
		for(HPC_Collection__Receipt__c curReceipt : idToReceiptMap.values()){
			Boolean hasChanged = false;

			hasChanged |= changeReceiptVal(curReceipt, 'Client_Payable__c', curReceipt.Client_Payable__c,
			((curReceipt.HPC_Collection__Net_Premium__c != null ? curReceipt.HPC_Collection__Net_Premium__c : 0) +
			(curReceipt.GST_Amount_on_Premium__c != null ? curReceipt.GST_Amount_on_Premium__c : 0) -
			(curReceipt.Commission_Amount_Discount__c != null ? curReceipt.Commission_Amount_Discount__c : 0) +
			(curReceipt.Service_Fee__c != null ? curReceipt.Service_Fee__c : 0) +
			(curReceipt.Service_Fee_GST__c != null ? curReceipt.Service_Fee_GST__c : 0) +
			(curReceipt.Stamp_Duty__c != null ? curReceipt.Stamp_Duty__c : 0) +
			(curReceipt.Policy_Charges__c != null ? curReceipt.Policy_Charges__c : 0) +
			(curReceipt.Other_Taxes__c != null ? curReceipt.Other_Taxes__c : 0) +
			(curReceipt.Other_Charges__c != null ? curReceipt.Other_Charges__c : 0)));

			hasChanged |= changeReceiptVal(curReceipt, 'MIB_Amount__c', curReceipt.MIB_Amount__c,
			((curReceipt.HPC_Collection__Net_Premium__c != null ? curReceipt.HPC_Collection__Net_Premium__c : 0) *
			(curReceipt.MIB__c != null ? curReceipt.MIB__c / 100 : 0)));
			
			hasChanged |= changeReceiptVal(curReceipt, 'ECI_Levy_Amount__c', curReceipt.ECI_Levy_Amount__c,
			((curReceipt.HPC_Collection__Net_Premium__c != null ? curReceipt.HPC_Collection__Net_Premium__c : 0) *
			(curReceipt.ECI_Levy__c != null ? curReceipt.ECI_Levy__c / 100 : 0)));

			hasChanged |= changeReceiptVal(curReceipt, 'Payable_to_Insurer__c', curReceipt.Payable_to_Insurer__c,
			((curReceipt.HPC_Collection__Net_Premium__c != null ? curReceipt.HPC_Collection__Net_Premium__c : 0) +
			(curReceipt.GST_Amount_on_Premium__c != null ? curReceipt.GST_Amount_on_Premium__c : 0) - 
			((curReceipt.HPC_Collection__Commission_Amount__c != null ? curReceipt.HPC_Collection__Commission_Amount__c : 0) +
			(curReceipt.Commission_GST__c != null ? curReceipt.Commission_GST__c : 0)) + 
			(curReceipt.IA_Levy_Amount__c != null ? curReceipt.IA_Levy_Amount__c : 0) +
			(curReceipt.MIB_Amount__c != null ? curReceipt.MIB_Amount__c : 0) +
			(curReceipt.ECI_Levy_Amount__c != null ? curReceipt.ECI_Levy_Amount__c : 0)));
			
			if(hasChanged){
				modifiedReceiptIdSet.add(curReceipt.Id);
			}
		}
		return modifiedReceiptIdSet;
	}
	
	/**
	 * Recalculate underwriter transactions 
	 * @param incomeIdToIncomeMap - Maps between ID and a underwriter transactions that might need recalc.
	 * 		The transactions should include related installments.
	 * @param incomeIdToReceiptsMap - Maps between underwriter transaction ID and its associated underwriter installments.
	 * @return A set of the IDs of all the transactions that got modified.
	 */
	public static Set<Id> recalculateUnderwriterTransactions(Map<Id, Novidea_HPC__Income__c> incomeIdToIncomeMap,
			Map<Id, List<HPC_Collection__Receipt__c>> incomeIdToReceiptsMap){
		Set<Id> modifiedIncomeIdSet = new Set<Id>();
		for(Novidea_HPC__Income__c curIncome : incomeIdToIncomeMap.values()){
			Boolean hasChanged = false;
			Decimal premiumBeforeGst = 0;
			Decimal gstAmountOnPremium = 0;
			Decimal payableToInsurer = 0;
			Decimal clientPayable = 0;
			Decimal commissionAmount = 0;
			Decimal commissionAmountDiscount = 0;
			Decimal commissionGST = 0;
			Decimal netBrokerage = 0;
			Decimal totalCommission = 0;
			Decimal serviceFee = 0;
			Decimal serviceFeeGST = 0;
			Decimal stampDuty = 0;
			Decimal policyCharges = 0;
			Decimal otherTaxes = 0;
			Decimal otherCharges = 0;
			Decimal iaLevyAmount = 0;
			Decimal mibAmount = 0;
			Decimal eciLevyAmount = 0;
			for(HPC_Collection__Receipt__c childReceipt : incomeIdToReceiptsMap.get(curIncome.Id)){
				premiumBeforeGst += childReceipt.HPC_Collection__Net_Premium__c != null ? childReceipt.HPC_Collection__Net_Premium__c : 0;
				gstAmountOnPremium += childReceipt.GST_Amount_on_Premium__c != null ? childReceipt.GST_Amount_on_Premium__c : 0;
				payableToInsurer += childReceipt.Payable_to_Insurer__c != null ? childReceipt.Payable_to_Insurer__c : 0;
				clientPayable += childReceipt.Client_Payable__c != null ? childReceipt.Client_Payable__c : 0;
				commissionAmount += childReceipt.HPC_Collection__Commission_Amount__c != null ? childReceipt.HPC_Collection__Commission_Amount__c : 0;
				commissionAmountDiscount += childReceipt.Commission_Amount_Discount__c != null ? childReceipt.Commission_Amount_Discount__c : 0;
				commissionGST += childReceipt.Commission_GST__c != null ? childReceipt.Commission_GST__c : 0;
				netBrokerage += childReceipt.Net_Brokerage__c != null ? childReceipt.Net_Brokerage__c : 0;
				serviceFee += childReceipt.Service_Fee__c != null ? childReceipt.Service_Fee__c : 0;
				serviceFeeGST += childReceipt.Service_Fee_GST__c != null ? childReceipt.Service_Fee_GST__c : 0;
				stampDuty += childReceipt.Stamp_Duty__c != null ? childReceipt.Stamp_Duty__c : 0;
				policyCharges += childReceipt.Policy_Charges__c != null ? childReceipt.Policy_Charges__c : 0;
				otherTaxes += childReceipt.Other_Taxes__c != null ? childReceipt.Other_Taxes__c : 0;
				otherCharges += childReceipt.Other_Charges__c != null ? childReceipt.Other_Charges__c : 0;
				iaLevyAmount += childReceipt.IA_Levy_Amount__c != null ? childReceipt.IA_Levy_Amount__c : 0;
				mibAmount += childReceipt.MIB_Amount__c != null ? childReceipt.MIB_Amount__c : 0;
				eciLevyAmount += childReceipt.ECI_Levy_Amount__c != null ? childReceipt.ECI_Levy_Amount__c : 0;					
			}
			Decimal hkTaxes = iaLevyAmount + mibAmount + eciLevyAmount;
			hasChanged |= changeIncomeVal(curIncome, 'Premium_before_GST__c', curIncome.Premium_before_GST__c, premiumBeforeGst);
			hasChanged |= changeIncomeVal(curIncome, 'GST_Amount_on_Premium__c', curIncome.GST_Amount_on_Premium__c, gstAmountOnPremium);
			hasChanged |= changeIncomeVal(curIncome, 'Novidea_HPC__Tax_on_Net_Premium__c', curIncome.Novidea_HPC__Tax_on_Net_Premium__c,
				premiumBeforeGst != 0 ? ((gstAmountOnPremium / premiumBeforeGst) * 100) : curIncome.Novidea_HPC__Tax_on_Net_Premium__c);
			hasChanged |= changeIncomeVal(curIncome, 'Novidea_HPC__Premium__c', curIncome.Novidea_HPC__Premium__c, premiumBeforeGst);
			hasChanged |= changeIncomeVal(curIncome, 'Payable_to_Insurer__c', curIncome.Payable_to_Insurer__c, payableToInsurer);
			hasChanged |= changeIncomeVal(curIncome, 'Premium_to_Pay__c', curIncome.Premium_to_Pay__c, (premiumBeforeGst + gstAmountOnPremium + hkTaxes));
			hasChanged |= changeIncomeVal(curIncome, 'Premium_To_Pay_With_Credit_Fee__c', curIncome.Premium_To_Pay_With_Credit_Fee__c, (clientPayable + hkTaxes));
			hasChanged |= changeIncomeVal(curIncome, 'Novidea_HPC__Commission_Amount__c', curIncome.Novidea_HPC__Commission_Amount__c, commissionAmount);
			hasChanged |= changeIncomeVal(curIncome, 'Commission_Discount__c', curIncome.Commission_Discount__c, commissionAmountDiscount);
			hasChanged |= changeIncomeVal(curIncome, 'Commission_GST__c', curIncome.Commission_GST__c, commissionGST);
			hasChanged |= changeIncomeVal(curIncome, 'Commission_discount_rate__c', curIncome.Commission_discount_rate__c, 
				commissionAmount != 0 ? (100 * commissionAmountDiscount / commissionAmount) : curIncome.Commission_discount_rate__c);
			hasChanged |= changeIncomeVal(curIncome, 'Total_Commission__c', curIncome.Total_Commission__c,
				(commissionAmount + commissionGST));
			//TODO: validate premiumBeforeGst != 0
			hasChanged |= changeIncomeVal(curIncome, 'Novidea_HPC__Commission_Percentage__c', curIncome.Novidea_HPC__Commission_Percentage__c,
				premiumBeforeGst != 0 ? (100 * commissionAmount / premiumBeforeGst) : curIncome.Novidea_HPC__Commission_Percentage__c);
			//TODO: validate commissionAmount != 0
			hasChanged |= changeIncomeVal(curIncome, 'Net_Brokerage__c', curIncome.Net_Brokerage__c, netBrokerage);
			hasChanged |= changeIncomeVal(curIncome, 'Service_Fee__c', curIncome.Service_Fee__c, serviceFee);
			hasChanged |= changeIncomeVal(curIncome, 'Service_Fee_GST__c', curIncome.Service_Fee_GST__c, serviceFeeGST);
			hasChanged |= changeIncomeVal(curIncome, 'Stamp_Duty__c', curIncome.Stamp_Duty__c, stampDuty);
			hasChanged |= changeIncomeVal(curIncome, 'Policy_Charges__c', curIncome.Policy_Charges__c, policyCharges);
			hasChanged |= changeIncomeVal(curIncome, 'Others_Taxes__c', curIncome.Others_Taxes__c, otherTaxes);
			hasChanged |= changeIncomeVal(curIncome, 'Other_Charges__c', curIncome.Other_Charges__c, otherCharges);			
			hasChanged |= changeIncomeVal(curIncome, 'IA_Levy_Amount__c', curIncome.IA_Levy_Amount__c, iaLevyAmount);
			hasChanged |= changeIncomeVal(curIncome, 'MIB_Amount__c', curIncome.MIB_Amount__c, mibAmount);
			hasChanged |= changeIncomeVal(curIncome, 'ECI_Levy_Amount__c', curIncome.ECI_Levy_Amount__c, eciLevyAmount);
			// hasChanged |= changeIncomeVal(curIncome, 'MIB__c', curIncome.MIB__c,
			// curIncome.Payable_to_Insurer__c!= 0 ? (100 * mibAmount / premiumBeforeGst) : curIncome.MIB__c);
			// hasChanged |= changeIncomeVal(curIncome, 'ECI_Levy__c', curIncome.ECI_Levy__c,
			// curIncome.Payable_to_Insurer__c != 0 ? (100 * eciLevyAmount / premiumBeforeGst) : curIncome.ECI_Levy__c);
			
			//Total other taxes and charges is a formula field
			if(hasChanged){
				modifiedIncomeIdSet.add(curIncome.Id);
			}
		}
		return modifiedIncomeIdSet;
	}

	/**
	 * Recalculate Client installments 
	 * @param idToClientReceiptMap - Maps between ID and a client installments that might need recalc.
	 * @param clientIdToUnderwriterReceiptsMap - maps between a client installment Id to relate underwriter installments.
	 * @return A set of the IDs of all the installments that got modified.
	 */
	public static Set<Id> recalculateClientInstallments(Map<Id, HPC_Collection__Receipt__c> idToClientReceiptMap,
			Map<Id, List<HPC_Collection__Receipt__c>> clientIdToUnderwriterReceiptsMap){
		Set<Id> modifiedReceiptIdSet = new Set<Id>();
		for(HPC_Collection__Receipt__c curReceipt : idToClientReceiptMap.values()){
			Boolean hasChanged = false;
			Decimal premiumBeforeGst = 0;
			Decimal gstAmountOnPremium = 0;
			Decimal payableToInsurer = 0;
			Decimal clientPayable = 0;
			Decimal commissionAmount = 0;
			Decimal commissionAmountDiscount = 0;
			Decimal commissionGST = 0;
			Decimal netBrokerage = 0;
			Decimal serviceFee = 0;
			Decimal serviceFeeGST = 0;
			Decimal stampDuty = 0;
			Decimal policyCharges = 0;
			Decimal otherTaxes = 0;
			Decimal otherCharges = 0;
			Decimal iaLevyAmount = 0;
			Decimal mibAmount = 0;
			Decimal eciLevyAmount = 0;
			if(clientIdToUnderwriterReceiptsMap.get(curReceipt.Id) != null){
				for(HPC_Collection__Receipt__c childReceipt : clientIdToUnderwriterReceiptsMap.get(curReceipt.Id)){
					premiumBeforeGst += childReceipt.HPC_Collection__Net_Premium__c != null ? childReceipt.HPC_Collection__Net_Premium__c : 0;
					gstAmountOnPremium += childReceipt.GST_Amount_on_Premium__c != null ? childReceipt.GST_Amount_on_Premium__c : 0;
					payableToInsurer += childReceipt.Payable_to_Insurer__c != null ? childReceipt.Payable_to_Insurer__c : 0;
					clientPayable += childReceipt.Client_Payable__c != null ? childReceipt.Client_Payable__c : 0;
					commissionAmount += childReceipt.HPC_Collection__Commission_Amount__c != null ? childReceipt.HPC_Collection__Commission_Amount__c : 0;
					commissionAmountDiscount += childReceipt.Commission_Amount_Discount__c != null ? childReceipt.Commission_Amount_Discount__c : 0;
					commissionGST += childReceipt.Commission_GST__c != null ? childReceipt.Commission_GST__c : 0;
					netBrokerage += childReceipt.Net_Brokerage__c != null ? childReceipt.Net_Brokerage__c : 0;
					serviceFee += childReceipt.Service_Fee__c != null ? childReceipt.Service_Fee__c : 0;
					serviceFeeGST += childReceipt.Service_Fee_GST__c != null ? childReceipt.Service_Fee_GST__c : 0;
					stampDuty += childReceipt.Stamp_Duty__c != null ? childReceipt.Stamp_Duty__c : 0;
					policyCharges += childReceipt.Policy_Charges__c != null ? childReceipt.Policy_Charges__c : 0;
					otherTaxes += childReceipt.Other_Taxes__c != null ? childReceipt.Other_Taxes__c : 0;
					otherCharges += childReceipt.Other_Charges__c != null ? childReceipt.Other_Charges__c : 0;

					iaLevyAmount += childReceipt.IA_Levy_Amount__c != null ? childReceipt.IA_Levy_Amount__c : 0;
					mibAmount += childReceipt.MIB_Amount__c != null ? childReceipt.MIB_Amount__c : 0;
					eciLevyAmount += childReceipt.ECI_Levy_Amount__c != null ? childReceipt.ECI_Levy_Amount__c : 0;
					
				}
				hasChanged |= changeReceiptVal(curReceipt, 'HPC_Collection__Net_Premium__c',
					curReceipt.HPC_Collection__Net_Premium__c, premiumBeforeGst);
				hasChanged |= changeReceiptVal(curReceipt, 'GST_Amount_on_Premium__c', curReceipt.GST_Amount_on_Premium__c, gstAmountOnPremium);
				hasChanged |= changeReceiptVal(curReceipt, 'Payable_to_Insurer__c', curReceipt.Payable_to_Insurer__c, payableToInsurer);
				hasChanged |= changeReceiptVal(curReceipt, 'Client_Payable__c', curReceipt.Client_Payable__c, clientPayable);
				hasChanged |= changeReceiptVal(curReceipt, 'HPC_Collection__Commission_Amount__c',
					curReceipt.HPC_Collection__Commission_Amount__c, commissionAmount);
				hasChanged |= changeReceiptVal(curReceipt, 'Commission_Amount_Discount__c',
					curReceipt.Commission_Amount_Discount__c, commissionAmountDiscount);
				hasChanged |= changeReceiptVal(curReceipt, 'Commission_GST__c', curReceipt.Commission_GST__c, commissionGST);
				hasChanged |= changeReceiptVal(curReceipt, 'Net_Brokerage__c', curReceipt.Net_Brokerage__c, netBrokerage);
				hasChanged |= changeReceiptVal(curReceipt, 'Service_Fee__c', curReceipt.Service_Fee__c, serviceFee);
				hasChanged |= changeReceiptVal(curReceipt, 'Service_Fee_GST__c', curReceipt.Service_Fee_GST__c, serviceFeeGST);
				hasChanged |= changeReceiptVal(curReceipt, 'Stamp_Duty__c', curReceipt.Stamp_Duty__c, stampDuty);
				hasChanged |= changeReceiptVal(curReceipt, 'Policy_Charges__c', curReceipt.Policy_Charges__c, policyCharges);
				hasChanged |= changeReceiptVal(curReceipt, 'Other_Taxes__c', curReceipt.Other_Taxes__c, otherTaxes);
				hasChanged |= changeReceiptVal(curReceipt, 'Other_Charges__c', curReceipt.Other_Charges__c, otherCharges);
				hasChanged |= changeReceiptVal(curReceipt, 'IA_Levy_Amount__c', curReceipt.IA_Levy_Amount__c, iaLevyAmount);
				hasChanged |= changeReceiptVal(curReceipt, 'MIB_Amount__c', curReceipt.MIB_Amount__c, mibAmount);
				hasChanged |= changeReceiptVal(curReceipt, 'ECI_Levy_Amount__c', curReceipt.ECI_Levy_Amount__c, eciLevyAmount);
				// hasChanged |= changeReceiptVal(curReceipt, 'MIB__c', curReceipt.MIB__c,
				// premiumBeforeGst!= 0 ? (100 * mibAmount / premiumBeforeGst) : curReceipt.MIB__c);
				// hasChanged |= changeReceiptVal(curReceipt, 'ECI_Levy__c', curReceipt.ECI_Levy__c,
				// premiumBeforeGst != 0 ? (100 * eciLevyAmount / premiumBeforeGst) : curReceipt.ECI_Levy__c);


				if(hasChanged){
					modifiedReceiptIdSet.add(curReceipt.Id);
				}
			}
		}
		return modifiedReceiptIdSet;
	}

	/**
	 * Recalculate Client transactions 
	 * @param incomeIdToIncomeMap - Maps between ID and a client transactions that might need recalc.
	 * 		The transactions should include related installments.
	 * @param incomeIdToReceiptsMap - Maps between client transaction ID and its associated client installments.
	 * @return A of set the IDs of all the transactions that got modified.
	 */
	public static Set<Id> recalculateClientTransactions(Map<Id, Novidea_HPC__Income__c> incomeIdToIncomeMap,
			Map<Id, List<HPC_Collection__Receipt__c>> incomeIdToReceiptsMap){
		return recalculateUnderwriterTransactions(incomeIdToIncomeMap, incomeIdToReceiptsMap);
	}

	/**
	 * Validates that amounts on underwriter transactions equals client transactions times policy breakdown percentage
	 * @param clientIncomeIdToIncomeMap - Maps between ID and a client transactions to validate.
	 * @param underwriterIncomeIdToIncomeMap - Maps between ID and a underwriter transactions to validate.
	 * @return A mapping between transactions ID and possible error message (no message signifies a valid transaction)
	 */
	public static Map<Id, String> validateTransactions(Map<Id, Novidea_HPC__Income__c> clientIncomeIdToIncomeMap,
			Map<Id, Novidea_HPC__Income__c> underwriterIncomeIdToIncomeMap){
		Map<Id, String> incomeIdToErrorMessageMap = new Map<Id, String>();
		Map<Id, Novidea_HPC__Income__c> clientIncomeIdToAggregatedIncome = new Map<Id, Novidea_HPC__Income__c>();
		for(Novidea_HPC__Income__c underwriterIncome : underwriterIncomeIdToIncomeMap.values()){
			Novidea_HPC__Income__c clientIncome = clientIncomeIdToIncomeMap.get(underwriterIncome.Novidea_HPC__Client_Income__c);
			if(clientIncome != null){
				if(!clientIncomeIdToAggregatedIncome.containsKey(clientIncome.Id)){
					clientIncomeIdToAggregatedIncome.put(clientIncome.Id, createIncome());
				}
				Novidea_HPC__Income__c mockIncome = clientIncomeIdToAggregatedIncome.get(clientIncome.Id);
				aggregateIncomes(underwriterIncome, mockIncome);
				String errorMessage = '';
				errorMessage = validateAssociatedCarrierAccount(underwriterIncome, errorMessage);
				errorMessage = validateUnderwriterTransaction(underwriterIncome, errorMessage);
				if(underwriterIncome.Novidea_HPC__Policy_Breakdown__c != null && 
						underwriterIncome.Novidea_HPC__Policy_Breakdown__r.Novidea_HPC__Signed_Line__c != null){
					Decimal calculatedGST = clientIncome.Novidea_HPC__Policy__r.RecordType.DeveloperName == 'Multi_Section' ? clientIncome.Premium_before_GST__c *
					(underwriterIncome.Novidea_HPC__Policy__r.Novidea_HPC__Apportionment__c / 100) * (underwriterIncome.Novidea_HPC__Policy_Breakdown__r.Novidea_HPC__Signed_Line__c / 100) : clientIncome.Premium_before_GST__c *
					(underwriterIncome.Novidea_HPC__Policy_Breakdown__r.Novidea_HPC__Signed_Line__c / 100);
					if(((calculatedGST - underwriterIncome.Premium_before_GST__c).abs()) > TOLERANCE){
						List<Object> params = new List<Object>();
						params.add(underwriterIncome.Premium_before_GST__c);
						params.add(underwriterIncome.name);
						params.add(calculatedGST);
						errorMessage += String.format(System.Label.Validation_Premium_before_GST_1, params);
						errorMessage += ' \n';
					}
				}
				if(String.isNotBlank(errorMessage)){
					errorMessage = String.format(System.Label.UWR_Transaction, new List<Object> {underwriterIncome.Name}) + ' \n' + errorMessage;
					incomeIdToErrorMessageMap.put(underwriterIncome.Id, errorMessage);
				}
			}
		}
		for(Novidea_HPC__Income__c clientIncome : clientIncomeIdToIncomeMap.values()){
			String errorMessage = '';
			errorMessage = validateAssociatedClientAccount(clientIncome, errorMessage);
			System.debug('net: ' + clientIncome.Net_Brokerage__c);
			errorMessage = validateIaLevyCap (clientIncome, errorMessage);
			errorMessage = validateClientTransactionFields(clientIncome, clientIncomeIdToAggregatedIncome.get(clientIncome.Id), errorMessage);
			if(String.isNotBlank(errorMessage)){
				errorMessage = String.format(System.Label.Client_Transaction, new List<Object> {clientIncome.Name}) + ' \n' + errorMessage;
				incomeIdToErrorMessageMap.put(clientIncome.Id, errorMessage);
			}
		}
		return incomeIdToErrorMessageMap;
	}

	private static Boolean changeReceiptVal(HPC_Collection__Receipt__c receipt, String fieldName, Decimal oldValue, Decimal newValue){
		receipt.put(fieldName, newValue);
		return oldValue != newValue;
	}

	private static Boolean changeIncomeVal(Novidea_HPC__Income__c income, String fieldName, Decimal oldValue, Decimal newValue){
		income.put(fieldName, newValue);
		return oldValue != newValue;
	}

	private static Novidea_HPC__Income__c createIncome(){
		Novidea_HPC__Income__c newIncome = new Novidea_HPC__Income__c(
			Premium_before_GST__c = 0,
			GST_Amount_on_Premium__c = 0,
			Novidea_HPC__Premium__c = 0,
			Payable_to_Insurer__c = 0,
			Premium_to_Pay__c = 0,
			Premium_To_Pay_With_Credit_Fee__c = 0,
			Novidea_HPC__Commission_Amount__c = 0,
			Commission_Discount__c = 0,
			Net_Brokerage__c = 0,
			Commission_GST__c = 0,
			Service_Fee__c = 0,
			Service_Fee_GST__c = 0,
			Stamp_Duty__c = 0,
			Policy_Charges__c = 0,
			Others_Taxes__c = 0,
			Other_Charges__c = 0, 
			IA_Levy_Amount__c = 0,
			MIB_Amount__c = 0,
			ECI_Levy_Amount__c = 0
		);
		return newIncome;
	}

	private static void aggregateIncomes(Novidea_HPC__Income__c incomeToAggregate, Novidea_HPC__Income__c aggregatedIncome){
		aggregatedIncome.Premium_before_GST__c += incomeToAggregate.Premium_before_GST__c != null ?
				incomeToAggregate.Premium_before_GST__c : 0;
		aggregatedIncome.GST_Amount_on_Premium__c += incomeToAggregate.GST_Amount_on_Premium__c != null ? incomeToAggregate.GST_Amount_on_Premium__c : 0;
		aggregatedIncome.Novidea_HPC__Premium__c += incomeToAggregate.Novidea_HPC__Premium__c != null ? incomeToAggregate.Novidea_HPC__Premium__c : 0;
		aggregatedIncome.Payable_to_Insurer__c += incomeToAggregate.Payable_to_Insurer__c != null ? incomeToAggregate.Payable_to_Insurer__c : 0;
		aggregatedIncome.Premium_to_Pay__c += incomeToAggregate.Premium_to_Pay__c != null ? incomeToAggregate.Premium_to_Pay__c : 0;
		aggregatedIncome.Premium_To_Pay_With_Credit_Fee__c += incomeToAggregate.Premium_To_Pay_With_Credit_Fee__c != null ?
			incomeToAggregate.Premium_To_Pay_With_Credit_Fee__c : 0;
		aggregatedIncome.Novidea_HPC__Commission_Amount__c += incomeToAggregate.Novidea_HPC__Commission_Amount__c != null ?
			incomeToAggregate.Novidea_HPC__Commission_Amount__c : 0;
		aggregatedIncome.Commission_Discount__c += incomeToAggregate.Commission_Discount__c != null ? incomeToAggregate.Commission_Discount__c : 0;
		aggregatedIncome.Net_Brokerage__c += incomeToAggregate.Net_Brokerage__c != null ? incomeToAggregate.Net_Brokerage__c : 0;
		aggregatedIncome.Commission_GST__c += incomeToAggregate.Commission_GST__c != null ? incomeToAggregate.Commission_GST__c : 0;
		aggregatedIncome.Service_Fee__c += incomeToAggregate.Service_Fee__c != null ? incomeToAggregate.Service_Fee__c : 0;
		aggregatedIncome.Service_Fee_GST__c += incomeToAggregate.Service_Fee_GST__c != null ? incomeToAggregate.Service_Fee_GST__c : 0;
		aggregatedIncome.Stamp_Duty__c += incomeToAggregate.Stamp_Duty__c != null ? incomeToAggregate.Stamp_Duty__c : 0;
		aggregatedIncome.Policy_Charges__c += incomeToAggregate.Policy_Charges__c != null ? incomeToAggregate.Policy_Charges__c : 0;
		aggregatedIncome.Others_Taxes__c += incomeToAggregate.Others_Taxes__c != null ? incomeToAggregate.Others_Taxes__c : 0;
		aggregatedIncome.Other_Charges__c += incomeToAggregate.Other_Charges__c != null ? incomeToAggregate.Other_Charges__c : 0;
		aggregatedIncome.IA_Levy_Amount__c += incomeToAggregate.IA_Levy_Amount__c != null ? incomeToAggregate.IA_Levy_Amount__c : 0;
		aggregatedIncome.MIB_Amount__c += incomeToAggregate.MIB_Amount__c != null ? incomeToAggregate.MIB_Amount__c : 0;
		aggregatedIncome.ECI_Levy_Amount__c += incomeToAggregate.ECI_Levy_Amount__c != null ? incomeToAggregate.ECI_Levy_Amount__c : 0;
	}

	//HWA-2304
	public static String validateFeeAccountPopulated(Novidea_HPC__Income__c clientIncome, String errorMessage){
		if(clientIncome.Fee_Account__c == null){
			errorMessage += System.Label.Validation_Fee_Account + ' \n';
		}
		return errorMessage;
	}

	private static String validateAssociatedCarrierAccount(Novidea_HPC__Income__c underwriterIncome, String errorMessage){
		if(underwriterIncome.Novidea_HPC__Carrier__r.NIBA__Has_Financial_Implications__c != true){
			errorMessage += String.format(System.Label.Validation_Associated_Carrier_Implications_1, new List<Object> {underwriterIncome.Novidea_HPC__Carrier__r.Name}) + ' \n';
		}
		if(underwriterIncome.Novidea_HPC__Carrier__r.NIBA__Accounting_Status__c != 'Open' &&
				underwriterIncome.Novidea_HPC__Carrier__r.NIBA__Accounting_Status__c != 'Run Off'){
			errorMessage += String.format(System.Label.Validation_Associated_Account_Status_1, new List<Object> {underwriterIncome.Novidea_HPC__Carrier__r.Name}) + ' \n';
		}
		return errorMessage;
	}

	private static String validateAssociatedClientAccount(Novidea_HPC__Income__c clientIncome, String errorMessage){
		if(clientIncome.Novidea_HPC__Client__r.NIBA__Has_Financial_Implications__c != true){
			errorMessage += String.format(System.Label.Validation_Associated_Client_Implications_1, new List<Object> {clientIncome.Novidea_HPC__Client__r.Name}) + ' \n';
		}
		if(clientIncome.Novidea_HPC__Client__r.NIBA__Accounting_Status__c != 'Open' &&
				clientIncome.Novidea_HPC__Client__r.NIBA__Accounting_Status__c != 'Run Off'){
			errorMessage += String.format(System.Label.Validation_Associated_Client_Status_1, new List<Object> {clientIncome.Novidea_HPC__Client__r.Name}) + ' \n';
		}
		return errorMessage;
	}

	private static String validateUnderwriterTransaction(Novidea_HPC__Income__c underwriterIncome, String errorMessage){
		if(underwriterIncome.Novidea_HPC__Policy_Breakdown__c == null || 
		underwriterIncome.Novidea_HPC__Policy_Breakdown__r.Novidea_HPC__Signed_Line__c == null){
			errorMessage += String.format(System.Label.Validation_Signed_Line_Required_1, new List<Object> {underwriterIncome.Novidea_HPC__Policy_Breakdown__r.Name}) + ' \n';
		}
		if(underwriterIncome.Brokerage_Account__c == null || underwriterIncome.Brokerage_Account_Definition_Status__c == 'Unable to define Brokerage Account'){
			errorMessage += System.Label.Validation_No_Brokrage_Account + ' \n';
		}
		if(underwriterIncome.Legal_Entity__c == null || underwriterIncome.Legal_Entity__r.Unit_4_Code__c != '2204HK00') { // IF not Hong Kong do the validation
			if(underwriterIncome.Tax_Account__c == null && underwriterIncome.Tax_Account_Definition_Status__c == 'Unable to define Tax Account'){
				errorMessage += System.Label.Validation_No_Tax_Account + ' \n';
			}
		}
		return errorMessage;
	}

	private static String validateIaLevyCap (Novidea_HPC__Income__c clientIncome, String errorMessage){
		if(clientIncome.Novidea_HPC__Policy__r.Policy_Aggregated_IA_Levy_Amount__c > clientIncome.Novidea_HPC__Policy__r.IA_Levy_Cap__c) {
			Decimal diff = clientIncome.Novidea_HPC__Policy__r.Policy_Aggregated_IA_Levy_Amount__c - clientIncome.Novidea_HPC__Policy__r.IA_Levy_Cap__c;
			errorMessage += String.format(System.Label.Validation_IA_Levy_Cap, new List<Object> {String.valueOf(diff)}) + ' \n';
		}

		return errorMessage;
	}

	private static String validateClientTransactionField(Novidea_HPC__Income__c clientIncome, Novidea_HPC__Income__c mockIncome, String errorMessage, String fieldName, String fieldLabel){
		Decimal diffExpectedVsActual = ((Decimal)clientIncome.get(fieldName) - (Decimal)mockIncome.get(fieldName)).abs();
		if(diffExpectedVsActual > TOLERANCE){
			errorMessage += System.Label.Please_Modify + ' ' + fieldLabel;
			Decimal diff = (Decimal)clientIncome.get(fieldName) - (Decimal)mockIncome.get(fieldName);
			errorMessage += '. ' + (diff > 0 ? System.Label.Add + ' ' : System.Label.Subtract + ' ') + Math.abs(diff) + ' \n';
		}
		return errorMessage;
	}

	private static String validateClientTransactionFields(Novidea_HPC__Income__c clientIncome, Novidea_HPC__Income__c mockIncome, String errorMessage){
		Map<String,Schema.SObjectField> incomeFields = Schema.getGlobalDescribe().get('Novidea_HPC__Income__c').getDescribe().fields.getMap();
		errorMessage = validateClientTransactionField(clientIncome, mockIncome, errorMessage, 'Premium_before_GST__c', incomeFields.get('Premium_before_GST__c').getDescribe().getLabel());
		errorMessage = validateClientTransactionField(clientIncome, mockIncome, errorMessage, 'GST_Amount_on_Premium__c', incomeFields.get('GST_Amount_on_Premium__c').getDescribe().getLabel());
		errorMessage = validateClientTransactionField(clientIncome, mockIncome, errorMessage, 'Novidea_HPC__Premium__c', incomeFields.get('Novidea_HPC__Premium__c').getDescribe().getLabel());
		errorMessage = validateClientTransactionField(clientIncome, mockIncome, errorMessage, 'Payable_to_Insurer__c', incomeFields.get('Payable_to_Insurer__c').getDescribe().getLabel());
		errorMessage = validateClientTransactionField(clientIncome, mockIncome, errorMessage, 'Premium_to_Pay__c',incomeFields.get('Premium_to_Pay__c').getDescribe().getLabel());
		errorMessage = validateClientTransactionField(clientIncome, mockIncome, errorMessage, 'Premium_To_Pay_With_Credit_Fee__c', incomeFields.get('Premium_To_Pay_With_Credit_Fee__c').getDescribe().getLabel());
		errorMessage = validateClientTransactionField(clientIncome, mockIncome, errorMessage, 'Novidea_HPC__Commission_Amount__c', incomeFields.get('Novidea_HPC__Commission_Amount__c').getDescribe().getLabel());
		errorMessage = validateClientTransactionField(clientIncome, mockIncome, errorMessage, 'Commission_Discount__c', incomeFields.get('Commission_Discount__c').getDescribe().getLabel());
		errorMessage = validateClientTransactionField(clientIncome, mockIncome, errorMessage, 'Net_Brokerage__c', incomeFields.get('Net_Brokerage__c').getDescribe().getLabel());
		errorMessage = validateClientTransactionField(clientIncome, mockIncome, errorMessage, 'Commission_GST__c', incomeFields.get('Commission_GST__c').getDescribe().getLabel());
		errorMessage = validateClientTransactionField(clientIncome, mockIncome, errorMessage, 'Service_Fee__c', incomeFields.get('Service_Fee__c').getDescribe().getLabel());
		errorMessage = validateClientTransactionField(clientIncome, mockIncome, errorMessage, 'Service_Fee_GST__c', incomeFields.get('Service_Fee_GST__c').getDescribe().getLabel());
		errorMessage = validateClientTransactionField(clientIncome, mockIncome, errorMessage, 'Stamp_Duty__c', incomeFields.get('Stamp_Duty__c').getDescribe().getLabel());
		errorMessage = validateClientTransactionField(clientIncome, mockIncome, errorMessage, 'Policy_Charges__c', incomeFields.get('Policy_Charges__c').getDescribe().getLabel());
		errorMessage = validateClientTransactionField(clientIncome, mockIncome, errorMessage, 'Others_Taxes__c', incomeFields.get('Others_Taxes__c').getDescribe().getLabel());
		errorMessage = validateClientTransactionField(clientIncome, mockIncome, errorMessage, 'Other_Charges__c', incomeFields.get('Other_Charges__c').getDescribe().getLabel());
		errorMessage = validateClientTransactionField(clientIncome, mockIncome, errorMessage, 'IA_Levy_Amount__c', incomeFields.get('IA_Levy_Amount__c').getDescribe().getLabel());
		errorMessage = validateClientTransactionField(clientIncome, mockIncome, errorMessage, 'MIB_Amount__c', incomeFields.get('MIB_Amount__c').getDescribe().getLabel());
		errorMessage = validateClientTransactionField(clientIncome, mockIncome, errorMessage, 'ECI_Levy_Amount__c', incomeFields.get('ECI_Levy_Amount__c').getDescribe().getLabel());
		return errorMessage;
	}
}