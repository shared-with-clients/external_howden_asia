@isTest(seeAllData = false)
public with sharing class ReceiptTriggerHelperTest {
    @TestSetup
    static void makeData(){

        HPC_Collection__Receipt__c  receipt = new HPC_Collection__Receipt__c(
            HPC_Collection__Commission_Amount__c = 1,
            HPC_Collection__Collection_Type__c = 'Office',
            Discount__c = 2,
            Commission_GST__c = 3,
           GST_Amount_on_Premium__c = 4,
           Net_Brokerage__c = 5,
           Other_Charges__c = 6,
           HPC_Collection__Premium_To_Pay__c = 7,
           HPC_Collection__Net_Premium__c = 8,
           Other_Taxes__c = 9,
           Payable_to_Insurer__c = 10,
           Policy_Charges__c = 11,
           Service_Fee__c = 12,
           Service_Fee_GST__c = 13,
           Client_Payable__c = 14,
           Commission_Amount_Discount__c = 15,
           Legal_Entity__c = null,
           Legal_Entity_Unit_4_Code__c = 'cancelledReceipt.Legal_Entity_Unit_4_Code__c;',
           Stamp_Duty__c = 16,
           Settlement_Commission_Discount_Amount__c = 17,
           Division__c = null,
           Department__c = null
        );
        Database.insert(receipt);
        HPC_Collection__Receipt__c  receipt2 = new HPC_Collection__Receipt__c(
            HPC_Collection__Client_Receipt__c = receipt.Id,
            HPC_Collection__Collection_Type__c = 'Office',
            HPC_Collection__Commission_Amount__c = 1,
            Discount__c = 2,
            Commission_GST__c = 3,
           GST_Amount_on_Premium__c = 4,
           Net_Brokerage__c = 5,
           Other_Charges__c = 6,
           HPC_Collection__Premium_To_Pay__c = 7,
           HPC_Collection__Net_Premium__c = 8,
           Other_Taxes__c = 9,
           Payable_to_Insurer__c = 10,
           Policy_Charges__c = 11,
           Service_Fee__c = 12,
           Service_Fee_GST__c = 13,
           Client_Payable__c = 14,
           Commission_Amount_Discount__c = 15,
           Legal_Entity__c = null,
           Legal_Entity_Unit_4_Code__c = 'cancelledReceipt.Legal_Entity_Unit_4_Code__c;',
           Stamp_Duty__c = 16,
           Settlement_Commission_Discount_Amount__c = 17,
           Division__c = null,
           Department__c = null
        );
        Database.insert(receipt2);
    }
    @isTest
    private static void updateUwrInstallmentsTest(){
        HPC_Collection__Receipt__c clientInstallment = [SELECT Id FROM HPC_Collection__Receipt__c WHERE HPC_Collection__Client_Receipt__c = null LIMIT 1];
        clientInstallment.HPC_Collection__Collection_Type__c = 'Carrier';
        Database.update(clientInstallment);

        HPC_Collection__Receipt__c uwrInstallment = [SELECT Id, HPC_Collection__Collection_Type__c FROM HPC_Collection__Receipt__c WHERE HPC_Collection__Client_Receipt__c != null LIMIT 1];
        System.assertEquals(clientInstallment.HPC_Collection__Collection_Type__c, uwrInstallment.HPC_Collection__Collection_Type__c, 'Expected collection type copied from client installment to uwr installment');
    }   

    @isTest
    private static void copyFieldsTest(){
        HPC_Collection__Receipt__c  receipt = new HPC_Collection__Receipt__c(
            HPC_Collection__Commission_Amount__c = 1,
            Discount__c = 2,
            Commission_GST__c = 3,
           GST_Amount_on_Premium__c = 4,
           Net_Brokerage__c = 5,
           Other_Charges__c = 6,
           HPC_Collection__Premium_To_Pay__c = 7,
           HPC_Collection__Net_Premium__c = 8,
           Other_Taxes__c = 9,
           Payable_to_Insurer__c = 10,
           Policy_Charges__c = 11,
           Service_Fee__c = 12,
           Service_Fee_GST__c = 13,
           Client_Payable__c = 14,
           Commission_Amount_Discount__c = 15,
           Legal_Entity__c = null,
           Legal_Entity_Unit_4_Code__c = 'cancelledReceipt.Legal_Entity_Unit_4_Code__c;',
           Stamp_Duty__c = 16,
           Settlement_Commission_Discount_Amount__c = 17,
           Division__c = null,
           Department__c = null
        );
        HPC_Collection__Receipt__c  copiedReceipt = new HPC_Collection__Receipt__c();
        ReceiptTriggerHelper.setDataFromCancelledReceipts(new List<HPC_Collection__Receipt__c>{receipt});
        ReceiptTriggerHelper.copyFields( copiedReceipt,  receipt);
    }
}
