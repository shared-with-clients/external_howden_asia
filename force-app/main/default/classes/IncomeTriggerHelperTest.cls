@isTest(seeAllData=false)
public with sharing class IncomeTriggerHelperTest{

	@TestSetup
	private static void makeData(){
		Novidea_HPC__General_Switches__c gs = new Novidea_HPC__General_Switches__c(Name = 'Default');
		Map<String,Id> accRtIds = NOVU.RecordTypeUtils.getIdsByDevNameAndFullSObjType(new List<String>{'Legal_Entity', 'Business', 'ReInsurer', 'Business', 'Accounting', 'Carrier'}, 'Account');
		Map<String,Id> incomeRtIds = NOVU.RecordTypeUtils.getIdsByDevNameAndFullSObjType(new List<String>{'Regular', 'MTA', 'uwr_mta', 'uwr_regular'}, 'Novidea_HPC__Income__c');


		//Create custom settings
		gs.Novidea_HPC__Income_Is_Approved_Field__c = 'HPC_Collection__Is_Approved__c';
		Database.insert(gs);
		Novidea_HPC.TriggerUtils.disableTrigger('enableHasReceiptReadyForCollectionIncome', IncomeTriggerHelperTest.class + '');
		Novidea_HPC__Trigger__c triggerCS = Novidea_HPC__Trigger__c.getInstance();
		triggerCS.Novidea_HPC__Stop_Validation_Rule__c  = true;
		upsert triggerCS;

		NBU__Settings__c nbuSettings = new NBU__Settings__c();
		nbuSettings.NBU__Base_Currency__c = 'SGD';
		insert nbuSettings;

		PolicyTriggerHelper.stopTrigger = true;

		//create accounts
		Account legalEntity = new Account(
			Novidea_HPC__Status__c = 'Approved',
			Name = 'legalEntity', 
			Account_Full_Name__c = 'legalEntity',
			Fee_Brokerage__c = true, 
			Account_Currency__c = 'USD', 
			RecordTypeId = accRtIds.get('Legal_Entity'), 
			NIBA__Account_Type__c = 'Other P&L',
			NBU__Legal_Entity_Base_Currency__c = 'SGD'
		);
		Database.insert(legalEntity);
		Account brokerSGD = new Account(
			Name='Brokerage SGD', 
			Account_Full_Name__c = 'Brokerage SGD',
			Legal_Entity__c = legalEntity.Id,
			Novidea_HPC__Status__c = 'Approved', 
			RecordTypeId = accRtIds.get('Accounting'),
			NIBA__Account_Type__c = 'Brokerage', 
			Brokerage_Rebate_Flag__c = false, 
			Deffered_Brokerage_Flag__c = false,
			Sub_Agent__c = false, 
			Account_Currency__c = 'SGD', 
			Business_Transaction_Type__c = 'New Business'
		);
		Account brokerDefferedSGD = new Account(
			Name='Brokerage SGD', 
			Account_Full_Name__c = 'Brokerage SGD',
			Legal_Entity__c = legalEntity.Id,
			Novidea_HPC__Status__c = 'Approved', 
			RecordTypeId = accRtIds.get('Accounting'),
			NIBA__Account_Type__c = 'Brokerage', 
			Brokerage_Rebate_Flag__c = false, 
			Deffered_Brokerage_Flag__c = true,
			Sub_Agent__c = false, 
			Account_Currency__c = 'SGD', 
			Business_Transaction_Type__c = 'New Business'
		);
		Account brokerSubAgentSGD = new Account(
			Name='Brokerage SGD', 
			Account_Full_Name__c = 'Brokerage SGD',
			Legal_Entity__c = legalEntity.Id,
			Novidea_HPC__Status__c = 'Approved', 
			RecordTypeId = accRtIds.get('Accounting'),
			NIBA__Account_Type__c = 'Brokerage', 
			Brokerage_Rebate_Flag__c = false, 
			Deffered_Brokerage_Flag__c = false,
			Sub_Agent__c = true, 
			Account_Currency__c = 'SGD', 
			Business_Transaction_Type__c = 'New Business'
		);
		

		Account bankAcc = new Account(
			Novidea_HPC__Status__c = 'Approved',
			Name = 'bankAcc', 
			Account_Full_Name__c = 'bankAcc',
			Fee_Brokerage__c = true, 
			Account_Currency__c = 'SGD', 
			Insurance_Class__c = 'RI', 
			NIBA__Account_Type__c = 'Bank', 
			RecordTypeId = accRtIds.get('Accounting') 
		);
		Account reinsurer = new Account(
			Novidea_HPC__Status__c = 'Approved',
			Name = 'reinsurer', 
			Account_Full_Name__c = 'reinsurer',
			Fee_Brokerage__c = true, 
			Account_Currency__c = 'SGD', 
			Insurance_Class__c = 'RI', 
			NIBA__Account_Type__c = 'Bank', 
			RecordTypeId = accRtIds.get('ReInsurer')
		);
		
        Account testAccountCarrier = new Account(
            Name = 'Test Account Carrier',
			Account_Full_Name__c = 'Test Account Carrier',
			RecordTypeId = accRtIds.get('Carrier'),
			Novidea_HPC__Status__c = 'Approved',
            NIBA__Re_Insurer_type__c = 'Non-Bureau Company',
			ShippingCountry = 'Singapore', 
			Offshore__c = 'Offshore');

        Account testAccountClient = new Account(
			Name = 'Test Account Client',
			Account_Full_Name__c = 'Test Account Client',
			Legal_Entity__c = legalEntity.Id, 
			Novidea_HPC__Status__c = 'Approved',
            RecordTypeId = accRtIds.get('Business'),
			ShippingCountry = 'United Kingdom');
			
		Database.insert(new List<Account>{brokerSGD, brokerDefferedSGD, brokerSubAgentSGD, testAccountClient, testAccountCarrier, bankAcc});
		
		Account taxAcc = new Account(
			Novidea_HPC__Status__c = 'Approved',
			Name = 'taxAcc', 
			Account_Full_Name__c = 'taxAcc',
			Tax_Status__c = 'Exempted', 
			Legal_Entity__c = legalEntity.Id, 
			Tax_Direction__c = 'Tax Out', 
			Fee_Brokerage__c = true, 
			Account_Currency__c = 'SGD', 
			NIBA__Bank_Currency__c = 'SGD', 
			Insurance_Class__c = 'RI', 
			NIBA__Account_Type__c = 'Other P&L', 
			RecordTypeId = accRtIds.get('Accounting')
		);
		Account brokerageFeeSGD = new Account(
			Novidea_HPC__Status__c = 'Approved',
			Name = 'Fee SGD', 
			Account_Full_Name__c = 'Fee SGD',
			Fee_Brokerage__c = true, 
			Account_Currency__c = 'SGD', 
			RecordTypeId = accRtIds.get('Accounting'), 
			NIBA__Account_Type__c = 'Other P&L',
			Legal_Entity__c = legalEntity.Id,
			Business_Transaction_Type__c = 'New Business',
			Division__c = 'Corporate Development'
		);
		Account brokerageFeeUSD = new Account(
			Novidea_HPC__Status__c = 'Approved',
			Name = 'Fee USD', 
			Account_Full_Name__c = 'Fee USD',
			Legal_Entity__c = legalEntity.Id,
			Fee_Brokerage__c = true, 
			Account_Currency__c = 'USD', 
			RecordTypeId = accRtIds.get('Accounting'), 
			NIBA__Account_Type__c = 'Other P&L',
			Division__c = 'Corporate Development'
		);
		Database.insert(new List<Account>{taxAcc, brokerageFeeUSD, brokerageFeeSGD});
		//Create product
		Novidea_HPC__Product__c prod = new Novidea_HPC__Product__c();
		Database.insert(prod);

		//Create product definition
		Novidea_HPC__Product_Def__c pDef = new Novidea_HPC__Product_Def__c(
			Debit_Note_Indicator__c = 'RI', 
			Code__c = '123', 
			GST_Applicable_On_Brokerage__c = 'Exempted', 
			GST_Applicable_On_Premium__c = 'Yes', 
			Life_Non_Life_Indicator__c = 'Life', 
			PPW_Applicable__c = 'No');
		Database.insert(pDef);

		//Create policy
		Novidea_HPC__Policy__c policy = new Novidea_HPC__Policy__c(
			Novidea_HPC__Apportionment__c = 100, 
			Legal_Entity__c = legalEntity.id, 
			Novidea_HPC__Division_Code__c = 'Corporate Development', 
			Novidea_HPC__Product__c = prod.Id, 
			Reinsurance_Broker__c = Userinfo.getUserId(), 
			Reinsurance_Bro__c = 'Yes',
			Novidea_HPC__Product_Definition__c = pDef.Id, 
			Novidea_HPC__Carrier__c = testAccountCarrier.Id, 
			Novidea_HPC__Client__c = testAccountClient.Id);
		Database.insert(policy);

		//Create policy breakdown
		Novidea_HPC__Policy_Breakdown__c pb = new Novidea_HPC__Policy_Breakdown__c(
			Novidea_HPC__Policy__c = policy.Id, 
			Novidea_HPC__Signed_Line__c = 50);
		Database.insert(pb);
		
		
		//Create incomes
		Novidea_HPC__Income__c oldIncome = new Novidea_HPC__Income__c(
			RecordTypeId = incomeRtIds.get('MTA'), 
            Novidea_HPC__Client__c = testAccountClient.Id, 
            Novidea_HPC__Policy__c = policy.Id, 
            Novidea_HPC__Carrier__c = testAccountCarrier.Id,
            Novidea_HPC__Number_of_Payments__c = 1,
            HPC_Collection__Period_Between_Payments__c = 12,
            HPC_Collection__First_Payment_Date__c = Date.today(),
			Premium_before_GST__c = 1000,
			Novidea_HPC__Endorsement_Number__c = 2,
			Transaction_Status__c = 'New',
			Novidea_HPC__Creation_Reason__c = 'Non-Financial',
			Novidea_HPC__Settlement_Currency__c = 'SGD',
			Novidea_HPC__Endorsement_Number_Text__c = '1',
			Legal_Entity__c = legalEntity.id, 
			Transaction_Type__c = 'New Business'
		);

		Novidea_HPC__Income__c oldIncome2 = new Novidea_HPC__Income__c(
			RecordTypeId = incomeRtIds.get('MTA'), 
			Legal_Entity__c = legalEntity.id, 
            Novidea_HPC__Client__c = testAccountClient.Id, 
            Novidea_HPC__Policy__c = policy.Id, 
            Novidea_HPC__Carrier__c = testAccountCarrier.Id,
            Novidea_HPC__Number_of_Payments__c = 2,
            HPC_Collection__Period_Between_Payments__c = 12,
            HPC_Collection__First_Payment_Date__c = Date.today()+1,
			Premium_before_GST__c = 1100,
			Novidea_HPC__Endorsement_Number__c = 2,
			Transaction_Status__c = 'New',
			Novidea_HPC__Creation_Reason__c = 'Non-Financial',
			Novidea_HPC__Settlement_Currency__c = 'SGD',
			Novidea_HPC__Endorsement_Number_Text__c = '2',
			Transaction_Type__c = 'New Business'
		);
		Novidea_HPC__Income__c income = new Novidea_HPC__Income__c(
			RecordTypeId = incomeRtIds.get('uwr_mta'), 
            Novidea_HPC__Client__c = testAccountClient.Id, 
            Novidea_HPC__Policy__c = policy.Id, 
            Novidea_HPC__Carrier__c = testAccountCarrier.Id,
            Novidea_HPC__Number_of_Payments__c = 3,
            HPC_Collection__Period_Between_Payments__c = 12,
            HPC_Collection__First_Payment_Date__c = Date.today(),
			Premium_before_GST__c = 1200,
			Novidea_HPC__Endorsement_Number__c = 2,
			Transaction_Status__c = 'New',
			Novidea_HPC__Creation_Reason__c = 'Extension',
			Novidea_HPC__Income_Reason__c = 'contra',
			Novidea_HPC__Settlement_Currency__c = 'SGD',
			Tax_Status__c = 'Exempted',
			Legal_Entity__c = legalEntity.Id,
			Transaction_Type__c = 'New Business'
		);

		Novidea_HPC__Income__c regIncome = new Novidea_HPC__Income__c(
			RecordTypeId = incomeRtIds.get('Regular'), 
			Legal_Entity__c = legalEntity.id, 
            Novidea_HPC__Client__c = testAccountClient.Id, 
            Novidea_HPC__Policy__c = policy.Id, 
            Novidea_HPC__Carrier__c = testAccountCarrier.Id,
			Novidea_HPC__Number_of_Payments__c = 1,
			HPC_Collection__Payment_Schedule__c = 'Annual',
            HPC_Collection__Period_Between_Payments__c = 12,
            HPC_Collection__First_Payment_Date__c = Date.today(),
			Premium_before_GST__c = 1000,
			Novidea_HPC__Endorsement_Number__c = 2,
			Approved_On__c = Datetime.now(),
		 	Approved_By__c = Userinfo.getUserId(),
			Transaction_Status__c = 'Approved',
			Novidea_HPC__Settlement_Currency__c = 'SGD',
			Transaction_Type__c = 'New Business',
			Tax_Status__c = 'Exempted',
			Policy_Charges__c = 1,
			Novidea_HPC__Commission_Amount__c = 1,
			Commission_Discount__c = 0,
			Commission_GST__c = 7,
			GST_Amount_on_Premium__c = 0.5,
			Insurer_Share__c = 0.2,
			Net_Brokerage__c = 1,
			Novidea_HPC__Premium__c = 1000,
			Other_Charges__c = 1,
			Others_Taxes__c = 1,
			Payable_to_Insurer__c = 500,
			Service_Fee__c = 1,
			Service_Fee_GST__c = 0.5,
			Total_Commission__c = 200,
			Premium_to_Pay__c = 300,
			Premium_To_Pay_With_Credit_Fee__c = 301
		);

		String uwrIncString = '{ "IsDeleted" : false, "CurrencyIsoCode" : "SGD", "RecordTypeId" : "", "Novidea_HPC__Policy__c" : "", "Novidea_HPC__Adjusted_Income__c" : null, "Novidea_HPC__Affects_Renewal__c" : false, "Novidea_HPC__Agency_Fee_Currency__c" : "SGD", "Novidea_HPC__Agency_Fee__c" : null, "Novidea_HPC__Agent_Discount_From_Production__c" : null, "Novidea_HPC__Agent_Discount_Percentage__c" : null, "Novidea_HPC__Agent_Discount__c" : null, "Novidea_HPC__Bordero_Date__c" : null, "Novidea_HPC__Carrier__c" : "", "Novidea_HPC__Client_Invoice__c" : null, "Novidea_HPC__Client__c" : "", "Novidea_HPC__Comment__c" : null, "Novidea_HPC__Commission_Amount_AgentB_From_Production__c" : null, "Novidea_HPC__Commission_Amount_Currency__c" : "SGD", "Novidea_HPC__Commission_Amount_from_Production__c" : null, "Novidea_HPC__Commission_From_Production__c" : null, "Novidea_HPC__Commission_Percentage__c" : 2.5, "Novidea_HPC__Creation_Reason__c" : null, "Novidea_HPC__Credit_Fees_From_Production__c" : null, "Novidea_HPC__Credit_Fees__c" : null, "Novidea_HPC__Date_Received__c" : null, "Novidea_HPC__Effective_Date__c" : "2020-07-21", "Novidea_HPC__Endorsement_Number__c" : 0.0, "Novidea_HPC__Expiration_Date__c" : "2021-07-20", "Novidea_HPC__Fees_From_Production__c" : null, "Novidea_HPC__Fees__c" : null, "Novidea_HPC__Incidents__c" : null, "Novidea_HPC__IncludeCreditFees__c" : false, "Novidea_HPC__Income_Reason__c" : "Unknown", "Novidea_HPC__Income_Recognition_Date__c" : null, "Novidea_HPC__Is_Cancelled__c" : false, "Novidea_HPC__Is_Fully_Collected__c" : false, "Novidea_HPC__Is_Updated_by_Production__c" : false, "Novidea_HPC__Local_Tax__c" : null, "Novidea_HPC__Main_Income__c" : null, "Novidea_HPC__Number_of_Payments__c" : 1.0, "Novidea_HPC__Original_Insurer_Net_Amount__c" : 8775.0, "Novidea_HPC__Original_Settlement_Rate__c" : 1.0, "Novidea_HPC__Other_Commission__c" : null, "Novidea_HPC__Payment_Type__c" : null, "Novidea_HPC__Policy_Breakdown__c" : "", "Novidea_HPC__Premium_Bruto__c" : 9000.0, "Novidea_HPC__Premium_Currency__c" : "SGD", "Novidea_HPC__Premium_From_Production__c" : null, "Novidea_HPC__Premium_To_Pay_With_Credit_Fee__c" : 9630.0, "Novidea_HPC__Premium_To_Pay__c" : 9630.0, "Novidea_HPC__Premium__c" : 9000.0, "Novidea_HPC__Product__c" : "", "Novidea_HPC__Production_Update_Date__c" : null, "Novidea_HPC__Received_Copy__c" : false, "Novidea_HPC__Renewal__c" : false, "Novidea_HPC__Seller_Broker_Commission_Amount__c" : null, "Novidea_HPC__Settlement_Agency_Fee_Formula__c" : 0.0, "Novidea_HPC__Settlement_Agent_Discount_Formula__c" : 0.0, "Novidea_HPC__Settlement_Agent_Discount__c" : null, "Novidea_HPC__Settlement_Commission_Amount_Formula__c" : 225.0, "Novidea_HPC__Settlement_Commission_Amount__c" : null, "Novidea_HPC__Settlement_Currency__c" : "SGD", "Novidea_HPC__Settlement_Insurer_Net_Amount__c" : 8775.0, "Novidea_HPC__Settlement_Premium_Amount_Formula__c" : 9000.0, "Novidea_HPC__Settlement_Premium_Amount__c" : null, "Novidea_HPC__Tax_On_Premium__c" : null, "Novidea_HPC__Tax_on_Net_Premium__c" : 7.0, "Novidea_HPC__Total_Commission__c" : 225.0, "Novidea_HPC__Transaction_Description__c" : null, "Novidea_HPC__Underwriter_Commission__c" : null, "Novidea_HPC__Value_Date__c" : "2020-07-21", "HPC_Collection__Adjusetment_Receipt__c" : null, "HPC_Collection__Adjusted_Income__c" : null, "HPC_Collection__Adjusted_Receipts_List__c" : null, "HPC_Collection__Agreement_Commission_Percentage__c" : null, "HPC_Collection__Been_Adjusted__c" : false, "HPC_Collection__Broker_Fee_Moved_to_Office_Account__c" : false, "HPC_Collection__Broker_Number__c" : null, "HPC_Collection__Calculation_Type__c" : null, "HPC_Collection__Cancelled__c" : false, "HPC_Collection__Commission_Amount_Adjustment__c" : null, "HPC_Collection__Commission_And_Fee_Transfer__c" : null, "HPC_Collection__Corrected_Commission_Amount__c" : 225.0, "HPC_Collection__Corrected_Fees__c" : 0.0, "HPC_Collection__Corrected_Net_Premium__c" : 9000.0, "HPC_Collection__Corrected_Other_Commission__c" : 0.0, "HPC_Collection__Corrected_Premium_to_Pay__c" : 9630.0, "HPC_Collection__Fees_Adjustment__c" : null, "HPC_Collection__First_Payment_Date__c" : "2020-07-21", "HPC_Collection__Income_Split__c" : null, "HPC_Collection__Is_Approved__c" : false, "HPC_Collection__Main_Income__c" : null, "HPC_Collection__Net_Premium_Adjustment__c" : null, "HPC_Collection__Other_Commission_Adjustment__c" : null, "HPC_Collection__Partialy_Cancelled__c" : false, "HPC_Collection__Payment_Schedule__c" : "Annual", "HPC_Collection__Period_Between_Payments_Type__c" : "Months", "HPC_Collection__Period_Between_Payments__c" : 12.0, "HPC_Collection__Policy_Premium_Collector__c" : null, "HPC_Collection__Policy_Seller_Broker_Id__c" : null,  "HPC_Collection__Return_Fee__c" : false, "HPC_Collection__Return_Premium_Type__c" : null, "HPC_Collection__Settled_By_Seller__c" : false, "HPC_Collection__Stamp__c" : null, "HPC_Collection__Transfer_Date__c" : null, "Previous_Policy_Number__c" : null, "Endorsement_Policy_Number__c" : "CV-0-20-", "Insurer_s_tax_invoice_received_date__c" : null, "Insurer_s_tax_invoice_number__c" : null, "PPW_Status__c" : null, "Waive_Status__c" : null, "Extended_Due_Date__c" : null, "Calculated_Tax__c" : 630.0, "Client_Transaction__c" : null, "Test_Trust_Bank_1__c" : null, "Transaction_Type__c" : null, "Transaction_Status__c" : "New", "Provisional_Bill__c" : false, "Premium_before_GST__c" : 9000.0, "Test_Trust_Bank_2__c" : null, "GST_Rate_on_Commission__c" : 7.0, "Test_Trust_Bank_3__c" : null, "Commission_Discount__c" : 0.0, "Novidea_HPC__Validation_Control__c" : false, "Novidea_HPC__Bordereau__c" : null, "Broker_Fee__c" : null, "Stamp_Duty__c" : null, "Policy_Charges__c" : null, "Others_Taxes__c" : null, "Service_Fee__c" : null, "Service_Fee_GST__c" : null, "Total_Other_Taxes_and_Charges__c" : 0.0, "Novidea_HPC__Client_Income__c" : "", "Novidea_HPC__Client_Payment_Terms__c" : null, "Novidea_HPC__Order__c" : 100.0, "Novidea_HPC__Original_Signing_Date__c" : null, "Novidea_HPC__Original_Signing_Number__c" : null, "Novidea_HPC__Original_Summary_Fees__c" : null, "Novidea_HPC__Original_Summary_Taxes__c" : null, "Novidea_HPC__Settlement_Summary_Fees_Formula__c" : 0.0, "Novidea_HPC__Settlement_Summary_Taxes_Formula__c" : 0.0, "Insurer_Share__c" : 90.0, "Lead_Insurer__c" : false, "Insurer_Endorsement_Number__c" : null, "Annual_Premium__c" : 0.0, "Pro_Rata_Calculation_Method__c" : null, "Internal_Current_Fiscal_Year__c" : 2020.0, "Insurer_Policy_Number__c" : null, "Other_Charges__c" : null, "Payable_to_Insurer__c" : 9389.25, "GST_Amount_on_Premium__c" : 630.0, "Commission_discount_rate__c" : 0.0, "Reversal_Reason__c" : null, "Reversal_Date__c" : null, "Premium_to_Pay__c" : 9630.0, "Premium_To_Pay_With_Credit_Fee__c" : 9630.0, "Policy_Period__c" : "2020-07-21 - 2021-07-20", "Novidea_HPC__Cancel_Income__c" : null, "Novidea_HPC__Closing_Doc_Numerator__c" : null, "Novidea_HPC__Closing_Doc_Type__c" : null, "Novidea_HPC__Deferred_Installments_Scheme__c" : null, "Novidea_HPC__Do_Not_Invoice__c" : false, "Novidea_HPC__Endorsement_Number_Text__c" : "0.0", "Novidea_HPC__Initial_Original_Settlement_Rate__c" : 1.0, "Novidea_HPC__Original_Summary_Commission__c" : null, "Novidea_HPC__Settlement_Due_Date__c" : null, "Novidea_HPC__Settlement_Summary_Commission__c" : null, "Novidea_HPC__Transaction_Type__c" : "PM", "DLRS_Total_Commission_Collected__c" : null, "DLRS_Total_Premium_Collected__c" : null, "Total_Commission__c" : 240.75, "Commission_GST__c" : 15.75, "Net_Brokerage__c" : 225.0, "Commission_Amount__c" : null, "Stamp_Policy_Charges__c" : null, "OtherCharges_OtherTaxes__c" : null, "ServiceFee_Service_FeeGST__c" : null, "Total_Other_Taxes_and_Charges_Internal__c" : 0.0, "Test_Value_Num_1__c" : null, "Is_Active_In_Filing_Criteria__c" : 1.0, "Test_Value_Num_2__c" : null, "Novidea_HPC__Endorsment__c" : null, "Novidea_HPC__Policy_Record_Type_Name__c" : "Policy", "Novidea_HPC__Original_Summary_Deducted_Taxes__c" : null, "Novidea_HPC__Settlement_Summary_Deducted_Tax_Formula__c" : 0.0, "Approved_On__c" : null, "Created_Date__c" : "2020-07-21T13:44:12.000+0000", "Plateform_Event_Ctrl__c" : "Published", "Novidea_HPC__Endorsement__c" : null, "Legal_Entity__c" : "", "Division__c" : "Strategic Solutions", "Department__c" : "None", "Tax_Status__c" : "Standard Rated", "Credit_Note_Date__c" : null, "Credit_Note_Number__c" : null, "Brokerage_Account__c" : null, "Orig_Base_Exchange_Rate__c" : 1.0, "Brokerage_Account_Definition_Status__c" : "Unable to define Brokerage Account", "AutoIncome_From_Subsidiary__c" : false, "Tax_Account__c" : "", "Tax_Account_Definition_Status__c" : "Unable to define Tax Account", "Ctrl_Transaction_for_Co_Broker__c" : null, "Test_Text__c" : null, "Brokerage_GST_in_SGD__c" : 225.0, "Full_Brokerage_in_Local_Currency__c" : 250.0, "Gross_Premium_Amount_Base_Currency__c" : 9000.0, "Gross_Brokerage_Amount_Base_Currency__c" : 250.0, "Service_Fee_Base_Currency__c" : 0.0, "Discount_Base_Currency__c" : 0.0, "Net_Brokerage_Base_Currency__c" : 225.0, "Net_premium_Base_Currency__c" : 9000.0, "Bank_Trust_Account__c" : null, "Approved_By__c" : null, "Premium_Discount__c" : 0.0, "Novidea_HPC__External_Id__c" : null, "Fee_Account__c" : null, "Debit_Credit_note_No__c" : null, "Service_Fee_Currency__c" : null, "Service_Fee_SGD__c" : null, "Novidea_HPC__Underwriter_Transaction_Created__c" : true, "Recalculate__c" : false, "Validation__c" : null, "DLRS_Total_Commission_GST__c" : null, "Warranty_Due_Date__c" : "2020-09-19", "Novidea_HPC__Carrier_Premium_Payment_Terms__c" : null, "Novidea_HPC__Replace_Income__c" : null, "Novidea_HPC__Warranty_Narrative__c" : null, "Computed_Amount__c" : 1583.01 }';
		Novidea_HPC__Income__c uwrIncome = (Novidea_HPC__Income__c)JSON.deserialize(uwrIncString, Novidea_HPC__Income__c.class);
		uwrIncome.RecordTypeId = incomeRtIds.get('uwr_regular');
		uwrIncome.Novidea_HPC__Policy__c = policy.Id;
		uwrIncome.Novidea_HPC__Carrier__c = testAccountCarrier.Id;
		uwrIncome.Novidea_HPC__Client__c = testAccountClient.Id;
		uwrIncome.Novidea_HPC__Policy_Breakdown__c = pb.Id;
		uwrIncome.Novidea_HPC__Product__c = null;
		uwrIncome.HPC_Collection__Is_Approved__c = true;
		uwrIncome.Transaction_Status__c = 'Approved';
		uwrIncome.Legal_Entity__c = legalEntity.id;

		Database.insert(new List<Novidea_HPC__Income__c>{regIncome, oldIncome, oldIncome2, income});

		Novidea_HPC__Income__c contraIncome = new Novidea_HPC__Income__c(
			RecordTypeId = incomeRtIds.get('Regular'), 
            Novidea_HPC__Client__c = testAccountClient.Id, 
            Novidea_HPC__Policy__c = policy.Id, 
            Novidea_HPC__Carrier__c = testAccountCarrier.Id,
            HPC_Collection__Period_Between_Payments__c = 12,
            HPC_Collection__First_Payment_Date__c = Date.today()+2,
			Premium_before_GST__c = -1000,
			Approved_On__c = Datetime.now(),
		 	Approved_By__c = Userinfo.getUserId(),
			Novidea_HPC__Settlement_Currency__c = 'SGD',
			Legal_Entity__c = legalEntity.id,
			Novidea_HPC__Cancel_Income__c = regIncome.Id
		);

		income.Novidea_HPC__Client_Income__c = oldIncome.Id;
		uwrIncome.Novidea_HPC__Client_Income__c = regIncome.Id;
		regIncome.Novidea_HPC__Adjusted_Income__c = oldIncome.Id;
		Test.startTest();

		Database.upsert(new List<Novidea_HPC__Income__c>{income, regIncome, uwrIncome, contraIncome});

		// Database.insert(new List<Novidea_HPC__Income__c>{uwrIncome, contraIncome});
		Test.stopTest();

		
	}

	@isTest(seeAllData=false)
	private static void Tests(){
		Test.startTest();
		Account carrier = [SELECT Id, Name FROM Account WHERE RecordType.DeveloperName = 'Carrier' LIMIT 1];
		Account client = [SELECT Id, Name FROM Account WHERE RecordType.DeveloperName = 'Business' LIMIT 1];
		Account taxAcc = [SELECT Id, Name FROM Account WHERE RecordType.DeveloperName = 'Accounting' AND Name = 'taxAcc' LIMIT 1];
		Account legalEntity = [SELECT Id, Name FROM Account WHERE RecordType.DeveloperName = 'Legal_Entity' LIMIT 1];


		Novidea_HPC__Policy__c policy = [SELECT Id, MTA_Count__c, Legal_Entity__c, Novidea_HPC__Division_Code__c, Novidea_HPC__Application__c FROM Novidea_HPC__Policy__c WHERE Novidea_HPC__Carrier__c = :carrier.Id AND Novidea_HPC__Client__c = :client.Id];
		Novidea_HPC__Policy_Breakdown__c pb = [SELECT Id, Novidea_HPC__Signed_Line__c FROM Novidea_HPC__Policy_Breakdown__c LIMIT 1];
		pb.Novidea_HPC__Signed_Line__c = 50;
		List<Novidea_HPC__Income__c> incomes = [
			SELECT Id, Novidea_HPC__Number_of_Payments__c, HPC_Collection__Payment_Schedule__c, 
			Novidea_HPC__Cancel_Income__r.HPC_Collection__Payment_Schedule__c,
			Novidea_HPC__Cancel_Income__r.Novidea_HPC__Number_of_Payments__c, Novidea_HPC__Settlement_Currency__c, 
			Fee_Account__r.Name, RecordType.DeveloperName, Novidea_HPC__Premium__c, HPC_Collection__First_Payment_Date__c, 
			Novidea_HPC__Client_Income__c, Tax_Account__c, Transaction_Type__c, Bank_Trust_Account__c, Legal_Entity__c, Division__c 
			FROM Novidea_HPC__Income__c];

		Novidea_HPC__Income__c income = new Novidea_HPC__Income__c();
		Novidea_HPC__Income__c oldIncome = new Novidea_HPC__Income__c();
		Novidea_HPC__Income__c oldIncome2 = new Novidea_HPC__Income__c();
		Novidea_HPC__Income__c regIncome = new Novidea_HPC__Income__c();
		Novidea_HPC__Income__c uwrIncome = new Novidea_HPC__Income__c();
		Novidea_HPC__Income__c contraIncome = new Novidea_HPC__Income__c();

		for(Novidea_HPC__Income__c incomeVar : incomes){
			system.debug(incomeVar);
			if(incomeVar.RecordType.DeveloperName == 'Regular' && incomeVar.HPC_Collection__First_Payment_Date__c == Date.today()){
				regIncome = incomeVar;
				system.debug(incomeVar);
			}else{
				if(incomeVar.RecordType.DeveloperName == 'uwr_regular'){
					uwrIncome = incomeVar;
					system.debug(incomeVar);
				}else{
					if(incomeVar.Novidea_HPC__Client_Income__c != null){
						income = incomeVar;
						system.debug(incomeVar);
					}else{
						if(incomeVar.HPC_Collection__First_Payment_Date__c == Date.today()){
							oldIncome = incomeVar;
							system.debug(incomeVar);
						}else if(incomeVar.HPC_Collection__First_Payment_Date__c == Date.today()+1){
							oldIncome2 = incomeVar;
							system.debug(incomeVar);
						}
						else if(incomeVar.HPC_Collection__First_Payment_Date__c == Date.today()+2){
							contraIncome = incomeVar;
							system.debug(incomeVar);
						}
					}
				}
			}
		}

		
		System.debug('starting asserts');
		//assignBankTrustAccount method
		System.assert(income.Bank_Trust_Account__c != null , 'Expected Bank_Trust_Account__c to be filled');
		//setEndorsementNumber method
		System.assertEquals(2, policy.MTA_Count__c, 'Expected MTA count to increase');
		//setLegalEntityDivisionAndDepartment method
		System.assertEquals(policy.Novidea_HPC__Division_Code__c, income.Division__c, 'Expected division populated in income from policy');
		//setMTATransactionType method
		System.assertEquals('Void', oldIncome.Transaction_Type__c, 'Expected oldIncome to get transaction type void');
		System.assertEquals('Void', oldIncome2.Transaction_Type__c, 'Expected income to get transaction type void from the oldIncome');
		//setContraTaxAccount method
		System.assertEquals(taxAcc.Id, income.Tax_Account__c, 'Expected matched tax account');
		//setDataFromClientIncome method
		System.assertEquals(regIncome.Novidea_HPC__Premium__c , uwrIncome.Novidea_HPC__Premium__c * 100 / pb.Novidea_HPC__Signed_Line__c, 'Expected field to be populated');
		//assignBrokerageFeeAccount method
		System.assertEquals('Fee SGD', income.Fee_Account__r.Name, 'Income is expected to be associasted with SGD fee account');
		//updateContraNumberOfPayments method
		System.assertEquals(contraIncome.Novidea_HPC__Number_of_Payments__c, contraIncome.Novidea_HPC__Cancel_Income__r.Novidea_HPC__Number_of_Payments__c, 'Contra income number of payments copied over from cancel income');
		System.assertEquals(contraIncome.HPC_Collection__Payment_Schedule__c, contraIncome.Novidea_HPC__Cancel_Income__r.HPC_Collection__Payment_Schedule__c, 'Contra income payment schedule copied over from cancel income');
		//CancellationOrMTALogic method
		regIncome.Transaction_Status__c = 'Cancelled';

		try{
			Database.update(regIncome);
		}catch(Exception ex){
			System.debug(ex.getMessage());
			//System.Assert(ex.getMessage().contains('Adjusted Transaction Cannot Be Cancelled'), 'Expected return validation');
		}
		Test.stopTest();
	}
}
