public class EnquiryConvertationController {

    @AuraEnabled
    public static Response validateConversion(String recordId) {
        try {
            Response resp;
            Novidea_HPC__Lead__c enquiryToConvert = [SELECT Id, Novidea_HPC__Client__c, Novidea_HPC__Status__c, Account_not_Exists_or_Unapproved__c, Novidea_HPC__Client__r.Novidea_HPC__Status__c FROM Novidea_HPC__Lead__c WHERE Id =: recordId];           
            system.debug('enquiryToConvert ' + enquiryToConvert);
            if(enquiryToConvert.Novidea_HPC__Status__c != 'Converted') {
                if(enquiryToConvert.Novidea_HPC__Client__c != null) {
                    if(enquiryToConvert.Account_not_Exists_or_Unapproved__c) {
                        resp = new Response('Error', 'Assigned client account to the Enquiry is not Approved yet or has no related contacts');
                    } else {
                        resp = new Response('Success', JSON.deserializeUntyped(Novidea_HPC.ButtonsAPI.autoLeadConvertionToApplication(enquiryToConvert.Id)));
                    }
                } else {
                    resp = new Response('Success', JSON.deserializeUntyped(Novidea_HPC.ButtonsAPI.autoLeadConvertionToApplication(enquiryToConvert.Id)));
                   /* Account accountToInsert =  new Account(Name = 'New Account for ' + enquiryToConvert.Id, Novidea_HPC__Status__c = 'Uapproced');
                    insert accountToInsert;*/
                }
            } else {
               resp = new Response('Error', 'Lead is already converted'); 
            }
            return resp;
        } catch(Exception e) {
            system.debug('Exception ' + e.getMessage() + e.getLineNumber());
            return new Response('Error', 'Convertion exception. Please, contact your administrator!');
        }
    }
}