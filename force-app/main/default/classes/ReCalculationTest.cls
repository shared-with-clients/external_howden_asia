@isTest
public with sharing class ReCalculationTest {

    @testSetup
	static void init(){
        Map<String,Id> accountRtIds = NOVU.RecordTypeUtils.getIdsByDevNameAndFullSObjType(new String[]{'Carrier','Business'},'Account');
        Account carrier1 = new Account(name = 'Carrier1', Account_Full_Name__c = 'Carrier1', NIBA__Accounting_Code__c = '1', NIBA__Accounting_Status__c = 'Open', NIBA__Has_Financial_Implications__c=true,
        RecordTypeId = accountRtIds.get('Carrier'), NIBA__Re_Insurer_type__c = 'Non-Bureau Company', BillingStreet = 'Shalom', Offshore__c= 'Offshore');   
        Account client1 = new Account(name = 'Client1', Account_Full_Name__c = 'Client1', NIBA__Accounting_Code__c = '3', NIBA__Accounting_Status__c = 'Open', NIBA__Has_Financial_Implications__c=true,
        RecordTypeId = accountRtIds.get('Business'), NIBA__Re_Insurer_type__c = 'Non-Bureau Company', BillingStreet = 'Shalom');
        Database.insert(new List<Account>{carrier1, client1});       
       
        PolicyTriggerHelper.stopTrigger = true;
        
        Novidea_HPC__Product_Def__c prodacDef1 = new Novidea_HPC__Product_Def__c(
            Name = 'productDef1', Code__c ='123',
            GST_Applicable_On_Premium__c = 'Yes',
            Life_Non_Life_Indicator__c = 'Life',
            PPW_Applicable__c ='Yes',
            GST_Applicable_On_Brokerage__c = 'Standard Rated',
            Novidea_HPC__DeveloperName__c = 'productDef11', 
            Novidea_HPC__Bureau_Code__c = 'test1');
        Database.insert(new Novidea_HPC__Product_Def__c[]{prodacDef1});

        Novidea_HPC__Policy__c policy1 = new Novidea_HPC__Policy__c(Novidea_HPC__Carrier__c = carrier1.Id, Novidea_HPC__Client__c = client1.Id,
			Novidea_HPC__Effective_Date__c = Date.today(), Novidea_HPC__Expiration_Date__c = Date.today().addYears(1).addDays(-1),
            Novidea_HPC__Policy_Number__c = 'PM12', Novidea_HPC__Product_Definition__c = prodacDef1.Id);
        Database.insert(new List<Novidea_HPC__Policy__c>{policy1});

        //PolicyTriggerHelper.stopTrigger = false;
        Novidea_HPC__Policy_Breakdown__c breakdown1 = new Novidea_HPC__Policy_Breakdown__c(Name='Brd1',
            Novidea_HPC__Carrier__c = carrier1.id,
            Novidea_HPC__Policy__c = policy1.id,
            Novidea_HPC__Agent_Discount_Percentage__c = 5,
            Novidea_HPC__Commission_Breakdown__c = 20, Novidea_HPC__Credit_Fee_Breakdown__c = 10, Novidea_HPC__Fees_Breakdown__c = 15,
            Novidea_HPC__Premium_Breakdown__c = 25, Novidea_HPC__Premium_Currency__c = 'USD',
            Novidea_HPC__Settlement_Currency__c = 'USD', Novidea_HPC__Value_Date__c = Date.today(), Novidea_HPC__Written_Line_Percentage__c = 32,
            Novidea_HPC__Status__c = 'Active', Novidea_HPC__Sub_Status__c = 'Initial',
            Novidea_HPC__Line_Type__c = 'Line to stand', Novidea_HPC__Market__c = 'IUA', Novidea_HPC__Slip_Role__c = 'Lead', Novidea_HPC__Carriers_Ref__c = 'test', 
            Novidea_HPC__Claims_Agreement_New__c	= 'No',
            Novidea_HPC__Signed_Line__c = 35, Lead_Insurer__c = true);
        Database.insert(new Novidea_HPC__Policy_Breakdown__c[]{breakdown1});

        //client income
        Id regularRecordTypeId = NOVU.RecordTypeUtils.getIdByDevNameAndFullSObjType('regular','Novidea_HPC__Income__c');
        Novidea_HPC__Income__c income1 = new Novidea_HPC__Income__c(
            RecordTypeId = regularRecordTypeId,
            Novidea_HPC__Policy__c = policy1.Id,
            Novidea_HPC__Effective_Date__c = Date.today(),
            Novidea_HPC__Expiration_Date__c = Date.today().addYears(1).addDays(-1),
            Novidea_HPC__Commission_Amount__c = 100,
            Novidea_HPC__Commission_Amount_Currency__c = 'USD',
            Novidea_HPC__Premium__c = 1000,  
            Novidea_HPC__Premium_Currency__c = 'USD',
            Novidea_HPC__Number_of_Payments__c = 1, 
            Novidea_HPC__Underwriter_Commission__c = 15,
            Novidea_HPC__Client__c = client1.Id,
            Novidea_HPC__Agent_Discount__c = 10,
            Novidea_HPC__Agency_Fee__c = 5,
            Novidea_HPC__Original_Settlement_Rate__c = 1, 
            Novidea_HPC__Settlement_Currency__c = 'USD',
            Novidea_HPC__Commission_Percentage__c = 98,
            HPC_Collection__Period_Between_Payments__c = 12, 
            Commission_GST__c = 98,
            Premium_before_GST__c= 100,
            GST_Amount_on_Premium__c= 200,
            Payable_to_Insurer__c = 56,
            Premium_To_Pay_With_Credit_Fee__c = 56,
            Net_Brokerage__c = 45,
            Service_Fee__c = 56, Service_Fee_GST__c = 25,
            Stamp_Duty__c=85, Policy_Charges__c =58, 
            Others_Taxes__c =10, Other_Charges__c=12,
            Novidea_HPC__Tax_on_Net_Premium__c = 6, 
            Premium_to_Pay__c = 25,
            Commission_Discount__c = 23, 
            Commission_discount_rate__c =2);

        //Fee income
        Id feeRecordTypeId = NOVU.RecordTypeUtils.getIdByDevNameAndFullSObjType('Fee','Novidea_HPC__Income__c');
        Novidea_HPC__Income__c income2 = new Novidea_HPC__Income__c(
            RecordTypeId = feeRecordTypeId,
            Novidea_HPC__Policy__c = policy1.Id,
            Novidea_HPC__Effective_Date__c = Date.today(),
            Novidea_HPC__Expiration_Date__c = Date.today().addYears(1).addDays(-1),
            Novidea_HPC__Commission_Amount__c = 100,
            Novidea_HPC__Commission_Amount_Currency__c = 'USD',
            Novidea_HPC__Premium__c = 1000,  
            Novidea_HPC__Premium_Currency__c = 'USD',
            Novidea_HPC__Number_of_Payments__c = 1, 
            Novidea_HPC__Underwriter_Commission__c = 15,
            Novidea_HPC__Client__c = client1.Id,
            Novidea_HPC__Agent_Discount__c = 10,
            Novidea_HPC__Agency_Fee__c = 5,
            Novidea_HPC__Original_Settlement_Rate__c = 1, 
            Novidea_HPC__Settlement_Currency__c = 'USD',
            Novidea_HPC__Commission_Percentage__c = 98,
            HPC_Collection__Period_Between_Payments__c = 12, 
            Commission_GST__c = 98,
            Premium_before_GST__c= 100,
            GST_Amount_on_Premium__c= 200,
            Payable_to_Insurer__c = 56,
            Premium_To_Pay_With_Credit_Fee__c = 56,
            Net_Brokerage__c = 45,
            Service_Fee__c = 56, Service_Fee_GST__c = 25,
            Stamp_Duty__c=85, Policy_Charges__c =58, 
            Others_Taxes__c =10, Other_Charges__c=12,
            Novidea_HPC__Tax_on_Net_Premium__c = 6, 
            Premium_to_Pay__c = 25,
            Commission_Discount__c = 23, 
            Commission_discount_rate__c =2);
        Database.insert(new Novidea_HPC__Income__c[]{income1, income2});

        //Underwriter income
        Id uwrRegularRecordTypeId = NOVU.RecordTypeUtils.getIdByDevNameAndFullSObjType('uwr_regular','Novidea_HPC__Income__c');
        Novidea_HPC__Income__c incomeUnderwriter1 = new Novidea_HPC__Income__c(
            RecordTypeId = uwrRegularRecordTypeId,
            Novidea_HPC__Policy__c = policy1.Id,
            Novidea_HPC__Effective_Date__c = Date.today(),
            Novidea_HPC__Expiration_Date__c = Date.today().addYears(1).addDays(-1),
            Novidea_HPC__Commission_Amount__c = 100,
            Novidea_HPC__Commission_Amount_Currency__c = 'USD',
            Novidea_HPC__Premium__c = 1000,
            Novidea_HPC__Premium_Currency__c = 'USD',
            Novidea_HPC__Number_of_Payments__c = 1,
            HPC_Collection__Period_Between_Payments__c = 12,
            Novidea_HPC__Underwriter_Commission__c = 15,
            Novidea_HPC__Carrier__c = carrier1.Id,
            Novidea_HPC__Client_Income__c = income1.Id,
            Novidea_HPC__Agent_Discount__c = 10,
            Novidea_HPC__Agency_Fee__c = 5,
            Novidea_HPC__Original_Settlement_Rate__c = 1,
            Novidea_HPC__Settlement_Currency__c = 'USD',
            Novidea_HPC__Tax_on_Net_Premium__c = 2,
            Novidea_HPC__Commission_Percentage__c = 98,
            Commission_discount_rate__c = 0.568,
            Commission_Discount__c = 100,
            Commission_GST__c = 300,
            Premium_To_Pay_With_Credit_Fee__c = 56,
            ///Total_Other_Taxes_and_Charges__c = 40, // not writeable
            Novidea_HPC__Policy_Breakdown__c = breakdown1.Id);
        Database.insert(new Novidea_HPC__Income__c[]{incomeUnderwriter1});


        List<HPC_Collection__Receipt__c> underwriterReceipts = [
            SELECT Id
            FROM HPC_Collection__Receipt__c
            WHERE HPC_Collection__Income__c = :incomeUnderwriter1.Id
        ];

        List<HPC_Collection__Receipt__c> clientReceipts = [
            SELECT Id
            FROM HPC_Collection__Receipt__c
            WHERE HPC_Collection__Income__c = :income1.Id
        ];

        for(HPC_Collection__Receipt__c receipt : underwriterReceipts){
            receipt.HPC_Collection__Client_Receipt__c = clientReceipts[0].Id;
            receipt.HPC_Collection__Net_Premium__c = 1;
            receipt.HPC_Collection__Commission_Amount__c = 1;
        }
        Database.update(underwriterReceipts);
    }
    
    @isTest
    public static void doRecalculationTest(){
        Test.startTest();
        List<ReCalculation.TransactionRow> rows = new List<ReCalculation.TransactionRow>();
        ReCalculation.TransactionRow row = new ReCalculation.TransactionRow();
        row.incomeId = [SELECT Id FROM Novidea_HPC__Income__c WHERE RecordType.DeveloperName != 'Fee' LIMIT 1].Id;
        row.isSuccess = true;
        row.Message = '';
        rows.add(row);
        ReCalculation.TransactionRow row2 = new ReCalculation.TransactionRow();
        row2.incomeId = [SELECT Id FROM Novidea_HPC__Income__c WHERE RecordType.DeveloperName = 'Fee' LIMIT 1].Id;
        row2.isSuccess = true;
        row2.Message = '';
        rows.add(row2);
        ReCalculation.TransactionRow row3 = new ReCalculation.TransactionRow();
        row3.incomeId = [SELECT Id FROM Novidea_HPC__Income__c WHERE RecordType.DeveloperName = 'uwr_regular' LIMIT 1].Id;
        row3.isSuccess = true;
        row3.Message = '';
        rows.add(row3);
        List<ReCalculation.TransactionRow> transactionRowResult = ReCalculation.doRecalculation(rows);
        System.assertEquals(3, transactionRowResult.size()); //message not empty
        Test.stopTest();

    }

    @isTest(seeAllData=false)
    public static void doRecalculationClientTransactionTest(){
        Test.startTest();
        List<ReCalculation.TransactionRow> rows = new List<ReCalculation.TransactionRow>();
        ReCalculation.TransactionRow row = new ReCalculation.TransactionRow();
        row.incomeId = [SELECT Id FROM Novidea_HPC__Income__c WHERE RecordType.DeveloperName = 'regular' LIMIT 1].Id;
        row.isSuccess = true;
        row.Message = '';
        rows.add(row);
        ReCalculation.TransactionRow row2 = new ReCalculation.TransactionRow();
        //row2.incomeId = [SELECT Id FROM Novidea_HPC__Income__c WHERE RecordType.DeveloperName = 'regular' LIMIT 1].Id;
        //row2.isSuccess = true;
        //row2.Message = '';
        rows.add(row2);
        
        List<ReCalculation.TransactionRow> transactionRowResult = ReCalculation.doRecalculation(rows);
        System.assertEquals(2, transactionRowResult.size()); //message not empty
        Test.stopTest();

    }
}