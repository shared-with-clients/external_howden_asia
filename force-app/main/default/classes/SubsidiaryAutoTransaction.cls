public with sharing class SubsidiaryAutoTransaction {
	/**
	 *  Creates subsidary transactions
	 */
	@InvocableMethod(label='Subsidiary Auto Transaction')
	/**
	 *  Creates subsidary transactions according to a given list of risks
	 *  @param ids - list of the input risk IDs
	 */
	public static void createSubsidiaryAutoTransaction(List<Id> ids) {
		Savepoint savePoint = Database.setSavepoint();
		try {
			List<Novidea_HPC__Income__c> incomesToInsert = new List<Novidea_HPC__Income__c>();

			List<Novidea_HPC__Account_Risk__c> subsidiaries = [
				SELECT Id, Novidea_HPC__Product__r.Novidea_HPC__Policy__c, Novidea_HPC__Product__r.Novidea_HPC__Policy__r.Novidea_HPC__Client__c, 
					Novidea_HPC__Product__r.Novidea_HPC__Premium_Currency__c, Novidea_HPC__Product__r.Novidea_HPC__Effective_Date__c, Novidea_HPC__Product__r.Novidea_HPC__Expiration_Date__c,
					Novidea_HPC__Product__r.Novidea_HPC__Commission__c, Novidea_HPC__Product__r.Novidea_HPC__Commission_Currency__c, Novidea_HPC__Account__c, 
					Novidea_HPC__Product__r.Novidea_HPC__Policy__r.Legal_Entity__c, Novidea_HPC__Product__r.Novidea_HPC__Policy__r.Novidea_HPC__Division_Code__c, Novidea_HPC__Product__r.Novidea_HPC__Policy__r.Department__c,
					Novidea_HPC__Order__c, Novidea_HPC__Product__r.Novidea_HPC__Premium__c
				FROM Novidea_HPC__Account_Risk__c 
				WHERE Id IN :ids
					AND Novidea_HPC__Client__c = true

			];

			RecordType incomeRecordType = [
				SELECT Id 
				FROM RecordType 
				WHERE DeveloperName = 'Regular' 
					AND IsActive = true 
					AND SobjectType = 'Novidea_HPC__Income__c'
			];

			List<String> policyList = new List<String>();
			List<String> clientList = new List<String>();

			for(Novidea_HPC__Account_Risk__c subsidiary : subsidiaries){
				policyList.Add(subsidiary.Novidea_HPC__Product__r.Novidea_HPC__Policy__c);
				clientList.Add(subsidiary.Novidea_HPC__Product__r.Novidea_HPC__Policy__r.Novidea_HPC__Client__c);
			}

			List<Novidea_HPC__Income__c> mainIncomeList = [
				SELECT Id, Novidea_HPC__Client__c, Novidea_HPC__Policy__c, Novidea_HPC__Number_of_Payments__c
				FROM Novidea_HPC__Income__c
				WHERE RecordTypeId = :incomeRecordType.Id
					AND Novidea_HPC__Client__c IN :clientList
					AND Novidea_HPC__Policy__c IN :policyList
				ORDER BY CreatedDate Asc
			];

			Map<String, Novidea_HPC__Income__c> clientPolicyToIncomeMap = createIncomeMap(mainIncomeList);

			Decimal totalAutoIncomesPremiumBeforeGST = 0;
			for(Novidea_HPC__Account_Risk__c subsidiary : subsidiaries){
				String key = subsidiary.Novidea_HPC__Product__r.Novidea_HPC__Policy__r.Novidea_HPC__Client__c + '-' + subsidiary.Novidea_HPC__Product__r.Novidea_HPC__Policy__c;

				if(clientPolicyToIncomeMap.containsKey(key)){
					Novidea_HPC__Income__c income = clientPolicyToIncomeMap.get(key);
					Decimal premiumBeforeGST = subsidiary.Novidea_HPC__Product__r.Novidea_HPC__Premium__c * (subsidiary.Novidea_HPC__Order__c / 100);

					incomesToInsert.Add(createNewIncome(subsidiary, income, premiumBeforeGST));
					totalAutoIncomesPremiumBeforeGST += premiumBeforeGST;
				}
			}
			if(!incomesToInsert.isEmpty()){
				Database.insert(incomesToInsert, !Test.IsRunningTest());
			}

			//at tests some of the incomes might fail the insert process and that’s why we “filter” them
			Map<Id, Novidea_HPC__Income__c> incomesToInsertMap = new Map<Id, Novidea_HPC__Income__c>(); 
			for(Novidea_HPC__Income__c income : incomesToInsert){

				if(income.Id != null){
					incomesToInsertMap.put(income.Id, income);
				}
			}

			incomesToInsert = incomesToInsertMap.values();

			//TODO check whether the main incomes or newly created incomes are changed in a manner that may affect this flow.
			subsidiaries = [
				SELECT Id, Novidea_HPC__Product__r.Novidea_HPC__Policy__c, Novidea_HPC__Product__r.Novidea_HPC__Policy__r.Novidea_HPC__Client__c, 
					Novidea_HPC__Product__r.Novidea_HPC__Premium_Currency__c, Novidea_HPC__Product__r.Novidea_HPC__Effective_Date__c, Novidea_HPC__Product__r.Novidea_HPC__Expiration_Date__c,
					Novidea_HPC__Product__r.Novidea_HPC__Commission__c, Novidea_HPC__Product__r.Novidea_HPC__Commission_Currency__c, Novidea_HPC__Account__c, 
					Novidea_HPC__Product__r.Novidea_HPC__Policy__r.Legal_Entity__c, Novidea_HPC__Product__r.Novidea_HPC__Policy__r.Novidea_HPC__Division_Code__c, Novidea_HPC__Product__r.Novidea_HPC__Policy__r.Department__c,
					Novidea_HPC__Order__c, Novidea_HPC__Product__r.Novidea_HPC__Premium__c
				FROM Novidea_HPC__Account_Risk__c
				WHERE Id IN :ids 
					AND Novidea_HPC__Product__c != null
					AND Novidea_HPC__Client__c = true
			];

			Map<Id, Novidea_HPC__Income__c> fullIncomeMap = new Map<Id, Novidea_HPC__Income__c>([
				SELECT Id, Net_Brokerage__c, Premium_before_GST__c, Novidea_HPC__Client__c, Novidea_HPC__Policy__c
				FROM Novidea_HPC__Income__c 
				WHERE (RecordTypeId = :incomeRecordType.Id 
					AND Novidea_HPC__Client__c IN :clientList 
					AND Novidea_HPC__Policy__c IN :policyList)
				OR Id IN :incomesToInsert
				ORDER BY CreatedDate ASC
			]);

			incomesToInsert = new List<Novidea_HPC__Income__c>();
			mainIncomeList = new List<Novidea_HPC__Income__c>();

			for(Novidea_HPC__Income__c income : fullIncomeMap.values()){
				if(incomesToInsertMap.containsKey(income.Id)){
					incomesToInsert.Add(income);
				}else{
					mainIncomeList.Add(income);
				}
			}

			Map<String, Novidea_HPC__Income__c> clientPolicyToMainIncomeMap = createIncomeMap(mainIncomeList);
			Map<String, Novidea_HPC__Income__c> clientPolicyToincomesToInsertMap = createIncomeMap(incomesToInsert);

			List<Novidea_HPC__Income__c> incomeToUpdate = New List<Novidea_HPC__Income__c>();
			for(Novidea_HPC__Account_Risk__c subsidiary : subsidiaries){
				String key = subsidiary.Novidea_HPC__Product__r.Novidea_HPC__Policy__r.Novidea_HPC__Client__c + '-' + subsidiary.Novidea_HPC__Product__r.Novidea_HPC__Policy__c;

				if(clientPolicyToMainIncomeMap.containsKey(key)){
					Novidea_HPC__Income__c income = clientPolicyToMainIncomeMap.get(key);
					if(income.Premium_before_GST__c != null){
						income.Premium_before_GST__c = income.Premium_before_GST__c - totalAutoIncomesPremiumBeforeGST;
						incomeToUpdate.Add(income);
					}
					
				}

				if(clientPolicyToincomesToInsertMap.containsKey(key)){
					Novidea_HPC__Income__c income = clientPolicyToincomesToInsertMap.get(key);
					if(income.Net_Brokerage__c != null && income.Premium_before_GST__c != null && subsidiary.Novidea_HPC__Product__r.Novidea_HPC__Premium__c != null){
						Decimal netBrokerage = income.Net_Brokerage__c;
						income.Net_Brokerage__c = (income.Net_Brokerage__c * income.Premium_before_GST__c) / subsidiary.Novidea_HPC__Product__r.Novidea_HPC__Premium__c;

						if(income.Net_Brokerage__c != netBrokerage){
							incomeToUpdate.Add(income);
						}
					}
				}
			}

			if(!incomeToUpdate.isEmpty()){
				Map<Id, Novidea_HPC__Income__c> incomeToUpdateSetMap = new Map<Id, Novidea_HPC__Income__c>(incomeToUpdate); 
				Database.update(new List<Novidea_HPC__Income__c>(incomeToUpdateSetMap.values()));
			}

			List<Novidea_HPC__Income__c> finalIncomeToUpdate = New List<Novidea_HPC__Income__c>();

			Map<Id, Novidea_HPC__Income__c> finalIncomeMap = new Map<Id, Novidea_HPC__Income__c>([
				SELECT Id, Net_Brokerage__c, Novidea_HPC__Commission_Amount__c, Commission_Discount__c, 
					Novidea_HPC__Policy__r.Sum_of_the_Co_Broker_Commission_Amount__c, Premium_before_GST__c, Novidea_HPC__Client__c
				FROM Novidea_HPC__Income__c 
				WHERE Id IN :incomeToUpdate
				ORDER BY CreatedDate Asc
			]);

			Map<String, Novidea_HPC__Income__c> clientPolicyToFinalIncomeMap = createIncomeMap(finalIncomeMap.values());
			
			for(Novidea_HPC__Account_Risk__c subsidiary : subsidiaries){
				String key = subsidiary.Novidea_HPC__Product__r.Novidea_HPC__Policy__r.Novidea_HPC__Client__c + '-' + subsidiary.Novidea_HPC__Product__r.Novidea_HPC__Policy__c;

				if(clientPolicyToFinalIncomeMap.containsKey(key)){
					Novidea_HPC__Income__c income = clientPolicyToFinalIncomeMap.get(key);

					if(income.Novidea_HPC__Commission_Amount__c != null && income.Commission_Discount__c != null &&
					income.Novidea_HPC__Policy__r.Sum_of_the_Co_Broker_Commission_Amount__c != null &&
					income.Premium_before_GST__c != null &&
					subsidiary.Novidea_HPC__Product__r.Novidea_HPC__Premium__c != 0){
						Decimal netBrokerage = income.Novidea_HPC__Commission_Amount__c - income.Commission_Discount__c - (income.Novidea_HPC__Policy__r.Sum_of_the_Co_Broker_Commission_Amount__c * 
						income.Premium_before_GST__c / subsidiary.Novidea_HPC__Product__r.Novidea_HPC__Premium__c);
						if(income.Net_Brokerage__c != netBrokerage){
							income.Net_Brokerage__c = netBrokerage;
							income.Plateform_Event_Ctrl__c = Novidea_HPC.TriggerUtils.isTriggerEnabled('SubsidiaryAutoTransactionPlatformEvents') ? 'Published' : null;
							finalIncomeMap.put(income.Id, income);
						}
					}
				}
			}

			if(!finalIncomeMap.isEmpty()){
				List<Database.SaveResult> finalIncomeToUpdateResults = Database.update(finalIncomeMap.values());
			}

			if (Novidea_HPC.TriggerUtils.isTriggerEnabled('SubsidiaryAutoTransactionPlatformEvents')){
				firePlatformEvents(finalIncomeMap.keyset());
	        }

		} catch (Exception e) {
			System.debug(e.getMessage() + ' - ' + e.getLineNumber() + ' - ' + e.getStackTraceString());
			Database.rollback(savePoint);
		}
	}

	private static Map<String, Novidea_HPC__Income__c> createIncomeMap(List<Novidea_HPC__Income__c> incomeList){
		Map<String, Novidea_HPC__Income__c> createdIncomeMap = new Map<String, Novidea_HPC__Income__c>();

		for(Novidea_HPC__Income__c income : incomeList){
			String key = income.Novidea_HPC__Client__c + '-' + income.Novidea_HPC__Policy__c;

			if(!createdIncomeMap.containsKey(key)){
				createdIncomeMap.put(key, income);
			}
		}
		return createdIncomeMap;
	}

	private static Novidea_HPC__Income__c createNewIncome(Novidea_HPC__Account_Risk__c subsidiary, Novidea_HPC__Income__c income, Decimal premiumBeforeGST){
		Novidea_HPC__Income__c newIncome = new Novidea_HPC__Income__c(
			AutoIncome_From_Subsidiary__c = true,
			Novidea_HPC__Premium_Currency__c = subsidiary.Novidea_HPC__Product__r.Novidea_HPC__Premium_Currency__c,
			Premium_before_GST__c = premiumBeforeGST,
			Novidea_HPC__Policy__c = subsidiary.Novidea_HPC__Product__r.Novidea_HPC__Policy__c,
			Novidea_HPC__Number_of_Payments__c = income.Novidea_HPC__Number_of_Payments__c,
			Novidea_HPC__Premium__c = premiumBeforeGST,
			HPC_Collection__First_Payment_Date__c = subsidiary.Novidea_HPC__Product__r.Novidea_HPC__Effective_Date__c,
			Novidea_HPC__Expiration_Date__c = subsidiary.Novidea_HPC__Product__r.Novidea_HPC__Expiration_Date__c,
			Novidea_HPC__Effective_Date__c = subsidiary.Novidea_HPC__Product__r.Novidea_HPC__Effective_Date__c,
			Novidea_HPC__Commission_Percentage__c = subsidiary.Novidea_HPC__Product__r.Novidea_HPC__Commission__c,
			Novidea_HPC__Commission_Amount_Currency__c = subsidiary.Novidea_HPC__Product__r.Novidea_HPC__Commission_Currency__c,
			Novidea_HPC__Client__c = subsidiary.Novidea_HPC__Account__c,
			Legal_Entity__c = subsidiary.Novidea_HPC__Product__r.Novidea_HPC__Policy__r.Legal_Entity__c,
			Division__c = subsidiary.Novidea_HPC__Product__r.Novidea_HPC__Policy__r.Novidea_HPC__Division_Code__c,
			Department__c = subsidiary.Novidea_HPC__Product__r.Novidea_HPC__Policy__r.Department__c,
			HPC_Collection__Period_Between_Payments__c = 12 / (income.Novidea_HPC__Number_of_Payments__c != null ? income.Novidea_HPC__Number_of_Payments__c : 1));

			return newIncome;
		
	}

	private static void firePlatformEvents(Set<Id> incomeIds){
		List<HPC_Collection__Receipt__c> relatedReceipts = [SELECT Id FROM HPC_Collection__Receipt__c WHERE HPC_Collection__Income__c IN :incomeIds];
		List<ReceiptEvent__e> platformEventsToFire = new List<ReceiptEvent__e>();

		for(HPC_Collection__Receipt__c receipt_i : relatedReceipts){
			ReceiptEvent__e platformEventToFire = new ReceiptEvent__e(
				PublishedRecordId__c = receipt_i.Id,
				ObjectName__c = 'Receipt');
			platformEventsToFire.Add(platformEventToFire);
		}

		if(!platformEventsToFire.isEmpty()){
			EventBus.publish(platformEventsToFire);
		}
	}
}