public with sharing class PolicyTriggerHelper {
	public static Boolean stopTrigger = false;
	public static Boolean inTrigger = false;
	private static final Set<String> CHANGED_FIELDS = new Set<String>{'Novidea_HPC__Client__c', 'Novidea_HPC__Carrier__c', 'Risk_Location__c', 'Novidea_HPC__Status__c', 'Novidea_HPC__Application__c', 'Client_Cedant_Code__c', 
	'Novidea_HPC__Product_Definition__c', 'Novidea_HPC__Effective_Date__c', 'Legal_Entity__c', 'IA_Levy_Exemption__c'};
	public static void onBeforeInsert(List<Novidea_HPC__Policy__c> newPolicies, Map<Id, Novidea_HPC__Policy__c> newPoliciesMap){
		if(stopTrigger || inTrigger){
			return;
		}
		inTrigger = true;
			
		Map<Id, List<Novidea_HPC__Policy_Breakdown__c>> policyIdToPolicyBreakdownsMap = new Map<Id, List<Novidea_HPC__Policy_Breakdown__c>>();
		Map<Id, Id> policyIdToProductId = new Map<Id, Id>();
		populateLegalEntityManualPolicy(newPolicies);
		if(validateQuotation(newPolicies)){
			//assignProductDefinition(newPolicies, newPoliciesMap, policyIdToProductId);
			Map<Id, Account> policyIdToClientMap = assignClientCedantCode(newPolicies, newPoliciesMap);
			validateMASApproval(newPolicies, newPoliciesMap, policyIdToClientMap, policyIdToPolicyBreakdownsMap, true, new Map<Id, Novidea_HPC__Policy__c>());
		}
		setIaLevyTaxAndCap(newPolicies, null);
		inTrigger = false;
	}

	public static void onBeforeUpdate(List<Novidea_HPC__Policy__c> newPolicies, List<Novidea_HPC__Policy__c> oldPolicies, Map<Id, Novidea_HPC__Policy__c> newPoliciesMap, Map<Id, Novidea_HPC__Policy__c> oldPoliciesMap){
		if(stopTrigger || inTrigger){
			return;
		}
		inTrigger = true;
		IncomeTriggerHelper.MapComparisonResult compareResult = IncomeTriggerHelper.compareRecordsFieldsInclude(newPoliciesMap.values(), oldPoliciesMap, CHANGED_FIELDS);
		if(!compareResult.isEqual){
			List<Id> placementConfirmedPolicies = new List<Id>();
			Map<Id, Id> policyIdToProductId = new Map<Id, Id>();
			Map<Id, List<Novidea_HPC__Policy_Breakdown__c>> policyIdToPolicyBreakdownsMap = new Map<Id, List<Novidea_HPC__Policy_Breakdown__c>>();
			if(validateLeadInsurer(newPolicies, oldPolicies, newPoliciesMap, oldPoliciesMap, placementConfirmedPolicies)){
				//assignProductDefinition(newPolicies, newPoliciesMap, policyIdToProductId);
				assignProcess(newPolicies, oldPolicies, newPoliciesMap, oldPoliciesMap);
				Map<Id, Account> policyIdToClientMap = assignClientCedantCode(newPolicies, newPoliciesMap);
				validateMASApproval(newPolicies, newPoliciesMap, policyIdToClientMap, policyIdToPolicyBreakdownsMap, false, new Map<Id, Novidea_HPC__Policy__c>());
				placementConfirmedLogic(newPolicies, oldPolicies, newPoliciesMap, oldPoliciesMap, policyIdToPolicyBreakdownsMap, placementConfirmedPolicies, policyIdToProductId);
			}
			setIaLevyTaxAndCap(newPolicies, oldPoliciesMap);
		}
		inTrigger = false;
	}

	private static void populateLegalEntityManualPolicy(List<Novidea_HPC__Policy__c> newPolicies){
		Set<Id> clientIds = new Set<Id>();
		for(Novidea_HPC__Policy__c policy : newPolicies){
			clientIds.add(policy.Novidea_HPC__Client__c);
		}

		Map<Id, Account> clientMap = new Map<Id, Account>([
			SELECT Id, Legal_Entity__c, Legal_Entity__r.Unit_4_Code__c
			FROM Account
			WHERE Id IN :clientIds
		]);

		for(Novidea_HPC__Policy__c policy : newPolicies){
			if(policy.Novidea_HPC__Client__c != null){
				policy.Legal_Entity__c = clientMap.get(policy.Novidea_HPC__Client__c).Legal_Entity__c;
				policy.Legal_Entity_Unit_4_Code__c = clientMap.get(policy.Novidea_HPC__Client__c).Legal_Entity__r.Unit_4_Code__c;
			}
		}
	}

	private static void placementConfirmedLogic(List<Novidea_HPC__Policy__c> newPolicies, List<Novidea_HPC__Policy__c> oldPolicies, Map<Id, Novidea_HPC__Policy__c> newPoliciesMap,
			Map<Id, Novidea_HPC__Policy__c> oldPoliciesMap, Map<Id, List<Novidea_HPC__Policy_Breakdown__c>> policyIdToPolicyBreakdownsMap, List<Id> placementConfirmedPolicies, Map<Id, Id> policyIdToProductId){        
		if(placementConfirmedPolicies.isEmpty()){
			return;
		}
		List<AggregateResult> securities = [
			SELECT SUM(Share_Percentage__c) sumPrecentage, Novidea_HPC_Policy_Breakdown__r.Novidea_HPC__Policy__c
			FROM Securities__c
			WHERE Novidea_HPC_Policy_Breakdown__r.Novidea_HPC__Policy__c IN :placementConfirmedPolicies
			GROUP BY Novidea_HPC_Policy_Breakdown__r.Novidea_HPC__Policy__c
		];
		for(AggregateResult aggResult : securities){
			newPoliciesMap.get((Id)aggResult.get('Novidea_HPC__Policy__c')).put('Total_Share_Percentage_of_Securities__c', (Decimal)aggResult.get('sumPrecentage'));
		}

		Map<Id, AggregateResult> policyIdToSubsidiaryMap = new Map<Id, AggregateResult>();
		//if(policyIdToProductId.isEmpty()){
		//    policyIdToProductId = populatePolicyToProductMap(newPoliciesMap, false);
		//}
		System.debug('policyIdToProductId size: ' + policyIdToProductId.size());
		//if(!policyIdToProductId.isEmpty()){
			//Set<Id> uniqueProductIdsSet = new Set<Id>(policyIdToProductId.values());
			//List<Id> productIds = new List<Id>(uniqueProductIdsSet);
		List<AggregateResult> subsidiaries = [
			SELECT SUM(Novidea_HPC__Order__c) sumOrder, SUM(Amount__c) sumAmount, Novidea_HPC__Product__c, Novidea_HPC__Product__r.Novidea_HPC__Policy__c policy
			FROM Novidea_HPC__Account_Risk__c
			WHERE Novidea_HPC__Product__r.Novidea_HPC__Policy__c IN :newPolicies
				AND Novidea_HPC__Client__c = true
			GROUP BY Novidea_HPC__Product__c, Novidea_HPC__Product__r.Novidea_HPC__Policy__c
		];
		System.debug('subsidiaries size: ' + subsidiaries.size());
		for(AggregateResult subsidiary : subsidiaries){
			policyIdToSubsidiaryMap.put((Id)subsidiary.get('policy'), subsidiary);
		}

		//}
		Map<Id, Novidea_HPC__Income__c> policyIdToIncomeMap = new Map<Id, Novidea_HPC__Income__c>();
		List<Novidea_HPC__Income__c> incomes = [
			SELECT Novidea_HPC__Premium_To_Pay_With_Credit_Fee__c, Novidea_HPC__Policy__c, Premium_before_GST__c
			FROM Novidea_HPC__Income__c
			WHERE Novidea_HPC__Policy__c IN :placementConfirmedPolicies
				AND RecordType.DeveloperName = 'Regular'
		];
		Decimal sumPremiumBeforeGST = 0;
		for(Novidea_HPC__Income__c income : incomes){
			policyIdToIncomeMap.put(income.Novidea_HPC__Policy__c, income);
			if(income.get('Premium_before_GST__c') != null){
				sumPremiumBeforeGST += (Decimal)income.get('Premium_before_GST__c');
			}
		   
		}
		for(Id policyId : placementConfirmedPolicies){
			Novidea_HPC__Policy__c tmpPolicy = newPoliciesMap.get(policyId);
			if(policyIdToSubsidiaryMap.containsKey(tmpPolicy.Id)){
				AggregateResult tmpSubsidiary = policyIdToSubsidiaryMap.get(tmpPolicy.Id);
				tmpPolicy.Total_percentage_paid_by_Subsidiaries__c = (Decimal)tmpSubsidiary.get('sumOrder');
				tmpPolicy.Total_amount_paid_by_Subsidiaries__c = (Decimal)tmpSubsidiary.get('sumAmount');
			}
			if(policyIdToIncomeMap.containsKey(policyId)){
				tmpPolicy.Transaction_Payable_Amount__c = policyIdToIncomeMap.get(policyId).Novidea_HPC__Premium_To_Pay_With_Credit_Fee__c;
			}
		}
		
		List<AggregateResult> policyBreakdows = [SELECT SUM(Novidea_HPC__Premium_Amount__c) sumPremiumAmount, SUM(Novidea_HPC__Signed_Line__c) sumConfirmedPremium, Novidea_HPC__Policy__c 
		FROM Novidea_HPC__Policy_Breakdown__c 
		WHERE Novidea_HPC__Policy__c IN :placementConfirmedPolicies 
		GROUP BY Novidea_HPC__Policy__c];
		for(AggregateResult aggPolicyBreakdow : policyBreakdows){
			if((Decimal)aggPolicyBreakdow.get('sumConfirmedPremium') > 100){
				//Total Confirmed Percentage of all Policy Breakdowns should not be more than 100%' => message
				newPoliciesMap.get((Id)aggPolicyBreakdow.get('Novidea_HPC__Policy__c')).put('Validation__c', 'Total Confirmed Percentage');
			}
			if((Decimal)aggPolicyBreakdow.get('sumPremiumAmount') > sumPremiumBeforeGST){
				// Total premium amount on Policy Breakdowns should not be more than total of Premium before GST paid by the Client => message
				newPoliciesMap.get((Id)aggPolicyBreakdow.get('Novidea_HPC__Policy__c')).put('Validation__c', 'Total Premium Amount');
			}
		}
	}

	private static void assignProcess(List<Novidea_HPC__Policy__c> newPolicies, List<Novidea_HPC__Policy__c> oldPolicies, Map<Id, Novidea_HPC__Policy__c> newPoliciesMap, Map<Id, Novidea_HPC__Policy__c> oldPoliciesMap){
		Map<Id, Id> policyIdToproductDefinitionIdMap = new Map<Id, Id>();
		for(Novidea_HPC__Policy__c policy : newPolicies){
			if(policy.Novidea_HPC__Product_Definition__c != null && policy.Novidea_HPC__Process__c == null){
				policyIdToproductDefinitionIdMap.put(policy.Id, policy.Novidea_HPC__Product_Definition__c);
			}
		}
		if(!policyIdToproductDefinitionIdMap.isEmpty()){
			Set<Id> uniqueProductDefinitionIdsSet = new Set<Id>(policyIdToproductDefinitionIdMap.values());
			List<Id> productDefinitionIds = new List<Id>(uniqueProductDefinitionIdsSet);
			List<Novidea_HPC__Process_Connection__c> processConnections = [
				SELECT Novidea_HPC__Process__c, Novidea_HPC__Product_Def__c 
				FROM Novidea_HPC__Process_Connection__c 
				WHERE Novidea_HPC__Product_Def__c IN :productDefinitionIds
			];
			if(!processConnections.isEmpty()){
				Map<Id, Id> productDefinitionIdsToProcessIdsMap = new Map<Id, Id>();
				for(Novidea_HPC__Process_Connection__c processConnection : processConnections){
					productDefinitionIdsToProcessIdsMap.put(processConnection.Novidea_HPC__Product_Def__c, processConnection.Novidea_HPC__Process__c);
				}
				for(Id policyId : policyIdToproductDefinitionIdMap.keySet()){
					newPoliciesMap.get(policyId).Novidea_HPC__Process__c = productDefinitionIdsToProcessIdsMap.get(newPoliciesMap.get(policyId).Novidea_HPC__Product_Definition__c);
				}
			}
		}
	}

	private static Boolean validateLeadInsurer(List<Novidea_HPC__Policy__c> newPolicies, List<Novidea_HPC__Policy__c> oldPolicies,
			Map<Id, Novidea_HPC__Policy__c> newPoliciesMap, Map<Id, Novidea_HPC__Policy__c> oldPoliciesMap, List<Id> placementConfirmedPolicies){
		
		Boolean result = true;
		for(Novidea_HPC__Policy__c policy : newPolicies){
			system.debug(policy);
			if(policy.Novidea_HPC__Status__c != oldPoliciesMap.get(policy.Id).Novidea_HPC__Status__c && policy.Novidea_HPC__Status__c == 'Placement confirmed'){
				placementConfirmedPolicies.add(policy.Id);
			}
		}
		if(!placementConfirmedPolicies.isEmpty()){
			List<AggregateResult> aggResults = [
				SELECT COUNT(Id) idsCount, Novidea_HPC__Policy__c
				FROM Novidea_HPC__Policy_Breakdown__c
				WHERE Id IN :placementConfirmedPolicies AND Lead_Insurer__c = true
				GROUP BY Novidea_HPC__Policy__c
			];
			for(AggregateResult aggResult : aggResults){
				if((Integer)aggResult.get('idsCount') > 1){
					result = false;
					newPoliciesMap.get((Id)aggResult.get('Novidea_HPC__Policy__c')).Validation__c = 'Lead Insurer';
					break;
				}
			}
		}
		return result;
	}

	private static Boolean validateQuotation(List<Novidea_HPC__Policy__c> newPolicies){
		List<Id> applicationsIds = new List<Id>();
		Boolean result = true;
		for(Novidea_HPC__Policy__c policy : newPolicies){
			if(policy.Novidea_HPC__Application__c != null){
				applicationsIds.add(policy.Novidea_HPC__Application__c);
			}
		}
		if(!applicationsIds.isEmpty()){
			Map<Id, Novidea_HPC__Application__c> applicationsMap = new Map<Id, Novidea_HPC__Application__c>([
				SELECT Novidea_HPC__Account__r.Novidea_HPC__Status__c
				FROM Novidea_HPC__Application__c
				WHERE Novidea_HPC__Account__r.Novidea_HPC__Status__c = 'Approved' 
					AND Id IN :applicationsIds
			]);
			for(Novidea_HPC__Policy__c policy : newPolicies){
				if(!applicationsMap.containsKey(policy.Novidea_HPC__Application__c)){
					policy.Validation__c = 'Client Unapproved';
					result = false;
				}
			}
		}
		return result;
	}

	public static Map<Id, Account> assignClientCedantCode(List<Novidea_HPC__Policy__c> newPolicies, Map<Id, Novidea_HPC__Policy__c> newPoliciesMap){
			Map<Id, Account> policyIdToAccountMap = new Map<Id, Account>();
			Map<Id, List<Novidea_HPC__Policy__c>> accountIdToPoliciesMap = new Map<Id, List<Novidea_HPC__Policy__c>>();
			for(Novidea_HPC__Policy__c policy : newPolicies){
				if(policy.Client_Cedant_Code__c == null && policy.Novidea_HPC__Client__c != null){
					if(accountIdToPoliciesMap.containsKey(policy.Novidea_HPC__Client__c)){
						accountIdToPoliciesMap.get(policy.Novidea_HPC__Client__c).add(policy);
					}else{
						accountIdToPoliciesMap.put(policy.Novidea_HPC__Client__c, new List<Novidea_HPC__Policy__c>{policy});
					}
				}
			}
			if(!accountIdToPoliciesMap.isEmpty()){
				Map<Id, Account> accountsMap = new Map<Id, Account>([
					SELECT Client_Code__c, Location_Type__c
					FROM Account
					WHERE Id IN :accountIdToPoliciesMap.keySet()
				]);
				for(List<Novidea_HPC__Policy__c> policies : accountIdToPoliciesMap.values()){
					for(Novidea_HPC__Policy__c policy : policies){
						policy.Client_Cedant_Code__c = accountsMap.get(policy.Novidea_HPC__Client__c).Client_Code__c;
					}
				}
			}
	   // }
	   return null;
	}

	public static Boolean validateMASApproval(List<Novidea_HPC__Policy__c> newPolicies, Map<Id, Novidea_HPC__Policy__c> newPoliciesMap, Map<Id, Account> policyIdToClientMap, Map<Id, List<Novidea_HPC__Policy_Breakdown__c>> policyIdToPolicyBreakdownsMap, Boolean isNew, Map<Id, Novidea_HPC__Policy__c> updatedPoliciesMap){
		Boolean result = true;
		Map<Id, Novidea_HPC__Policy__c> uncheckedMASPolicyMap = new Map<Id, Novidea_HPC__Policy__c>();
		Map<Id, Novidea_HPC__Policy__c> checkedMASPolicyMap = new Map<Id, Novidea_HPC__Policy__c>();
		Map<Id, Account> policyIdToAccountMap = new Map<Id, Account>();
		Map<Id, Id> policyIdsToAccountIdsMap = new Map<Id, Id>();
		Set<Id> carrierIds = new Set<Id>();
		Map<Id, Account> accountsMap;
		for(Novidea_HPC__Policy__c policy : newPolicies){
			System.debug(policy.Client_Cedant_Code__c == null);
			System.debug(policy.Novidea_HPC__Client__c != null);
			if(policy.Client_Cedant_Code__c == null && policy.Novidea_HPC__Client__c != null){
				policyIdsToAccountIdsMap.put(policy.Id ,policy.Novidea_HPC__Client__c);
			}
			if(isNew){
				if(policy.Novidea_HPC__Carrier__c != null){
					carrierIds.add(policy.Novidea_HPC__Carrier__c);
				}
			}
		}
		if(!policyIdsToAccountIdsMap.isEmpty() || !carrierIds.isEmpty()){
			Set<Id> accountUniqueIds = new Set<Id>(policyIdsToAccountIdsMap.values());
			List<Id> accountIds = new List<Id>(accountUniqueIds);
			accountsMap = new Map<Id, Account>([
				SELECT Client_Code__c, Location_Type__c, Novidea_HPC__Status__c, Offshore__c
				FROM Account
				WHERE Id IN :accountIds OR Id IN :carrierIds
			]);
			for(Id policyId : policyIdsToAccountIdsMap.keySet()){
				policyIdToAccountMap.put(policyId, accountsMap.get(policyIdsToAccountIdsMap.get(policyId)));
			}
		}
		if(isNew){
			/*for(Novidea_HPC__Policy__c policy : newPolicies){
				system.debug(accountsMap.get(policy.Novidea_HPC__Carrier__c));
				if(policy.Novidea_HPC__Carrier__c != null && (accountsMap.get(policy.Novidea_HPC__Carrier__c).Novidea_HPC__Status__c == 'Unapproved' || ((policy.Risk_Location__c == 'Domestic' || (policy.Novidea_HPC__Client__c != null && accountsMap.get(policy.Novidea_HPC__Client__c).Location_Type__c == 'Domestic')) && accountsMap.get(policy.Novidea_HPC__Carrier__c).Offshore__c == 'Offshore'))){
					policy.MAS_Approved_Required__c = true;
					result = false;
				}
			}*/
		}else{
			List<Novidea_HPC__Policy_Breakdown__c> policyBreakdowns = [
				SELECT Novidea_HPC__Policy__c, Novidea_HPC__Carrier__r.Novidea_HPC__Status__c, Novidea_HPC__Carrier__r.Offshore__c
				FROM Novidea_HPC__Policy_Breakdown__c
				WHERE Novidea_HPC__Policy__c IN :newPolicies
			];
			for(Novidea_HPC__Policy_Breakdown__c policyBreakdown : policyBreakdowns){
				if(policyIdToPolicyBreakdownsMap.containsKey(policyBreakdown.Novidea_HPC__Policy__c)){
					policyIdToPolicyBreakdownsMap.get(policyBreakdown.Novidea_HPC__Policy__c).add(policyBreakdown);
				}else{
					policyIdToPolicyBreakdownsMap.put(policyBreakdown.Novidea_HPC__Policy__c, new List<Novidea_HPC__Policy_Breakdown__c>{policyBreakdown});
				}
				if(policyBreakdown.Novidea_HPC__Carrier__c != null && (policyBreakdown.Novidea_HPC__Carrier__r.Novidea_HPC__Status__c == 'Unapproved' || ((newPoliciesMap.get(policyBreakdown.Novidea_HPC__Policy__c).Risk_Location__c == 'Domestic' || (policyIdToAccountMap.containsKey(policyBreakdown.Novidea_HPC__Policy__c) && policyIdToAccountMap.get(policyBreakdown.Novidea_HPC__Policy__c).Location_Type__c == 'Domestic')) && policyBreakdown.Novidea_HPC__Carrier__r.Offshore__c == 'Offshore'))){
					if(newPoliciesMap.get(policyBreakdown.Novidea_HPC__Policy__c).MAS_Approved_Required__c != true){
						newPoliciesMap.get(policyBreakdown.Novidea_HPC__Policy__c).MAS_Approved_Required__c = true;
						result = false;
						updatedPoliciesMap.put(policyBreakdown.Novidea_HPC__Policy__c, newPoliciesMap.get(policyBreakdown.Novidea_HPC__Policy__c));
						if(uncheckedMASPolicyMap.containsKey(policyBreakdown.Novidea_HPC__Policy__c)){
							uncheckedMASPolicyMap.remove(policyBreakdown.Novidea_HPC__Policy__c);
						}
					}
					checkedMASPolicyMap.put(policyBreakdown.Novidea_HPC__Policy__c, newPoliciesMap.get(policyBreakdown.Novidea_HPC__Policy__c));
				}else if(newPoliciesMap.get(policyBreakdown.Novidea_HPC__Policy__c).MAS_Approved_Required__c == true && !checkedMASPolicyMap.containsKey(policyBreakdown.Novidea_HPC__Policy__c)){
					uncheckedMASPolicyMap.put(policyBreakdown.Novidea_HPC__Policy__c, newPoliciesMap.get(policyBreakdown.Novidea_HPC__Policy__c));
					newPoliciesMap.get(policyBreakdown.Novidea_HPC__Policy__c).MAS_Approved_Required__c = false;
					updatedPoliciesMap.put(policyBreakdown.Novidea_HPC__Policy__c, newPoliciesMap.get(policyBreakdown.Novidea_HPC__Policy__c));
					result = false;
				}
			}

		}
		return result;
	}

	private static void setIaLevyTaxAndCap(List<Novidea_HPC__Policy__c> newPolicies, Map<Id, Novidea_HPC__Policy__c> oldPoliciesMap) {
		List<Tax_Parameters__c> taxParameters = new List<Tax_Parameters__c>();
		Set<Id> legalEntities = new Set<Id>();
		Set<Id> prodoctDefIds = new Set<Id>();
		List<Date> policyStartDates = new List<Date>();
		List<Novidea_HPC__Policy__c> relevantPolicies = new List<Novidea_HPC__Policy__c>();
		Map<Id, Tax_Parameters__c> policyIATaxParameterMap = new Map<Id, Tax_Parameters__c>();
		Map<Id, Tax_Parameters__c> policyMIBTaxParameterMap = new Map<Id, Tax_Parameters__c>();
		Map<Id, Tax_Parameters__c> policyECITaxParameterMap = new Map<Id, Tax_Parameters__c>();

		for(Novidea_HPC__Policy__c policy : newPolicies) {
			if(oldPoliciesMap != null){
				if((policy.Novidea_HPC__Status__c != oldPoliciesMap.get(policy.Id).Novidea_HPC__Status__c || 
				policy.Novidea_HPC__Product_Definition__c != oldPoliciesMap.get(policy.Id).Novidea_HPC__Product_Definition__c || 
				policy.IA_Levy_Exemption__c != oldPoliciesMap.get(policy.Id).IA_Levy_Exemption__c) &&
				policy.Legal_Entity__c != null && policy.Novidea_HPC__Effective_Date__c != null && policy.Novidea_HPC__Product_Definition__c != null) {
				  legalEntities.add(policy.Legal_Entity__c);
				  policyStartDates.add(policy.Novidea_HPC__Effective_Date__c);
				  relevantPolicies.add(policy);
				  prodoctDefIds.add(policy.Novidea_HPC__Product_Definition__c);
			  }
			}else{
				if(policy.Legal_Entity__c != null && policy.Novidea_HPC__Effective_Date__c != null && policy.Novidea_HPC__Product_Definition__c != null) {
					legalEntities.add(policy.Legal_Entity__c);
					policyStartDates.add(policy.Novidea_HPC__Effective_Date__c);
					relevantPolicies.add(policy);
					prodoctDefIds.add(policy.Novidea_HPC__Product_Definition__c);
				}
			}
		}

		if(!legalEntities.isEmpty() && !policyStartDates.isEmpty()) {
			policyStartDates.sort();
			Date minDate = policyStartDates.get(0);
			Date maxDate = policyStartDates.get(policyStartDates.size()-1);
			taxParameters = [SELECT Tax_Type__c, Legal_Entity__c, Life_Cap__c, Non_Life_Cap__c, Percentage__c, From_Effective_Date__c, To_Effective_Date__c 
			FROM Tax_Parameters__c 
			WHERE Legal_Entity__c IN :legalEntities AND From_Effective_Date__c <= :maxDate AND To_Effective_Date__c > :minDate];
		}
		if(!taxParameters.isEmpty()) {
			Map<Id, Novidea_HPC__Product_Def__c> prodDefsMap = new Map<Id, Novidea_HPC__Product_Def__c>([
				SELECT Id, Life_Non_Life_Indicator__c, Applicable_for_MIB__c, Applicable_for_ECI_Levy__c
				FROM Novidea_HPC__Product_Def__c 
				WHERE Id in :prodoctDefIds
				]);			
			for(Novidea_HPC__Policy__c policy : relevantPolicies) {
				if(policy.IA_Levy_Exemption__c){
					policy.IA_Levy_Percentage__c = 0;
					policy.IA_Levy_Cap__c = 0;
				}
				if(!prodDefsMap.get(policy.Novidea_HPC__Product_Definition__c).Applicable_for_MIB__c){
					policy.MIB_Percentage__c = 0;
				}

				if(!prodDefsMap.get(policy.Novidea_HPC__Product_Definition__c).Applicable_for_ECI_Levy__c){
					policy.ECI_Levy_Percentage__c = 0;
				}

				//Store relevant tax params for each type
				for(Tax_Parameters__c taxParam : taxParameters) {
					if(taxParam.Tax_Type__c == 'IA Levy' && taxParam.Legal_Entity__c == policy.Legal_Entity__c && taxParam.From_Effective_Date__c <= policy.Novidea_HPC__Effective_Date__c && taxParam.To_Effective_Date__c > policy.Novidea_HPC__Effective_Date__c) {
						if(!policyIATaxParameterMap.containsKey(policy.Id)) {
							policyIATaxParameterMap.put(policy.Id, taxParam);
						}
					}else if(taxParam.Tax_Type__c == 'MIB' && taxParam.Legal_Entity__c == policy.Legal_Entity__c && taxParam.From_Effective_Date__c <= policy.Novidea_HPC__Effective_Date__c && taxParam.To_Effective_Date__c > policy.Novidea_HPC__Effective_Date__c) {
						if(!policyMIBTaxParameterMap.containsKey(policy.Id)) {
							policyMIBTaxParameterMap.put(policy.Id, taxParam);
						}
					}else if(taxParam.Tax_Type__c == 'ECI Levy' && taxParam.Legal_Entity__c == policy.Legal_Entity__c && taxParam.From_Effective_Date__c <= policy.Novidea_HPC__Effective_Date__c && taxParam.To_Effective_Date__c > policy.Novidea_HPC__Effective_Date__c) {
						if(!policyECITaxParameterMap.containsKey(policy.Id)) {
							policyECITaxParameterMap.put(policy.Id, taxParam);
						}
					}
				}
				//Populate IA Levy
				if(policyIATaxParameterMap.containsKey(policy.Id) && !policy.IA_Levy_Exemption__c) {
					Tax_Parameters__c param = policyIATaxParameterMap.get(policy.Id);
					policy.IA_Levy_Percentage__c = param.Percentage__c;
					if(prodDefsMap.containsKey(policy.Novidea_HPC__Product_Definition__c) && prodDefsMap.get(policy.Novidea_HPC__Product_Definition__c).Life_Non_Life_Indicator__c == 'Life') {
						policy.IA_Levy_Cap__c = param.Life_Cap__c;
					}
					if(prodDefsMap.containsKey(policy.Novidea_HPC__Product_Definition__c) && prodDefsMap.get(policy.Novidea_HPC__Product_Definition__c).Life_Non_Life_Indicator__c == 'Non-Life') {
						policy.IA_Levy_Cap__c = param.Non_Life_Cap__c;
					}
				}
				//Populate MIB
				if(policyMIBTaxParameterMap.containsKey(policy.Id) && prodDefsMap.get(policy.Novidea_HPC__Product_Definition__c).Applicable_for_MIB__c) {
					Tax_Parameters__c param = policyMIBTaxParameterMap.get(policy.Id);
					policy.MIB_Percentage__c = param.Percentage__c;
				}
				//Populate ECI Levy
				if(policyECITaxParameterMap.containsKey(policy.Id) && prodDefsMap.get(policy.Novidea_HPC__Product_Definition__c).Applicable_for_ECI_Levy__c) {
					Tax_Parameters__c param = policyECITaxParameterMap.get(policy.Id);
					policy.ECI_Levy_Percentage__c = param.Percentage__c;
				}
			}
		}else{
			if(!relevantPolicies.isEmpty()) {
				throw new NOVU.NovideaExceptions.ConfigurationException(System.Label.Missing_Tax_Configuration);
			}
		}
	}
}