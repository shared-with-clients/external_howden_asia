public class GenerateDocumentController {
    public Novidea_HPC__Proposal__c currentProposal {get; set;}
    private Id recordId = null;
    
	public GenerateDocumentController(ApexPages.StandardController controller) {
        recordId = controller.getRecord().Id;
        currentProposal = [SELECT Document_all_fields_fulfilled__c FROM Novidea_HPC__Proposal__c WHERE Id =:recordId ];
    }
    
	public PageReference init() {
        if(currentProposal.Document_all_fields_fulfilled__c == false){
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.Perform_Error));
            return null;
        }
        else {
            return new PageReference('/apex/NPERFORM__generatePdf?Id=' + recordId);
        }
    }
    
    public PageReference back(){ 
        return new PageReference('/'+ recordId);
    }	
}