@isTest
public with sharing class TransactionCalculationsTest {
    @testSetup
	static void init(){
        Map<String,Id> accountRtIds = NOVU.RecordTypeUtils.getIdsByDevNameAndFullSObjType(new String[]{'Carrier','Business'},'Account');
        Account carrier1 = new Account(Name = 'Carrier1', Account_Full_Name__c = 'Carrier1', NIBA__Accounting_Code__c = '1', NIBA__Accounting_Status__c = 'Open', NIBA__Has_Financial_Implications__c=true,
        RecordTypeId = accountRtIds.get('Carrier'), NIBA__Re_Insurer_type__c = 'Non-Bureau Company', BillingStreet = 'Shalom', Offshore__c= 'Offshore');   
        Account client1 = new Account(Name = 'Client1', Account_Full_Name__c = 'Client1', NIBA__Accounting_Code__c = '3', NIBA__Accounting_Status__c = 'Open', NIBA__Has_Financial_Implications__c=true,
        RecordTypeId = accountRtIds.get('Business'), NIBA__Re_Insurer_type__c = 'Non-Bureau Company', BillingStreet = 'Shalom');
        Database.insert(new List<Account>{carrier1, client1});       
       
        PolicyTriggerHelper.stopTrigger = true;
        
        Novidea_HPC__Product_Def__c prodacDef1 = new Novidea_HPC__Product_Def__c(
            Name = 'productDef1', Code__c ='123',
            GST_Applicable_On_Premium__c = 'Yes',
            Life_Non_Life_Indicator__c = 'Life',
            PPW_Applicable__c ='Yes',
            GST_Applicable_On_Brokerage__c = 'Standard Rated',
            Novidea_HPC__DeveloperName__c = 'productDef11', 
            Novidea_HPC__Bureau_Code__c = 'test1');
        Database.insert(new Novidea_HPC__Product_Def__c[]{prodacDef1});

        Novidea_HPC__Policy__c policy1 = new Novidea_HPC__Policy__c(Novidea_HPC__Carrier__c = carrier1.Id, Novidea_HPC__Client__c = client1.Id,
			Novidea_HPC__Effective_Date__c = Date.today(), Novidea_HPC__Expiration_Date__c = Date.today().addYears(1).addDays(-1),
            Novidea_HPC__Policy_Number__c = 'PM12', Novidea_HPC__Product_Definition__c = prodacDef1.Id);
        Database.insert(new List<Novidea_HPC__Policy__c>{policy1});

        //PolicyTriggerHelper.stopTrigger = false;
        Novidea_HPC__Policy_Breakdown__c breakdown1 = new Novidea_HPC__Policy_Breakdown__c(Name='Brd1',
            Novidea_HPC__Carrier__c = carrier1.id,
            Novidea_HPC__Policy__c = policy1.id,
            Novidea_HPC__Agent_Discount_Percentage__c = 5,
            Novidea_HPC__Commission_Breakdown__c = 20, Novidea_HPC__Credit_Fee_Breakdown__c = 10, Novidea_HPC__Fees_Breakdown__c = 15,
            Novidea_HPC__Premium_Breakdown__c = 25, Novidea_HPC__Premium_Currency__c = 'USD',
            Novidea_HPC__Settlement_Currency__c = 'USD', Novidea_HPC__Value_Date__c = Date.today(), Novidea_HPC__Written_Line_Percentage__c = 32,
            Novidea_HPC__Status__c = 'Active', Novidea_HPC__Sub_Status__c = 'Initial',
            Novidea_HPC__Line_Type__c = 'Line to stand', Novidea_HPC__Market__c = 'IUA', Novidea_HPC__Slip_Role__c = 'Lead', Novidea_HPC__Carriers_Ref__c = 'test', 
            Novidea_HPC__Claims_Agreement_New__c	= 'No',
            Novidea_HPC__Signed_Line__c = 35, Lead_Insurer__c = true);
        Database.insert(new Novidea_HPC__Policy_Breakdown__c[]{breakdown1});

        //client income
        Id regularRecordTypeId = NOVU.RecordTypeUtils.getIdByDevNameAndFullSObjType('regular','Novidea_HPC__Income__c');
        Novidea_HPC__Income__c income1 = new Novidea_HPC__Income__c(
            RecordTypeId = regularRecordTypeId,
            Novidea_HPC__Policy__c = policy1.Id,
            Novidea_HPC__Effective_Date__c = Date.today(),
            Novidea_HPC__Expiration_Date__c = Date.today().addYears(1).addDays(-1),
            Novidea_HPC__Commission_Amount__c = 100,
            Novidea_HPC__Commission_Amount_Currency__c = 'GBP',
            Novidea_HPC__Premium__c = 1000,  
            Novidea_HPC__Premium_Currency__c = 'GBP',
            Novidea_HPC__Number_of_Payments__c = 1, 
            Novidea_HPC__Underwriter_Commission__c = 15,
            Novidea_HPC__Client__c = client1.Id,
            Novidea_HPC__Agent_Discount__c = 10,
            Novidea_HPC__Agency_Fee__c = 5,
            IA_Levy_Amount__c = 5,
            MIB_Amount__c = 4,
            ECI_Levy_Amount__c = 3,
            Novidea_HPC__Original_Settlement_Rate__c = 1, 
            Novidea_HPC__Settlement_Currency__c = 'GBP',
            Novidea_HPC__Commission_Percentage__c = 98,
            HPC_Collection__Period_Between_Payments__c = 12, 
            Commission_GST__c = 98,
            Premium_before_GST__c= 100,
            GST_Amount_on_Premium__c= 200,
            Payable_to_Insurer__c = 56,
            Premium_To_Pay_With_Credit_Fee__c = 56,
            Net_Brokerage__c = 45,
            Service_Fee__c = 56, Service_Fee_GST__c = 25,
            Stamp_Duty__c=85, Policy_Charges__c =58, 
            Others_Taxes__c =10, Other_Charges__c=12,
            Novidea_HPC__Tax_on_Net_Premium__c = 6, 
            Premium_to_Pay__c = 25,
            Commission_Discount__c = 23, 
            Commission_discount_rate__c =2);
        Database.insert(new Novidea_HPC__Income__c[]{income1});

        //Underwriter income
        Id uwrRegularRecordTypeId = NOVU.RecordTypeUtils.getIdByDevNameAndFullSObjType('uwr_regular','Novidea_HPC__Income__c');
        Novidea_HPC__Income__c incomeUnderwriter1 = new Novidea_HPC__Income__c(
            RecordTypeId = uwrRegularRecordTypeId,
            Novidea_HPC__Policy__c = policy1.Id,
            Novidea_HPC__Effective_Date__c = Date.today(),
            Novidea_HPC__Expiration_Date__c = Date.today().addYears(1).addDays(-1),
            Novidea_HPC__Commission_Amount__c = 100,
            Novidea_HPC__Commission_Amount_Currency__c = 'GBP',
            Novidea_HPC__Premium__c = 1000,
            Novidea_HPC__Premium_Currency__c = 'GBP',
            Novidea_HPC__Number_of_Payments__c = 1,
            HPC_Collection__Period_Between_Payments__c = 12,
            Novidea_HPC__Underwriter_Commission__c = 15,
            Novidea_HPC__Carrier__c = carrier1.Id,
            Novidea_HPC__Client_Income__c = income1.Id,
            Novidea_HPC__Agent_Discount__c = 10,
            Novidea_HPC__Agency_Fee__c = 5,
            Novidea_HPC__Original_Settlement_Rate__c = 1,
            Novidea_HPC__Settlement_Currency__c = 'GBP',
            Novidea_HPC__Tax_on_Net_Premium__c = 2,
            Novidea_HPC__Commission_Percentage__c = 98,
            Commission_discount_rate__c = 0.568,
            Commission_Discount__c = 100,
            Commission_GST__c = 300,
            IA_Levy_Amount__c = 5,
            MIB_Amount__c = 4,
            ECI_Levy_Amount__c = 3,
            Premium_To_Pay_With_Credit_Fee__c = 56,
            ///Total_Other_Taxes_and_Charges__c = 40, // not writeable
            Novidea_HPC__Policy_Breakdown__c = breakdown1.Id);

        Database.insert(new Novidea_HPC__Income__c[]{incomeUnderwriter1});

        List<HPC_Collection__Receipt__c> underwriterReceipts = [
            SELECT Id
            FROM HPC_Collection__Receipt__c
            WHERE HPC_Collection__Income__c = :incomeUnderwriter1.Id
        ];

        List<HPC_Collection__Receipt__c> clientReceipts = [
            SELECT Id
            FROM HPC_Collection__Receipt__c
            WHERE HPC_Collection__Income__c = :income1.Id
        ];

        for(HPC_Collection__Receipt__c receipt : underwriterReceipts){
            receipt.HPC_Collection__Client_Receipt__c = clientReceipts[0].Id;
            receipt.HPC_Collection__Net_Premium__c = 35;
            receipt.HPC_Collection__Commission_Amount__c = 1;
        }
        Database.update(underwriterReceipts);
    }

    private static Map<Id, HPC_Collection__Receipt__c> getIdsToReceiptstMap(Set<Id> incomeIds){
        List<HPC_Collection__Receipt__c> receipts = [
            SELECT 
                Name, 
                HPC_Collection__Client_Receipt__c, 
                HPC_Collection__Expected_Premium_to_Pay__c,
                HPC_Collection__Income__c,
                HPC_Collection__Commission_Amount__c,
                HPC_Collection__Net_Premium__c,
                HPC_Collection__Total_Commission__c,
                Service_Fee__c, 
                Service_Fee_GST__c, 
                Stamp_Duty__c, 
                Policy_Charges__c, 
                Other_Taxes__c, 
                Other_Charges__c,
                Client_Payable__c,
                Net_Brokerage__c,
                GST_Amount_on_Premium__c,
                Payable_to_Insurer__c,
                Commission_Amount_Discount__c,
                Commission_GST__c,
                IA_Levy_Amount__c,
                MIB_Amount__c,
                ECI_Levy_Amount__c,
                MIB__c,
                ECI_Levy__c,
                HPC_Collection__Income__r.Novidea_HPC__Client_Income__c,
                HPC_Collection__Income__r.Commission_GST__c, 
                HPC_Collection__Income__r.Novidea_HPC__Tax_on_Net_Premium__c, 
                HPC_Collection__Income__r.Total_Other_Taxes_and_Charges__c,
                HPC_Collection__Income__r.Commission_Discount__c, 
                HPC_Collection__Income__r.Novidea_HPC__Commission_Percentage__c,
                HPC_Collection__Income__r.Commission_discount_rate__c, 
                HPC_Collection__Income__r.Novidea_HPC__Policy_Breakdown__r.Novidea_HPC__Signed_Line__c
            FROM HPC_Collection__Receipt__c
            WHERE HPC_Collection__Income__c IN :incomeIds
        ];
        return new  Map<Id, HPC_Collection__Receipt__c>(receipts);
    }

    private static Map<Id, Novidea_HPC__Income__c> getClientIncomeIdToIncomeMap(){
        List<Novidea_HPC__Income__c> incomes = [
            SELECT 
                Name, 
                Commission_GST__c,
                Premium_before_GST__c,
                GST_Amount_on_Premium__c,
                Payable_to_Insurer__c,
                Premium_To_Pay_With_Credit_Fee__c,
                Net_Brokerage__c,
                Service_Fee__c,
                Service_Fee_GST__c,
                Stamp_Duty__c,
                Policy_Charges__c,
                Others_Taxes__c,
                Other_Charges__c,
                Premium_to_Pay__c,
                Commission_Discount__c,
                Commission_discount_rate__c,
                Novidea_HPC__Client__c,
                Novidea_HPC__Premium__c,
                Novidea_HPC__Commission_Amount__c,
                Novidea_HPC__Tax_on_Net_Premium__c, 
                Novidea_HPC__Commission_Percentage__c,
                Novidea_HPC__Client__r.Name,
                Novidea_HPC__Client__r.NIBA__Has_Financial_Implications__c,
                Novidea_HPC__Client__r.NIBA__Accounting_Status__c,
                Novidea_HPC__Policy_Breakdown__r.Novidea_HPC__Signed_Line__c,
                Total_Commission__c,
                IA_Levy_Amount__c,
                MIB_Amount__c,
                ECI_Levy_Amount__c,
                MIB__c,
                ECI_Levy__c,
                Novidea_HPC__Policy__r.RecordType.DeveloperName,
                Novidea_HPC__Policy__r.Policy_Aggregated_IA_Levy_Amount__c,
                Novidea_HPC__Policy__r.IA_Levy_Cap__c,
                (SELECT Id, GST_Amount_on_Premium__c, HPC_Collection__Net_Premium__c, HPC_Collection__Commission_Amount__c, HPC_Collection__Income__c,
                        Payable_to_Insurer__c, HPC_Collection__Expected_Premium_to_Pay__c, Commission_Amount_Discount__c, Net_Brokerage__c, Client_Payable__c, 
                        Commission_GST__c, Service_Fee__c, Service_Fee_GST__c, Stamp_Duty__c, Policy_Charges__c, Other_Taxes__c, Other_Charges__c,
                        HPC_Collection__Income__r.Commission_GST__c, IA_Levy_Amount__c, MIB_Amount__c, ECI_Levy_Amount__c
                        FROM HPC_Collection__Receipts__r)
                FROM Novidea_HPC__Income__c
                WHERE Novidea_HPC__Client__r.name = 'Client1'
        ];
        return new Map<Id, Novidea_HPC__Income__c>(incomes);
    }

    private static Map<Id, Novidea_HPC__Income__c> getUnderwriterIncomeIdToIncomeMap(){
        List<Novidea_HPC__Income__c> incomes = [
            SELECT 
            Name,
            Brokerage_Account__c,
            Tax_Account__c,
            Tax_Account_Definition_Status__c,
            Commission_GST__c,
            Total_Other_Taxes_and_Charges__c, 
            Commission_Discount__c,
            Commission_discount_rate__c,
            Premium_to_Pay__c,
            Premium_before_GST__c, 
            GST_Amount_on_Premium__c,
            Novidea_HPC__Premium__c,
            Payable_to_Insurer__c, 
            Premium_To_Pay_With_Credit_Fee__c, 
            Net_Brokerage__c, 
            Service_Fee__c,
            Service_Fee_GST__c,
            Stamp_Duty__c, 
            Policy_Charges__c, 
            Others_Taxes__c,
            Other_Charges__c,
            IA_Levy_Amount__c,
            MIB_Amount__c,
            ECI_Levy_Amount__c,
            MIB__c,
            ECI_Levy__c,
            Novidea_HPC__Commission_Amount__c,
            Novidea_HPC__Client_Income__c,
            Novidea_HPC__Tax_on_Net_Premium__c, 
            Novidea_HPC__Commission_Percentage__c,
            Novidea_HPC__Carrier__c,
            Novidea_HPC__Carrier__r.Name,
            Novidea_HPC__Carrier__r.NIBA__Has_Financial_Implications__c,
            Novidea_HPC__Carrier__r.NIBA__Accounting_Status__c,
            Novidea_HPC__Policy_Breakdown__r.Novidea_HPC__Signed_Line__c,
            Total_Commission__c,
            Novidea_HPC__Policy__r.Novidea_HPC__Apportionment__c,
            Novidea_HPC__Policy__r.Policy_Aggregated_IA_Levy_Amount__c,
            Novidea_HPC__Policy__r.IA_Levy_Cap__c,
            Legal_Entity__c,
            Legal_Entity__r.Unit_4_Code__c,
            (SELECT Id, HPC_Collection__Client_Receipt__c, HPC_Collection__Expected_Premium_to_Pay__c, Service_Fee__c, HPC_Collection__Income__c, HPC_Collection__Commission_Amount__c,GST_Amount_on_Premium__c, Payable_to_Insurer__c, HPC_Collection__Total_Commission__c, 
            HPC_Collection__Income__r.Novidea_HPC__Number_of_Payments__c, 
            Service_Fee_GST__c, Stamp_Duty__c, Policy_Charges__c, Other_Taxes__c, Other_Charges__c, 
            HPC_Collection__Income__r.Novidea_HPC__Client_Income__c, Client_Payable__c,
            Commission_Amount_Discount__c, Net_Brokerage__c, HPC_Collection__Net_Premium__c, Commission_GST__c, 
            HPC_Collection__Income__r.Commission_GST__c, HPC_Collection__Income__r.Novidea_HPC__Tax_on_Net_Premium__c, HPC_Collection__Income__r.Total_Other_Taxes_and_Charges__c, HPC_Collection__Income__r.Premium_To_Pay_With_Credit_Fee__c,
            HPC_Collection__Income__r.Commission_Discount__c, HPC_Collection__Income__r.Novidea_HPC__Commission_Percentage__c,
            HPC_Collection__Income__r.Commission_discount_rate__c, IA_Levy_Amount__c, MIB_Amount__c, ECI_Levy_Amount__c, 
            HPC_Collection__Income__r.Novidea_HPC__Policy_Breakdown__r.Novidea_HPC__Signed_Line__c
            FROM HPC_Collection__Receipts__r)
            FROM Novidea_HPC__Income__c
            WHERE Novidea_HPC__Carrier__r.name = 'Carrier1'
        ];
        return new Map<Id, Novidea_HPC__Income__c>(incomes);
    }
   
    @isTest
    public static void recalculateUnderwriterInstallmentsTest(){
        List<Novidea_HPC__Income__c> incomes = [SELECT Id FROM Novidea_HPC__Income__c WHERE Novidea_HPC__Main_Income__c = null AND Novidea_HPC__Carrier__r.name = 'Carrier1'];
        Set<Id> incomeIds = (new Map<Id,Novidea_HPC__Income__c>(incomes)).keySet();
        Map<Id, HPC_Collection__Receipt__c> idReceiptToReceiptMap = TransactionCalculationsTest.getIdsToReceiptstMap(incomeIds);
        Set<Id> modifiedReceiptIdSet = TransactionCalculations.recalculateUnderwriterInstallments(idReceiptToReceiptMap);
        System.assertEquals(1, modifiedReceiptIdSet.size());
    }

    @isTest
    public static void recalculateUnderwriterTransactionsTest(){
        Map<Id, Novidea_HPC__Income__c> underwriterIncomeIdToIncomeMap = TransactionCalculationsTest.getUnderwriterIncomeIdToIncomeMap();
        Map<Id, List<HPC_Collection__Receipt__c>> incomeIdToReceiptsMap = new Map<Id, List<HPC_Collection__Receipt__c>>();
        for(Novidea_HPC__Income__c income : underwriterIncomeIdToIncomeMap.values()){
            incomeIdToReceiptsMap.put(income.Id, income.HPC_Collection__Receipts__r);
        }
        Set<Id> modifiedIncomeIdSet = TransactionCalculations.recalculateUnderwriterTransactions(underwriterIncomeIdToIncomeMap, incomeIdToReceiptsMap);
        System.assertEquals(1, modifiedIncomeIdSet.size());
    }

    @isTest
    public static void recalculateClientInstallmentsTest(){
        List<Novidea_HPC__Income__c> incomes = [SELECT Id FROM Novidea_HPC__Income__c WHERE Novidea_HPC__Client__r.name = 'Client1'];
        Set<Id> incomeIds = (new Map<Id,Novidea_HPC__Income__c>(incomes)).keySet();
        Map<Id, HPC_Collection__Receipt__c> idToClientReceiptMap = TransactionCalculationsTest.getIdsToReceiptstMap(incomeIds);
        Map<Id, List<HPC_Collection__Receipt__c>> clientIdToUnderwriterReceiptsMap = new Map<Id, List<HPC_Collection__Receipt__c>>();
        List<Novidea_HPC__Income__c> underwriterIncomes = [SELECT Id FROM Novidea_HPC__Income__c WHERE Novidea_HPC__Carrier__r.name = 'Carrier1'];
        incomeIds = (new Map<Id,Novidea_HPC__Income__c>(underwriterIncomes)).keySet();
        Map<Id, HPC_Collection__Receipt__c> idToUnderwriterReceiptMap = TransactionCalculationsTest.getIdsToReceiptstMap(incomeIds);
        for(HPC_Collection__Receipt__c installment : idToUnderwriterReceiptMap.values()){
            if(installment.HPC_Collection__Client_Receipt__c != null ){
                if(!clientIdToUnderwriterReceiptsMap.containsKey(installment.HPC_Collection__Client_Receipt__c)){
                    clientIdToUnderwriterReceiptsMap.put(installment.HPC_Collection__Client_Receipt__c, new List<HPC_Collection__Receipt__c>());
                }
                clientIdToUnderwriterReceiptsMap.get(installment.HPC_Collection__Client_Receipt__c).add(installment);
            }
        }
        Set<Id> modifiedIncomeIdSet = TransactionCalculations.recalculateClientInstallments(idToClientReceiptMap, clientIdToUnderwriterReceiptsMap);
        System.assertEquals(1, modifiedIncomeIdSet.size());
    }

    @isTest
    public static void recalculateClientTransactionsTest(){
        Map<Id, Novidea_HPC__Income__c> clientIncomeIdToIncomeMap = TransactionCalculationsTest.getClientIncomeIdToIncomeMap();
        Map<Id, List<HPC_Collection__Receipt__c>> incomeIdToReceiptsMap = new Map<Id, List<HPC_Collection__Receipt__c>>();
        for(Novidea_HPC__Income__c income : clientIncomeIdToIncomeMap.values()){
            incomeIdToReceiptsMap.put(income.Id, income.HPC_Collection__Receipts__r);
        }
       Set<Id> modifiedIncomeIdSet = TransactionCalculations.recalculateClientTransactions(clientIncomeIdToIncomeMap, incomeIdToReceiptsMap);
       System.assertEquals(1, modifiedIncomeIdSet.size());
    }

    @isTest
    public static void validateTransactionsTest(){
       Map<Id, Novidea_HPC__Income__c> clientIncomeIdToIncomeMap = TransactionCalculationsTest.getClientIncomeIdToIncomeMap();
	   Map<Id, Novidea_HPC__Income__c> underwriterIncomeIdToIncomeMap = TransactionCalculationsTest.getUnderwriterIncomeIdToIncomeMap();
       Map<Id, String> incomeIdToErrorMessageMap = TransactionCalculations.validateTransactions(clientIncomeIdToIncomeMap, underwriterIncomeIdToIncomeMap);
       System.assertEquals(1, incomeIdToErrorMessageMap.size()); // validateClientTransactionFields return error number not match
    }

    @isTest
    public static void testRecalculateUnderwriterInstallmentsHongKong(){
        List<Novidea_HPC__Income__c> incomes = [SELECT Id FROM Novidea_HPC__Income__c WHERE Novidea_HPC__Main_Income__c = null AND Novidea_HPC__Carrier__r.name = 'Carrier1'];
        Set<Id> incomeIds = (new Map<Id,Novidea_HPC__Income__c>(incomes)).keySet();
        Map<Id, HPC_Collection__Receipt__c> idReceiptToReceiptMap = TransactionCalculationsTest.getIdsToReceiptstMap(incomeIds);
        idReceiptToReceiptMap.values()[0].MIB__c = 10;
        idReceiptToReceiptMap.values()[0].ECI_Levy__c = 12;
        Set<Id> modifiedReceiptIdSet = TransactionCalculations.recalculateUnderwriterInstallments(idReceiptToReceiptMap);

        System.assertEquals(3.5, idReceiptToReceiptMap.values()[0].MIB_Amount__c);
        System.assertEquals(4.2, idReceiptToReceiptMap.values()[0].ECI_Levy_Amount__c);
    }

    @isTest
    public static void testRecalculateUnderwriterTransactionsHongKong(){
        Map<Id, Novidea_HPC__Income__c> underwriterIncomeIdToIncomeMap = TransactionCalculationsTest.getUnderwriterIncomeIdToIncomeMap();
        Map<Id, List<HPC_Collection__Receipt__c>> incomeIdToReceiptsMap = new Map<Id, List<HPC_Collection__Receipt__c>>();
        for(Novidea_HPC__Income__c income : underwriterIncomeIdToIncomeMap.values()){
            incomeIdToReceiptsMap.put(income.Id, income.HPC_Collection__Receipts__r);
            income.HPC_Collection__Receipts__r[0].IA_Levy_Amount__c = 5;
            income.HPC_Collection__Receipts__r[0].MIB_Amount__c= 7;
            income.HPC_Collection__Receipts__r[0].ECI_Levy_Amount__c= 8;
        }
        Set<Id> modifiedIncomeIdSet = TransactionCalculations.recalculateUnderwriterTransactions(underwriterIncomeIdToIncomeMap, incomeIdToReceiptsMap);
        System.assertEquals(5, underwriterIncomeIdToIncomeMap.values()[0].IA_Levy_Amount__c);
        System.assertEquals(7, underwriterIncomeIdToIncomeMap.values()[0].MIB_Amount__c);
        System.assertEquals(8, underwriterIncomeIdToIncomeMap.values()[0].ECI_Levy_Amount__c);
    }

    @isTest
    public static void testRecalculateClientInstallmentsHongKong(){
        List<Novidea_HPC__Income__c> incomes = [SELECT Id FROM Novidea_HPC__Income__c WHERE Novidea_HPC__Client__r.name = 'Client1'];
        Set<Id> incomeIds = (new Map<Id,Novidea_HPC__Income__c>(incomes)).keySet();
        Map<Id, HPC_Collection__Receipt__c> idToClientReceiptMap = TransactionCalculationsTest.getIdsToReceiptstMap(incomeIds);
        Map<Id, List<HPC_Collection__Receipt__c>> clientIdToUnderwriterReceiptsMap = new Map<Id, List<HPC_Collection__Receipt__c>>();
        List<Novidea_HPC__Income__c> underwriterIncomes = [SELECT Id FROM Novidea_HPC__Income__c WHERE Novidea_HPC__Carrier__r.name = 'Carrier1'];
        incomeIds = (new Map<Id,Novidea_HPC__Income__c>(underwriterIncomes)).keySet();
        Map<Id, HPC_Collection__Receipt__c> idToUnderwriterReceiptMap = TransactionCalculationsTest.getIdsToReceiptstMap(incomeIds);
        for(HPC_Collection__Receipt__c installment : idToUnderwriterReceiptMap.values()){
            if(installment.HPC_Collection__Client_Receipt__c != null ){
                if(!clientIdToUnderwriterReceiptsMap.containsKey(installment.HPC_Collection__Client_Receipt__c)){
                    clientIdToUnderwriterReceiptsMap.put(installment.HPC_Collection__Client_Receipt__c, new List<HPC_Collection__Receipt__c>());
                }
                installment.IA_Levy_Amount__c = 2;
                installment.MIB_Amount__c= 3;
                installment.ECI_Levy_Amount__c = 4;
                clientIdToUnderwriterReceiptsMap.get(installment.HPC_Collection__Client_Receipt__c).add(installment);
            }
        }
        Set<Id> modifiedReceiptIdSet = TransactionCalculations.recalculateClientInstallments(idToClientReceiptMap, clientIdToUnderwriterReceiptsMap);
        System.assertEquals(2, idToClientReceiptMap.values()[0].IA_Levy_Amount__c);
        System.assertEquals(3, idToClientReceiptMap.values()[0].MIB_Amount__c);
        System.assertEquals(4, idToClientReceiptMap.values()[0].ECI_Levy_Amount__c);
    }

    @isTest
    public static void testRecalculateClientTransactionsHongKong(){
        Map<Id, Novidea_HPC__Income__c> clientIncomeIdToIncomeMap = TransactionCalculationsTest.getClientIncomeIdToIncomeMap();
        Map<Id, List<HPC_Collection__Receipt__c>> incomeIdToReceiptsMap = new Map<Id, List<HPC_Collection__Receipt__c>>();
        for(Novidea_HPC__Income__c income : clientIncomeIdToIncomeMap.values()){
            incomeIdToReceiptsMap.put(income.Id, income.HPC_Collection__Receipts__r);
            income.HPC_Collection__Receipts__r[0].IA_Levy_Amount__c = 5;
            income.HPC_Collection__Receipts__r[0].MIB_Amount__c= 7;
            income.HPC_Collection__Receipts__r[0].ECI_Levy_Amount__c= 8;
        }
       Set<Id> modifiedIncomeIdSet = TransactionCalculations.recalculateClientTransactions(clientIncomeIdToIncomeMap, incomeIdToReceiptsMap);
       System.assertEquals(5, clientIncomeIdToIncomeMap.values()[0].IA_Levy_Amount__c);
       System.assertEquals(7, clientIncomeIdToIncomeMap.values()[0].MIB_Amount__c);
       System.assertEquals(8, clientIncomeIdToIncomeMap.values()[0].ECI_Levy_Amount__c);
    }

}