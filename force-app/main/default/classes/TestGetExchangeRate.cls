@isTest
public with sharing class TestGetExchangeRate {
   
    @isTest
	private static void testGetExchangeRate2Values(){
        NBU__Settings__c bizuSettings = new NBU__Settings__c(NBU__Base_Currency__c = 'ILS');
		Database.insert(bizuSettings);

		List<NBU__Exchange_Rate__c> xchangeRates = new List<NBU__Exchange_Rate__c>{
            new NBU__Exchange_Rate__c(NBU__Source_Currency__c = 'ILS', NBU__Target_Currency__c = 'AUD', NBU__Rate__c = 1.1,  NBU__Rate_Date__c = Date.today().addDays(-1)),
            new NBU__Exchange_Rate__c(NBU__Source_Currency__c = 'ILS', NBU__Target_Currency__c = 'AUD', NBU__Rate__c = 1.7,  NBU__Rate_Date__c = Date.today()),
            new NBU__Exchange_Rate__c(NBU__Source_Currency__c = 'ILS', NBU__Target_Currency__c = 'CAD', NBU__Rate__c = 1.2,  NBU__Rate_Date__c = Date.today().addDays(-1)),
            new NBU__Exchange_Rate__c(NBU__Source_Currency__c = 'ILS', NBU__Target_Currency__c = 'CAD', NBU__Rate__c = 1.8,  NBU__Rate_Date__c = Date.today())
        };
    
        Database.insert(xchangeRates);

        List<GetExchangeRate.ExchangeRateParams> exchangeRateParams = new List<GetExchangeRate.ExchangeRateParams>();
        exchangeRateParams.add(new GetExchangeRate.ExchangeRateParams('ILS','AUD',null,null));
        exchangeRateParams.add(new GetExchangeRate.ExchangeRateParams('ILS','CAD',null,null));

        List<GetExchangeRate.ExchangeRateParams> results = GetExchangeRate.getExchangeRate(exchangeRateParams);
        testResult(results[0],'ILS','AUD',1.7,Date.today());
        testResult(results[1],'ILS','CAD',1.8,Date.today());

    }

    private static void testResult(GetExchangeRate.ExchangeRateParams exchangeRateParams,
                                    String source, String target, Decimal rate, Date actualDate){

        System.assertEquals(exchangeRateParams.sourceCurrency, source);
        System.assertEquals(exchangeRateParams.targetCurrency, target);
        System.assertEquals(exchangeRateParams.rate, rate);
        System.assertEquals(exchangeRateParams.actualDate, actualDate);

    }
}
