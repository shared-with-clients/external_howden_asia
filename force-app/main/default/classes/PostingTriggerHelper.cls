public with sharing class PostingTriggerHelper {

	public static void onBeforeInsert(List<NIBA__Posting__c> newPostings){
		postingFieldsPopulation(newPostings);
		postingCashPaymentValidation(newPostings);
		populateBankAccount(newPostings);
		generateInstallmentNoteNumbers(newPostings);
	}

	private static void populateBankAccount(List<NIBA__Posting__c> newPostings){
		Map<Id, NIBA__Posting__c> ledgerEntryIdToBankPostingMap = new Map<Id, NIBA__Posting__c>();

		for(NIBA__Posting__c posting : newPostings){
			Boolean isCorrectType = (posting.NIBA__Posting_Type__c == 'Journal' || posting.NIBA__Posting_Type__c == 'Cash Receipt' || posting.NIBA__Posting_Type__c == 'Cash Payment');
			if(isCorrectType && posting.NIBA__Account_Type__c == 'Bank' && !ledgerEntryIdToBankPostingMap.containsKey(posting.NIBA__Ledger_Entry__c)){
				ledgerEntryIdToBankPostingMap.put(posting.NIBA__Ledger_Entry__c, posting);
			}
		}

		for(NIBA__Posting__c posting : newPostings){
			Boolean isCorrectType = (posting.NIBA__Posting_Type__c == 'Journal' || posting.NIBA__Posting_Type__c == 'Cash Receipt' || posting.NIBA__Posting_Type__c == 'Cash Payment');
			if(isCorrectType && posting.NIBA__Account_Type__c != 'Bank' && ledgerEntryIdToBankPostingMap.containsKey(posting.NIBA__Ledger_Entry__c)){
				posting.Bank_Account__c = ledgerEntryIdToBankPostingMap.get(posting.NIBA__Ledger_Entry__c).NIBA__Account__c;
			}
		}
	}

	private static void postingFieldsPopulation(List<NIBA__Posting__c> newPostings){
		Integer lastReceiptNumber, lastPaymentNumber, lastJournalNumber;
		Datetime created = Datetime.now();
		Map<Id, String> ledgerEntryIdToReferenceMap = new Map<Id, String>();
		Map<Id, Integer> ledgerEntryIdToReferenceNumberMap = new Map<Id, Integer>();

		for(NIBA__Posting__c posting : newPostings){
			if(!ledgerEntryIdToReferenceMap.containsKey(posting.NIBA__Ledger_Entry__c)){
				switch on posting.NIBA__Posting_Type__c  {
					when 'Cash Receipt'{
						if(lastReceiptNumber == null){
							List<AggregateResult> maxReceiptNumber = [SELECT MAX(Receipting_Number__c) maxReceiptNumber
																		FROM NIBA__Posting__c
																		WHERE Receipting_Number__c != null AND CreatedDate = LAST_N_DAYS:90];
							lastReceiptNumber = Integer.valueOf(maxReceiptNumber[0].get('maxReceiptNumber')) != null ? Integer.valueOf(maxReceiptNumber[0].get('maxReceiptNumber')) : 0;
						}
						lastReceiptNumber += 1;	
						ledgerEntryIdToReferenceNumberMap.put(posting.NIBA__Ledger_Entry__c, lastReceiptNumber);
						ledgerEntryIdToReferenceMap.put(posting.NIBA__Ledger_Entry__c, createReference('RV', created, lastReceiptNumber));
					}
					when 'Cash Payment'{
						if(lastPaymentNumber == null){
							List<AggregateResult> maxPaymentNumber = [SELECT  MAX(Payment_Number__c) maxPaymentNumber
																		FROM NIBA__Posting__c
																		WHERE Payment_Number__c != null AND CreatedDate = LAST_N_DAYS:90];
							lastPaymentNumber = Integer.valueOf(maxPaymentNumber[0].get('maxPaymentNumber')) != null ? Integer.valueOf(maxPaymentNumber[0].get('maxPaymentNumber')) : 0;
						}
						lastPaymentNumber += 1;	
						ledgerEntryIdToReferenceNumberMap.put(posting.NIBA__Ledger_Entry__c, lastPaymentNumber);
						ledgerEntryIdToReferenceMap.put(posting.NIBA__Ledger_Entry__c, createReference('PV', created, lastPaymentNumber));
					}
					when 'Journal'{
						if(lastJournalNumber == null){
							List<AggregateResult> maxJournalNumber = [SELECT MAX(Voucher_Number__c) maxVoucherNumber
																		FROM NIBA__Posting__c
																		WHERE Voucher_Number__c != null AND CreatedDate = LAST_N_DAYS:90];
							lastJournalNumber = Integer.valueOf(maxJournalNumber[0].get('maxVoucherNumber')) != null ? Integer.valueOf(maxJournalNumber[0].get('maxVoucherNumber')) : 0;
						}
						lastJournalNumber += 1;	
						ledgerEntryIdToReferenceNumberMap.put(posting.NIBA__Ledger_Entry__c, lastJournalNumber);
						ledgerEntryIdToReferenceMap.put(posting.NIBA__Ledger_Entry__c, createReference('JV', created, lastJournalNumber));

					}
				}
			}
		}

		for(NIBA__Posting__c posting : newPostings){
			if(ledgerEntryIdToReferenceMap.containsKey(posting.NIBA__Ledger_Entry__c) && ledgerEntryIdToReferenceNumberMap.containsKey(posting.NIBA__Ledger_Entry__c)){
				switch on posting.NIBA__Posting_Type__c  {
					when 'Cash Receipt'{
						posting.Receipting_Number__c = ledgerEntryIdToReferenceNumberMap.get(posting.NIBA__Ledger_Entry__c);
						posting.Receipt_Reference__c = ledgerEntryIdToReferenceMap.get(posting.NIBA__Ledger_Entry__c);
					}
					when 'Cash Payment'{
						posting.Payment_Number__c = ledgerEntryIdToReferenceNumberMap.get(posting.NIBA__Ledger_Entry__c);
						posting.Payment_Reference__c = ledgerEntryIdToReferenceMap.get(posting.NIBA__Ledger_Entry__c);
					}
					when 'Journal'{
						posting.Voucher_Number__c = ledgerEntryIdToReferenceNumberMap.get(posting.NIBA__Ledger_Entry__c);
						posting.Journal_Reference__c = ledgerEntryIdToReferenceMap.get(posting.NIBA__Ledger_Entry__c);
					}
				}
			}
		}
	}

	private static String createReference(String prefix, Datetime created, Integer lastNumber){
		return prefix + created.year() + created.month() + ('000000' + lastNumber).right(6);
	}

	private static void postingCashPaymentValidation(List<NIBA__Posting__c> newPostings){
		Set<Id> legalEntitiesIdSet = new Set<Id>();

		for(NIBA__Posting__c posting : newPostings){
			if(posting.NIBA__Legal_Entity__c != null){
				legalEntitiesIdSet.add(posting.NIBA__Legal_Entity__c);
			}
		}

		Map<Id, Account> legalEntitiesMap = new Map<Id, Account>([
			SELECT Base_Currency__c
			FROM Account
			WHERE Id IN: legalEntitiesIdSet
		]);

		Map<Integer, NIBA__Posting__c> indexToPostingMap = new Map<Integer, NIBA__Posting__c>();
		Set<Id> receiptIdSet  = new Set<Id>();

		for(Integer index = 0; index < newPostings.size(); index++){
			indexToPostingMap.put(index, newPostings[index]);
		}

		List<String> sourceCurrencyList = new List<String>();
		List<String> targetCurrencyList = new List<String>();

		for(Integer key : indexToPostingMap.keyset()){
			NIBA__Posting__c posting = indexToPostingMap.get(key);
			if(posting.NIHC__Receipt__c != null){
				receiptIdSet.add(posting.NIHC__Receipt__c);
			}

			if(legalEntitiesMap.get(posting.NIBA__Legal_Entity__c) != null && legalEntitiesMap.get(posting.NIBA__Legal_Entity__c).Base_Currency__c != null){
				sourceCurrencyList.add(legalEntitiesMap.get(posting.NIBA__Legal_Entity__c).Base_Currency__c);
			}

			targetCurrencyList.add(posting.NIBA__Original_Currency__c);
		}

		Map<Id, HPC_Collection__Receipt__c> receiptsMap = new Map<Id, HPC_Collection__Receipt__c>([
			SELECT HPC_Collection__Income__c, HPC_Collection__Income__r.Novidea_HPC__Premium_Currency__c, Legal_Entity__c, Legal_Entity__r.Base_Currency__c,
				HPC_Collection__Policy__c, HPC_Collection__Policy__r.Fronting_Policy__c
			FROM HPC_Collection__Receipt__c
			WHERE Id IN :receiptIdSet
				AND HPC_Collection__Policy__c != null
		]);

		for(HPC_Collection__Receipt__c receipt : receiptsMap.values()){
			if(receipt.HPC_Collection__Income__r.Novidea_HPC__Premium_Currency__c != null){
				sourceCurrencyList.add(receipt.HPC_Collection__Income__r.Novidea_HPC__Premium_Currency__c);
			}
			if(receipt.Legal_Entity__c != null && receipt.Legal_Entity__r.Base_Currency__c != null){
				targetCurrencyList.add(receipt.Legal_Entity__r.Base_Currency__c);
			}
		}

		List<Latest_Exchange_Rate__c> exchangeRateList = [
			SELECT Source_Currency__c, Target_Currency__c
			FROM Latest_Exchange_Rate__c
			WHERE Source_Currency__c IN :sourceCurrencyList
					AND Target_Currency__c IN :targetCurrencyList
		];
		Map<String, Latest_Exchange_Rate__c> latestExchangeRateMap = new Map<String, Latest_Exchange_Rate__c>();

		for(Latest_Exchange_Rate__c exchangeRate : exchangeRateList){
			String key = exchangeRate.Source_Currency__c + '-' + exchangeRate.Target_Currency__c;
			if(!latestExchangeRateMap.containsKey(key)){
				latestExchangeRateMap.put(key, exchangeRate);
			}
		}

		for(NIBA__Posting__c posting : indexToPostingMap.values()){
			HPC_Collection__Receipt__c receipt = receiptsMap.get(posting.NIHC__Receipt__c);
			String baseCurrencyLegalEntity = legalEntitiesMap.get(posting.NIBA__Legal_Entity__c) != null && legalEntitiesMap.get(posting.NIBA__Legal_Entity__c).Base_Currency__c != null ? legalEntitiesMap.get(posting.NIBA__Legal_Entity__c).Base_Currency__c : null;
			String key = baseCurrencyLegalEntity + '-' + posting.NIBA__Original_Currency__c;
			if(latestExchangeRateMap.containsKey(key)){
				posting.Exchange_Rate__c = latestExchangeRateMap.get(key).Id;
			}
		}

		Map<Integer, NIBA__Posting__c> relevantPostingsMap = new Map<Integer, NIBA__Posting__c>();

		for(Integer key : indexToPostingMap.keyset()){
			NIBA__Posting__c posting = indexToPostingMap.get(key);
			if(posting.NIBA__Posting_Type__c == 'Cash Payment' && posting.NIHC__Receipt__c != null){
				relevantPostingsMap.put(key, posting);
			}
		}

		if(!relevantPostingsMap.isEmpty()){
			List<Id> policyIdList = new List<Id>();
			Map<Id, Id> policyIdTofrontingPolicyIdMap = new Map<Id, Id>();

			for(HPC_Collection__Receipt__c receipt : receiptsMap.values()){
				if(receipt.HPC_Collection__Policy__r.Fronting_Policy__c  != null){
					policyIdTofrontingPolicyIdMap.put(
						receipt.HPC_Collection__Policy__c, receipt.HPC_Collection__Policy__r.Fronting_Policy__c);
				}
			}
			
			if(!policyIdTofrontingPolicyIdMap.isEmpty()){
				List<NIBA__Posting__c> frontingPostings = [
					SELECT NIHC__Receipt__c, NIHC__Receipt__r.HPC_Collection__Policy__c
					FROM NIBA__Posting__c
					WHERE NIBA__Allocation_Ref__c != null
						AND NIHC__Receipt__r.HPC_Collection__Policy__c IN :policyIdTofrontingPolicyIdMap.values()
				];
		
				Map<Id, HPC_Collection__Receipt__c> policyIdToFrontingReceiptsMap = new Map<Id, HPC_Collection__Receipt__c>();
				Map<Id, NIBA__Posting__c> receiptIdTofrontingPostingsMap = new Map<Id, NIBA__Posting__c>();
				Set<Id> frontingPolicyIdsWithInvalidPostings = new Set<Id>();
				
				for(NIBA__Posting__c posting : frontingPostings){
					//The fronting policy contains allocated postings
					frontingPolicyIdsWithInvalidPostings.add(posting.NIHC__Receipt__r.HPC_Collection__Policy__c);
				}

				for(NIBA__Posting__c posting : relevantPostingsMap.values()){
					Id frontingPolicyId = policyIdTofrontingPolicyIdMap.get(posting.NIHC__Receipt__r.HPC_Collection__Policy__c);
					if(frontingPolicyIdsWithInvalidPostings.contains(frontingPolicyId)){
						posting.Validation__c = 'Fronting Policy Not Allocated';
					}else{
						posting.Validation__c = null;
					}
				}
			}
		}
	}

	public static void generateInstallmentNoteNumbers(List<NIBA__Posting__c> newPostings){
		Set<Id> receiptIds = new Set<Id>();
		List<InstallmentAutoNumberGenerator.TransactionRow> transactionRows = new List<InstallmentAutoNumberGenerator.TransactionRow>();
		for(NIBA__Posting__c posting : newPostings){
			receiptIds.add(posting.NIHC__Receipt__c);
		}

		if(!NOVU.ArrayUtils.isListNullOrEmpty(new List<Id>(receiptIds))){
			Map<Id, HPC_Collection__Receipt__c> receiptsWithRelated = new Map<Id, HPC_Collection__Receipt__c>([
				SELECT HPC_Collection__Income__c, HPC_Collection__Income__r.RecordTypeId, HPC_Collection__Income__r.RecordType.DeveloperName
				FROM HPC_Collection__Receipt__c
				WHERE Id IN :receiptIds
			]);
			Map<String, RecordType> incomeRts = NOVU.RecordTypeUtils.getRecordTypesByDevNameAndFullSObjType(
				new List<String>{ 'uwr_cancellation', 'uwr_fdo', 'uwr_mta', 'uwr_regular' },
				'Novidea_HPC__Income__c'
			);
			if(!NOVU.ArrayUtils.isListNullOrEmpty(receiptsWithRelated.values())){
				for(NIBA__Posting__c posting : newPostings){
					HPC_Collection__Receipt__c receipt = receiptsWithRelated.get(posting.NIHC__Receipt__c);
					if((posting.Debit_Credit_note_No__c == null || String.isEmpty(posting.Debit_Credit_note_No__c)) &&
					receipt.HPC_Collection__Income__c != null && receipt.HPC_Collection__Income__r.RecordTypeId != null){
						transactionRows.add(new InstallmentAutoNumberGenerator.TransactionRow(
							incomeRts.containsKey(receipt.HPC_Collection__Income__r.RecordType.DeveloperName), 
							receipt.HPC_Collection__Income__c)
						);
					}
				}
			}

			Map<Id, String> receiptIdToNoteNumberMap = new Map<Id, String>();
			if(!NOVU.ArrayUtils.isListNullOrEmpty(transactionRows)){
				receiptIdToNoteNumberMap = InstallmentAutoNumberGenerator.generateNumbers(transactionRows);
			}
			if(!NOVU.ArrayUtils.isListNullOrEmpty(receiptIdToNoteNumberMap.values())){
				for(NIBA__Posting__c posting : newPostings){
					if(posting.Debit_Credit_note_No__c == null || String.isEmpty(posting.Debit_Credit_note_No__c)){
						posting.Debit_Credit_note_No__c = receiptIdToNoteNumberMap.get(posting.NIHC__Receipt__c);
					}
				}
			}
		}
	}
}