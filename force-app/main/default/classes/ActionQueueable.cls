public with sharing class ActionQueueable implements Queueable{
    /** Constant value for creation reason */
	private static final String CREATION_REASON = 'Creation';//TODO: label this.
	/** The ID of the transaction to negate */
    public String dataInputJson {set; get;}
    public String dispatcherName {set; get;}

    public ActionQueueable(String dataInputJson, String dispatcherName) {
        this.dataInputJson = dataInputJson;
        this.dispatcherName = dispatcherName;
    }

    public void execute(QueueableContext context){
        System.Savepoint beforeAction = Database.setSavePoint();
        try{
            NOVU.WebResult result = (NOVU.WebResult)JSON.deserialize((String)NOVU.ActionsController.ActionsCall(dataInputJson, dispatcherName), NOVU.WebResult.class);
            System.debug(result);
            if(result.success != true){
                Database.rollback(beforeAction);
                throw new NOVU.NovideaExceptions.InvalidDataException(result.errorMessage);
            }
        }catch(Exception curException){
            String result = curException.getMessage();
            Database.rollback(beforeAction);
            throw curException;
        }
    }
    
}
