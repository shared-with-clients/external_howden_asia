@isTest(seeAllData = false)
public class ChangeCollectionTypeControllerTest {

    @TestSetup
    static void makeData(){
        RecordType accountRecordTypeCarrier = [SELECT Id FROM RecordType WHERE DeveloperName = 'Carrier' AND SobjectType = 'Account'];
        Account testAccountCarrier = new Account(
            Name = 'Test Account Carrier',
            RecordTypeId = accountRecordTypeCarrier.Id,
            NIBA__Re_Insurer_type__c = 'Non-Bureau Company',
            ShippingCountry = 'Singapore');

        RecordType accountRecordTypeClient = [SELECT Id FROM RecordType WHERE DeveloperName = 'Business' AND SobjectType = 'Account'];
        Account testAccountClient = new Account(
            Name = 'Test Account Client',
            RecordTypeId = accountRecordTypeClient.Id,
            ShippingCountry = 'United Kingdom');

        Database.insert(new List<Account>{testAccountCarrier, testAccountClient});


        Novidea_HPC__Policy__c policy = new Novidea_HPC__Policy__c(Novidea_HPC__Carrier__c = testAccountCarrier.Id, Novidea_HPC__Client__c = testAccountClient.Id);
        Database.insert(policy);

        RecordType incomeRecordType = [SELECT Id FROM RecordType 
            WHERE DeveloperName = 'Regular' 
                AND IsActive = true 
                AND SobjectType = 'Novidea_HPC__Income__c'];

        Novidea_HPC__Income__c income = new Novidea_HPC__Income__c(
            RecordTypeId = incomeRecordType.Id, 
            Novidea_HPC__Client__c = testAccountClient.Id, 
            Novidea_HPC__Policy__c = policy.Id, 
            Novidea_HPC__Carrier__c = testAccountCarrier.Id,
            Novidea_HPC__Number_of_Payments__c = 1,
            HPC_Collection__Period_Between_Payments__c = 12,
            HPC_Collection__First_Payment_Date__c = Date.today(),
            Premium_before_GST__c = 1000);
            
        Database.insert(income);

    }

    @isTest
    static void testChangeCollectionTypeNotAllow() {
        Novidea_HPC__Income__c income = [SELECT Id 
                                        FROM Novidea_HPC__Income__c
                                        LIMIT 1];

        ApexPages.StandardController stdController = new ApexPages.StandardController(income);
        
        ChangeCollectionTypeController controller = new ChangeCollectionTypeController(stdController);
        controller.init();
        
        System.assert((ApexPages.getMessages())[0].getSummary().contains(Label.Reverse_Transaction_Error));
    }
    
    @isTest
    static void testChangeCollectionTypeAllow() {
        Novidea_HPC__Income__c income = [SELECT Provisional_Bill__c 
                                        FROM Novidea_HPC__Income__c
                                        LIMIT 1];

        income.Provisional_Bill__c = true;
        Database.update(income);

        ApexPages.StandardController stdController = new ApexPages.StandardController(income);
        
        ChangeCollectionTypeController controller = new ChangeCollectionTypeController(stdController);
        PageReference pg = controller.init();
        System.assertEquals('/apex/NIHC__ContraOfTransaction?Id=' + income.id, pg.getURL());
 
        pg = controller.back();
        System.assertEquals('/' + income.id, pg.getURL());
    }
}
