trigger ReceiptTrigger on HPC_Collection__Receipt__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {
    Novidea_HPC__Trigger__c triggerSetting  = Novidea_HPC__Trigger__c.getInstance(); 
    if(triggerSetting != null && triggerSetting.Novidea_HPC__Prevent_Running__c != true){
        if(Trigger.isInsert && Trigger.isBefore){
            ReceiptTriggerHelper.onBeforeInsert((list<HPC_Collection__Receipt__c>)Trigger.new);
        }  
        else if(Trigger.isInsert && Trigger.isAfter){
            ReceiptTriggerHelper.onAfterInsert((list<HPC_Collection__Receipt__c>)Trigger.new);        
        }
        else if(Trigger.isUpdate && Trigger.isBefore){
            ReceiptTriggerHelper.onBeforeUpdate((list<HPC_Collection__Receipt__c>)Trigger.old, 
                    (Map<Id,HPC_Collection__Receipt__c>) Trigger.oldMap,
                    (list<HPC_Collection__Receipt__c>)Trigger.new,
                    (Map<Id,HPC_Collection__Receipt__c>) Trigger.newMap);
        }
        else if(Trigger.isUpdate && Trigger.isAfter){
            System.debug('After Update');
            ReceiptTriggerHelper.onAfterUpdate((list<HPC_Collection__Receipt__c>)Trigger.old,
                    (Map<Id,HPC_Collection__Receipt__c>) Trigger.oldMap, 
                    (list<HPC_Collection__Receipt__c>)Trigger.new, 
                    (Map<Id,HPC_Collection__Receipt__c>)Trigger.newMap);
        }   
    }  
}