trigger PostingTrigger on NIBA__Posting__c (before insert) {
    Novidea_HPC__Trigger__c triggerSetting  = Novidea_HPC__Trigger__c.getInstance(); 
    if (triggerSetting.Novidea_HPC__Prevent_Running__c == null || !triggerSetting.Novidea_HPC__Prevent_Running__c){ 

        if(Trigger.isInsert && Trigger.isBefore){
            PostingTriggerHelper.onBeforeInsert(Trigger.new);
        }
    }
}