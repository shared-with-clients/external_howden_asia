trigger IncomeTrigger on Novidea_HPC__Income__c(before insert, before update){
	Novidea_HPC__Trigger__c triggerSetting  = Novidea_HPC__Trigger__c.getInstance(); 
	if(triggerSetting != null && triggerSetting.Novidea_HPC__Prevent_Running__c != true){ 
		if(Trigger.isInsert && Trigger.isBefore){
			incomeTriggerHelper.onBeforeInsert(Trigger.new, trigger.newMap);
		}else if(trigger.isBefore && trigger.isUpdate){
			incomeTriggerHelper.onBeforeUpdate(trigger.new, trigger.newMap, trigger.old, trigger.oldMap);    
		}
	}
}